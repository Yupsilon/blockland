﻿using System;
using UnityEngine;
	
public class entityProjectile: entityBase
	{
	public int behavior = 0;
	public bool affectedByWind = false;
	public float rotateVelocity = 0;
	public float Weight=0f;
	public float groundFriction=0f;
	public Vector3 baseRotation=Vector3.zero;
	public Vector3 lastParticlePoint=Vector3.zero;
	public Texture2D trailTexture;
	public Texture2D bumpTexture;
	public Timer thinker;

	public delegate void collisionAction(entityProjectile self,Vector3 dir);
	public collisionAction collide;
	public collisionAction die;

	public entityProjectile (){
	}

	public entityProjectile (GameMission g, GameObject parent,Vector3 speed, float weight, bool wind, string trail, string ground)
	{
		game = g;
		gameObject = parent;
		behavior = 0;

		ForcesActing.Add (new Vector4 (speed.x, speed.y, speed.z, Time.time + 0.01f));

		Weight = weight;
		affectedByWind = wind;

		trailTexture = SpecialEffects.LoadTexture(game.gDat,trail);
		bumpTexture=SpecialEffects.LoadTexture(game.gDat,ground);
	}

	public entityProjectile (GameMission g, GameObject parent,Vector3 speed, float weight, float angvelocity,bool wind, string trail, string ground)
	{
		game = g;
		gameObject = parent;
		behavior = 1;

		ForcesActing.Add (new Vector4 (speed.x, speed.y, speed.z, Time.time + 0.01f));
		baseRotation = parent.transform.rotation.eulerAngles;

		Weight = weight;
		affectedByWind = wind;
		rotateVelocity = angvelocity;

		trailTexture = SpecialEffects.LoadTexture(game.gDat,trail);
		bumpTexture=SpecialEffects.LoadTexture(game.gDat,ground);
	}

	public override void onCreated ()
	{
		base.onCreated ();
	}

	public void createThiker(Timer.timerAction think,Timer.timerAction end)
	{
		if (thinker != null) {
			thinker.Kill (false);
		}
		thinker = Timer.Create(
			gameObject.name,0,
			think,end

		);
	}

	public override void onCollide(Vector3 dir)
	{
		if (collide != null) {

	  

			collide (this, dir);
		}
		if (bumpTexture != null) {
			float renderFix = 0.01f;
			if (!isGrounded ()) {
				if (dir.y > 0) {
					SpecialEffects.ExplosionShockwave (game, bumpTexture, true, 50f, new Vector3 (Position.x, Mathf.Ceil (Position.y)-renderFix, Position.z), Vector3.zero, 0.5f, 0.5f, 0.5f, Color.white);
				} else if (dir.y < 0) {
					SpecialEffects.ExplosionShockwave (game, bumpTexture, true, 50f, new Vector3 (Position.x, Mathf.Floor (Position.y)+renderFix, Position.z), Vector3.zero, 0.5f, 0.5f, 0.5f, Color.white);
				}
			}

			if (dir.x > 0) {
				SpecialEffects.ExplosionShockwave (game, bumpTexture, true, 50f, new Vector3 (Mathf.Ceil (Position.x)-renderFix, Position.y, Position.z), new Vector3 (0, 0, -90), 0.5f, 0.5f, 0.5f, Color.white);
			} else if (dir.x < 0) {
				SpecialEffects.ExplosionShockwave (game, bumpTexture, true, 50f, new Vector3 (Mathf.Floor (Position.x)+renderFix, Position.y, Position.z), new Vector3 (0, 0, 90), 0.5f, 0.5f, 0.5f, Color.white);
			}

			if (dir.z > 0) {
				SpecialEffects.ExplosionShockwave (game, bumpTexture, true, 50f, new Vector3 (Position.x, Position.y, Mathf.Ceil (Position.z)-renderFix), new Vector3 (-90, 0, 0), 0.5f, 0.5f, 0.5f, Color.white);
			} else if (dir.z < 0) {
				SpecialEffects.ExplosionShockwave (game, bumpTexture, true, 50f, new Vector3 (Position.x, Position.y, Mathf.Floor (Position.z)+renderFix), new Vector3 (90, 0, 0), 0.5f, 0.5f, 0.5f, Color.white);
			}
		}  

	
	}

	public override void Die (bool destroy)
	{
		if (die != null) {
			die (this,Vector3.zero);
		}
		if (thinker != null) {
			thinker.Kill (true);
		}
		base.Die (destroy);
	}

	public override float GetMass()
	{
		return Weight;
	}

	public override float GetAirDrag()
	{
		if (underWater) {
			return 1;
		}
		return 0f;
	}

	public override float GetFriction ()
	{
		return groundFriction;
	}

	public override bool Collides()
	{
		return true;
	}

	public bool hasUnderwaterAbility()
	{
		return false;
	}

	public override void Tick()
	{

		if (behavior == 0) {
			if (Velocity != Vector3.zero) {
				Quaternion realRotation = Quaternion.LookRotation (Velocity);
				Rotation = realRotation.eulerAngles+baseRotation;
			}
		}
		if (rotateVelocity > 0 && Velocity!=Vector3.zero) {
			Quaternion rotV = Quaternion.LookRotation (Velocity);
			Rotation += ( Quaternion.Inverse (Quaternion.Euler (baseRotation)) *(rotV)).eulerAngles/360f*rotateVelocity;
		}

		if (trailTexture != null && (Position - lastParticlePoint).magnitude > 0.5f) {
			lastParticlePoint = Position;
			SpecialEffects.ExplosionGeneric (game, trailTexture, 100f, lastParticlePoint, 10f, 0.5f, 1f, 1, Color.white);
		}



		if (!underWater && !hasUnderwaterAbility() && Position.y < game.GetWaterLevel ()) {

			SpecialEffects.makeSplash (game, Position, 1f, 1.5f);
			underWater = true;
			Timer.Create ("nimi", 5f,
				delegate(Timer original) {
					Die(true);
					return 0f;	
				});
		}

		base.Tick ();

	}
	public override bool isRelevant ()
	{
		return !isDead;
	}
	public Vector3 Size = Vector3.one;
	public override Vector3 getEntitySize ()
	{
		return Size;
	}
}

