﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class propertyMesh
{
	GameResources game;
	public DataMesh MeshData;
	public int TotalFrames;	
	public string TextureName;

	//Dynamic
	public List<string> LimbNames=new List<string>();
	public List<string> AnimationNames = new List<string>();

	public List<LimbAnimationData> Animations=new List<LimbAnimationData>();

	public propertyMesh(GameResources game){
		this.game = game;
		MeshData = DataMesh.LoadDefaultMesh ();
		TextureName = MeshData.MeshName;
	}
	public propertyMesh(GameResources game,string LoadFrom){
		this.game = game;
		MeshData = DataMesh.LoadDefaultMesh ();
		TextureName = LoadFrom;

		LimbNames=new List<string>();
		foreach (LimbBodyData Gir in MeshData.BodyData) {

			LimbNames.Add (Gir.LimbName);
		}

		Animations = new List<LimbAnimationData> ();

		Animations.Add (LimbAnimationData.Make("pose",1,false, new List<LimbAnimation>(), new List<string>()));
		//Animations[0].StartFrame=0;
		TotalFrames = Animations[0].AnimationFrames;

		AnimationNames = new List<string> {"pose"};
		foreach (LimbAnimationData Animation in MeshData.AnimationData) {

			AnimationNames.Add (Animation.AnimationName);
			Animations.Add (Animation);
		}
	}

	public Texture2D LoadTexture(string Loc)
	{
		return (LoadTexture (Loc, true));
	}

		public Texture2D LoadTexture(string Loc,bool readable)
		{
		if (game.LoadTexture (TextureName + "/", Loc,readable) != null) {
			return game.LoadTexture (TextureName + "/", Loc,readable);
		} else if (game.LoadTexture (TextureName + "/", TextureName,readable) != null) {
			return game.LoadTexture (TextureName + "/", TextureName,readable);
		}else  if (game.LoadTexture (MeshData.MeshName + "/", Loc,readable) != null) {
			return game.LoadTexture (MeshData.MeshName + "/", Loc,readable) ;
		}else  {
			return game.LoadTexture (MeshData.MeshName + "/", MeshData.MeshName,readable) ;
		}
	}

	public LimbAnimationData FindAnimation( string nAnimation)
	{
		List<string> Alternatives = new List<string>{ nAnimation , "stand","pose" };



		foreach (string aName in Alternatives) {
			if (AnimationNames.Contains (aName))
			{
				return Animations[AnimationNames.IndexOf (aName)];
			}
		}
		return null;
	}
}