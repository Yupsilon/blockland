﻿using UnityEngine;
using UnityEditor;
using System.Collections;


public class CreatePlane : ScriptableWizard
{

	public enum Orientation
	{
		Horizontal,
		Vertical
	}

	public enum AnchorPoint
	{
		TopLeft,
		TopHalf,
		TopRight,
		RightHalf,
		BottomRight,
		BottomHalf,
		BottomLeft,
		LeftHalf,
		Center
	}

	public static GameObject CreateSimple()
	{
		return Create (1, 1, 1, 1, Orientation.Horizontal, AnchorPoint.Center,  false, "Plane");
	}

	public static GameObject CreateSprite(
		float width,
		float length,
		Orientation orientation ,
		AnchorPoint anchor ,
		bool twoSided ,
		Sprite sprite,
		string name
	)
	{
		GameObject plane =  Create (1, 1, 1, 1, Orientation.Horizontal, anchor,  false, name);

		ApplySprite (plane.GetComponent<MeshRenderer> (), sprite);
	
		return plane;
	}

	public static void FaceCamera(Transform mesh, Camera cam)
	{
		mesh.transform.rotation = Quaternion.Euler (
			cam.transform.rotation.eulerAngles.x-90,
			cam.transform.rotation.eulerAngles.y,
			cam.transform.rotation.eulerAngles.z
		);
	}

	public static void ConfigurePlaneMaterial(Material vmat)
	{
		vmat.shader = (Shader.Find ("Standard"));

		vmat.SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
		vmat.SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);

		vmat.SetInt ("_ZWrite", 1);
		vmat.EnableKeyword ("_ALPHATEST_ON");
		vmat.EnableKeyword ("_ALPHABLEND_ON");
		vmat.EnableKeyword ("_ALPHAMMULTIPLY_ON");

		vmat.renderQueue = 2450;
	}

	public static void ApplyColor(MeshRenderer mesh, Color col)
	{
		mesh.material.color = col;
	}
		public static void ApplySprite(MeshRenderer mesh, Sprite sprite)
		{
				mesh.material.mainTexture=sprite.texture;
		int addition = 0;
		if (sprite.texture.filterMode!=FilterMode.Point) {
			addition = 1;
		}
			mesh.material.mainTextureOffset = new Vector2 (
			(sprite.rect.xMin + addition) / sprite.texture.width,
			(sprite.rect.yMin + addition) / sprite.texture.height
			);
			mesh.material.mainTextureScale = new Vector2 (
			(sprite.rect.width - addition*2) / sprite.texture.width,
			(sprite.rect.height - addition*2) / sprite.texture.height
			);

	}

	public static GameObject Create(
		int widthSegments,
		 int lengthSegments ,
		 float width,
		 float length ,
		 Orientation orientation ,
		 AnchorPoint anchor ,
		 bool twoSided ,
		string optionalName
	)
	{
		GameObject plane = new GameObject();

		if (!string.IsNullOrEmpty(optionalName))
			plane.name = optionalName;
		else
			plane.name = "Plane";

			plane.transform.position = Vector3.zero;

		Vector2 anchorOffset;
		string anchorId;
		switch (anchor)
		{
		case AnchorPoint.TopLeft:
			anchorOffset = new Vector2(-width/2.0f,length/2.0f);
			anchorId = "TL";
			break;
		case AnchorPoint.TopHalf:
			anchorOffset = new Vector2(0.0f,length/2.0f);
			anchorId = "TH";
			break;
		case AnchorPoint.TopRight:
			anchorOffset = new Vector2(width/2.0f,length/2.0f);
			anchorId = "TR";
			break;
		case AnchorPoint.RightHalf:
			anchorOffset = new Vector2(width/2.0f,0.0f);
			anchorId = "RH";
			break;
		case AnchorPoint.BottomRight:
			anchorOffset = new Vector2(width/2.0f,-length/2.0f);
			anchorId = "BR";
			break;
		case AnchorPoint.BottomHalf:
			anchorOffset = new Vector2(0.0f,-length/2.0f);
			anchorId = "BH";
			break;
		case AnchorPoint.BottomLeft:
			anchorOffset = new Vector2(-width/2.0f,-length/2.0f);
			anchorId = "BL";
			break;			
		case AnchorPoint.LeftHalf:
			anchorOffset = new Vector2(-width/2.0f,0.0f);
			anchorId = "LH";
			break;			
		case AnchorPoint.Center:
		default:
			anchorOffset = Vector2.zero;
			anchorId = "C";
			break;
		}

		MeshFilter meshFilter = (MeshFilter)plane.AddComponent(typeof(MeshFilter));
		plane.AddComponent(typeof(MeshRenderer));
		plane.GetComponent<MeshRenderer>().material=Resources.Load<Material> ("Materials/2D_Plane");


		string planeAssetName = plane.name + widthSegments + "x" + lengthSegments + "W" + width + "L" + length + (orientation == Orientation.Horizontal? "H" : "V") + anchorId + ".asset";
		Mesh m = (Mesh)AssetDatabase.LoadAssetAtPath("Assets/Editor/" + planeAssetName,typeof(Mesh));

		if (m == null)
		{
			m = new Mesh();
			m.name = plane.name;

			int hCount2 = widthSegments+1;
			int vCount2 = lengthSegments+1;
			int numTriangles = widthSegments * lengthSegments * 6;
			if (twoSided) {
				numTriangles *= 2;
			}
			int numVertices = hCount2 * vCount2;

			Vector3[] vertices = new Vector3[numVertices];
			Vector2[] uvs = new Vector2[numVertices];
			int[] triangles = new int[numTriangles];
			Vector4[] tangents = new Vector4[numVertices];
			Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);

			int index = 0;
			float uvFactorX = 1.0f/widthSegments;
			float uvFactorY = 1.0f/lengthSegments;
			float scaleX = width/widthSegments;
			float scaleY = length/lengthSegments;
			for (float y = 0.0f; y < vCount2; y++)
			{
				for (float x = 0.0f; x < hCount2; x++)
				{
					if (orientation == Orientation.Horizontal)
					{
						vertices[index] = new Vector3(x*scaleX - width/2f - anchorOffset.x, 0.0f, y*scaleY - length/2f - anchorOffset.y);
					}
					else
					{
						vertices[index] = new Vector3(x*scaleX - width/2f - anchorOffset.x, y*scaleY - length/2f - anchorOffset.y, 0.0f);
					}
					tangents[index] = tangent;
					uvs[index++] = new Vector2(x*uvFactorX, y*uvFactorY);
				}
			}

			index = 0;
			for (int y = 0; y < lengthSegments; y++)
			{
				for (int x = 0; x < widthSegments; x++)
				{
					triangles[index]   = (y     * hCount2) + x;
					triangles[index+1] = ((y+1) * hCount2) + x;
					triangles[index+2] = (y     * hCount2) + x + 1;

					triangles[index+3] = ((y+1) * hCount2) + x;
					triangles[index+4] = ((y+1) * hCount2) + x + 1;
					triangles[index+5] = (y     * hCount2) + x + 1;
					index += 6;
				}
				if (twoSided) {
					// Same tri vertices with order reversed, so normals point in the opposite direction
					for (int x = 0; x < widthSegments; x++)
					{
						triangles[index]   = (y     * hCount2) + x;
						triangles[index+1] = (y     * hCount2) + x + 1;
						triangles[index+2] = ((y+1) * hCount2) + x;

						triangles[index+3] = ((y+1) * hCount2) + x;
						triangles[index+4] = (y     * hCount2) + x + 1;
						triangles[index+5] = ((y+1) * hCount2) + x + 1;
						index += 6;
					}
				}
			}

			m.vertices = vertices;
			m.uv = uvs;
			m.triangles = triangles;
			m.tangents = tangents;
			m.RecalculateNormals();
		}

		meshFilter.sharedMesh = m;
		m.RecalculateBounds();

		return plane;
	}




}