﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class BoneAnimationOverride
{
	public Vector3 pos=Vector3.zero;
	public Vector3 rot=Vector3.zero;
	public Vector3 scale=Vector3.one;

	public BoneAnimationOverride()
	{

	}

	public BoneAnimationOverride(Vector3 p,Vector3 r, Vector3 s)
	{
		pos=p;
		rot=r;
		scale=s;
	}
}
