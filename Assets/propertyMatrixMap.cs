﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class propertyMatrixMap:propertyMap
{
	public enum WorldType
	{
		Continental,
		Archipelago,
	}
	public propertyMatrixMap(int GenSize, WorldType GenType)
	{
		Height = 100;
		Generate (GenSize-1);

		switch (GenType)
		{
		case WorldType.Continental:
			ElevateHill (GenSize / 2, GenSize / 2, GenSize * 3 / 5, 25);

			float mountains = 2+UnityEngine.Random.Range (0, GenSize / 10);
			for (int il = 0; il < mountains; il++) {
				float radius = 5+15*UnityEngine.Random.value*GenSize/100;
				ElevateHill (radius+(GenSize-radius*2) *UnityEngine.Random.value, radius+(GenSize-radius*2)*UnityEngine.Random.value,radius , 10+10*UnityEngine.Random.value);

			}

			Naturalize (1, 0.5f, 4);	
			break;
		case WorldType.Archipelago:

			float islands = 20+UnityEngine.Random.Range (0, GenSize / 10);
			for (int il = 0; il < islands; il++) {
				float radius = 5+15*UnityEngine.Random.value;
				//		ElevateHill (radius+(GenSize-radius*2) *UnityEngine.Random.value, radius+(GenSize-radius*2)*UnityEngine.Random.value,radius , //GenHeight/islands);

			}

			Naturalize (0.5f, 0.1f, 2);	
			break;
		}
	}


	public void ElevateMountain(int X, int Y, int Rad, int Elevation)
	{
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);

				if (Dist <= Rad) {
					SetTileAt (iX, iY, Elevation- (Elevation - GetTileAt (iX, iY)) * (Dist / Rad));
				}
			}
		}
	}
	public void ElevateHill(float X, float Y, float Rad, float Elevation)
	{
		ElevateHill ((int)X, (int)Y, (int)Rad, (int)Elevation);
	}

	public void ElevateHill(int X, int Y, int Rad, int Elevation)
	{
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);

				if (Dist < Rad) {
					SetTileAt (iX, iY,GetTileAt (iX, iY)+  (Elevation - GetTileAt (iX, iY)) * Mathf.Cos(Dist / Rad*Mathf.PI/2f));
				}
			}
		}
	}

	public void ElevateChasm(int X, int Y, int Rad, int Elevation)
	{
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);

				if (Dist < Rad) {
					SetTileAt (iX, iY,GetTileAt (iX, iY)+  (Elevation - GetTileAt (iX, iY)) * Mathf.Cos(Dist / Rad*Mathf.PI/2f));
				}
			}
		}
	}

	public void Naturalize(float Var, float wLevel, float Max)
	{
		int dSize = 4;
		int rSize = Mathf.CeilToInt ((float)GetWidth() / (float)dSize) * dSize + 1;

		propertyMap NaturalMap = new propertyMap (rSize);

		NaturalMap.SetTileAt (0, 0, UnityEngine.Random.Range (1, Max));
		NaturalMap.SetTileAt (0, NaturalMap.GetWidth(), UnityEngine.Random.Range (1, Max));
		NaturalMap.SetTileAt (NaturalMap.GetWidth(), 0, UnityEngine.Random.Range (1, Max));
		NaturalMap.SetTileAt (NaturalMap.GetWidth(), NaturalMap.GetWidth(), UnityEngine.Random.Range (1, Max));


		while (dSize >1 ) {
			for (int Y = 0; Y <= NaturalMap.GetWidth() / dSize; Y++) {
				for (int X = 0; X <= NaturalMap.GetWidth() / dSize; X++) {
					if (NaturalMap.GetTileAt (X * (dSize), Y * (dSize)) == 0) {
						if (NaturalMap.GetTileAt ((X - 1) * (dSize), (Y - 1) * (dSize)) == 0 &&
							NaturalMap.GetTileAt ((X - 1) * (dSize), (Y + 1) * (dSize)) == 0 &&
							NaturalMap.GetTileAt ((X + 1) * (dSize), (Y - 1) * (dSize)) == 0 &&
							NaturalMap.GetTileAt ((X + 1) * (dSize), (Y + 1) * (dSize)) == 0) { 
							NaturalMap.SetTileAt (X * dSize, Y * dSize, UnityEngine.Random.Range (1, Max));

						}else {
							NaturalMap.SmoothenTile (X * dSize, Y * dSize, dSize,false);
							float rnd = UnityEngine.Random.Range (-Var, Var);
							NaturalMap.SetTileAt (X * dSize, Y * dSize, 

								Mathf.Clamp(
									NaturalMap.GetTileAt (X * dSize, Y * dSize) + rnd,
									0,Max)


							);
							if (float.IsNaN(NaturalMap.GetTileAt (X * dSize, Y * dSize))) {
								NaturalMap.SetTileAt (X * dSize, Y * dSize, UnityEngine.Random.Range (1, Max));

							}
						}
					}
				}
			}


			for (int Y = 0; Y <= NaturalMap.GetWidth () / dSize; Y++) {
				for (int X = 0; X <= NaturalMap.GetWidth () / dSize; X++) {


					NaturalMap.SmoothenTile (X * dSize + Mathf.FloorToInt (dSize / 2f), 
						Y * dSize + Mathf.FloorToInt (dSize / 2f), 
						Mathf.FloorToInt(dSize/2f),true);
				}
			}

			dSize /= 2;
		}

		for (int Y = 0; Y <= NaturalMap.GetWidth () ; Y++) {
			for (int X = 0; X <= NaturalMap.GetWidth () ; X++) {
				if (NaturalMap.GetTileAt (X , Y ) == 0) {

					NaturalMap.SmoothenTile (X ,Y, 1,false);

				}
			}
		}

		for (int Y = 0; Y < GetWidth () ; Y++) {
			for (int X = 0; X < GetWidth () ; X++) {
				SetTileAt (X, Y, GetTileAt(X,Y)+NaturalMap.GetTileAt (X, Y));
			}
		}

		Max = GetMaxElevation();
		for (int Y = 0; Y < GetWidth () ; Y++) {
			for (int X = 0; X < GetWidth () ; X++) {
				SetTileAt (X, Y, GetTileAt(X,Y)-Max*wLevel);

			}
		}
	}
	public override Color GetColorAt(int iX,int iY, int iZ)
	{
		Color landC = new Color(0,1,0,1);
		Color hillC = new Color(1f/4f,3f/4f,1f/4f,1);
		Color snowC = new Color(1,1,1,1);
		Color dirtC = new Color(4/5f,2f/3f,0,1);
		Color shoreC = new Color(221f/255f,187f/255f,116f/255f,1);

		float WaterLevel = 0;
		float Max = Mathf.Max(Height/2,GetMaxElevation ());

		Color fColor = shoreC;
		if (iY < GetTileAt(iX,iZ)-1) {
			fColor = dirtC;
		} else {
			float Val = (Max - WaterLevel); 
			if (isInValue (iY, WaterLevel, WaterLevel + Val * 1f / 4f) >= 0) {
				float diff = isInValue (iY, WaterLevel, WaterLevel + Val * 1f / 4f);

				fColor = new Color (
					shoreC.r * (1 - diff) + landC.r * diff,
					shoreC.g * (1 - diff) + landC.g * diff,
					shoreC.b * (1 - diff) + landC.b * diff
				);

			} else if (isInValue (iY, WaterLevel + Val * 1f / 4f, WaterLevel + Val * 1f / 2f) >= 0) {
				float diff = isInValue (iY, WaterLevel + Val * 1f / 4f, WaterLevel + Val * 1f / 2f);

				fColor = new Color (
					landC.r * (1 - diff) + hillC.r * diff,
					landC.g * (1 - diff) + hillC.g * diff,
					landC.b * (1 - diff) + hillC.b * diff
				);

			} else if (isInValue (iY, WaterLevel + Val * 1f / 2f, Max) >= 0) {
				float diff = isInValue (iY, WaterLevel + Val * 1f / 2f, Max);

				fColor = new Color (
					hillC.r * (1 - diff) + snowC.r * diff,
					hillC.g * (1 - diff) + snowC.g * diff,
					hillC.b * (1 - diff) + snowC.b * diff
				);

			} else {
				fColor = snowC;

			}
		}

		float delta =  UnityEngine.Random.value / 5f;
		fColor.r -= delta;
		fColor.g -= delta;
		fColor.b -= delta;

		return fColor;
	}
}

