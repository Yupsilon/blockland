﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class propertyExplosion
{
	List<Color32> debreeColors = new List<Color32> ();

	public propertyExplosion(GameMission game, Vector3 center, float damageRadius, float kbRad, float kbMin, float kbMax, Texture2D effectName)
	{
		ClearTerrain (game, center, damageRadius);
		ExplosionSFX (game, center, damageRadius, effectName);
		KnockBackActors (game, center, kbRad, kbMin, kbMax);
		DamageActors (game, center, 1, 1);
	}


	public void ClearTerrain(GameMission game, Vector3 center, float damageRadius)
	{
		
		//CHANGE THE TERRAIN
		foreach (entityBlock bData in game.GetAllBlocksInCircle(center,damageRadius,false)) {
			debreeColors.Add (bData.getColor());
			bData.Die ();
		}
	}

	public void ExplosionSFX(GameMission game, Vector3 center, float damageRadius, Texture2D effectName)
	{
		//DO THE EFX
		if (center.y > game.GetWaterLevel ()) {
			SpecialEffects.ExplosionShockwave (game, SpecialEffects.LoadTexture(game.gDat,"shockwave"), 50f, center, damageRadius, 0.75f, 0.4f, Color.white);
			SpecialEffects.ExplosionGeneric (game, effectName, 10f, center, damageRadius, 0.4f, 0.2f, 3 +  game.settingsLevel, Color.white);
			if (debreeColors.Count > 0) {
				SpecialEffects.CastDebree (game, 5 + 1 * game.settingsLevel, damageRadius, damageRadius * 2, damageRadius / 2f, damageRadius, center, 0.1f, 1, debreeColors);
			}

			for (int Gir = 0; Gir < 3; Gir++) {
				Timer.Create ("smokegen", 0f+ Gir * 0.5f, delegate(Timer original) {
					SpecialEffects.ExplosionSmoke (game, SpecialEffects.LoadTexture(game.gDat,"smoke"), 100f, center, 1f, 1f, 0.5f, 3f, Color.white);
					return 0;});}
		} else {
			//bubbles!
		}
			}

			public void KnockBackActors(GameMission game, Vector3 center, float kbRad, float kbMin, float kbMax)
			{
				//knockback
				foreach (entityActor eData in game.GetAllGuysInRadius(center,kbRad)) {
					eData.Knockback ();
				}

				//knockback
				foreach (entityMesh mData in game.GetAllMeshesInRadius(center,kbMax/kbMin*kbRad)) {
					mData.ApplyKnockback (kbMax, center, kbRad,kbMax/kbMin*kbRad, 5f);
				}
			}

			public void DamageActors(GameMission game, Vector3 center, float radius, float damage)
			{
				foreach (entityActor eData in game.GetAllGuysInRadius(center,radius)) {
			eData.Skeleton.TakeDamage(damage,center);
				}
			}
}