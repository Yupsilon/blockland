﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAnimData
{
	public bool AnimationPaused=false;

	public LimbAnimationData animation = new LimbAnimationData();
	public float StartTime=0;
	public float EndTime=1;
	public float LagTime=1;
	public bool weaponEnabled=false;
	public int priority=0;

	public PlayerAnimData(LimbAnimationData anim,float lag, float end,int prio)
	{
		animation = anim;	
		StartTime = Time.time;
		LagTime = lag;
		EndTime = end;
		priority = prio;
	}

	public PlayerAnimData(LimbAnimationData anim,float lag, float end,int prio, bool wep)
	{
		animation = anim;	
		StartTime = Time.time;
		LagTime = lag;
		EndTime = end;
		priority = prio;
		weaponEnabled = wep;
	}

	public void Reset()
	{
		LagTime = 0;
		StartTime = Time.time;
	}

	public void End()
	{
		StartTime = 0;
		LagTime = 0;
		EndTime = 1;
	}

	public bool isFrozen()
	{
		return EndTime == 0;
	}

	public PlayerAnimData()
	{
		animation = new LimbAnimationData();
		StartTime = Time.time;
		LagTime = 1;
		EndTime = 1;
	}

	public float GetLagEnd()
	{
		return StartTime + LagTime;
	}

	public float GetDuration()
	{		
		return animation.AnimationFrames/EndTime;
	}

	public float GetEndTime()
	{		
		if (isFrozen ()) {
			return Mathf.Infinity;
		}
		return GetLagEnd () + GetDuration();
	}

	public float GetLagPercentage()
	{
		return (StartTime - Time.time)/LagTime;
	}

	public float GetDonePercentage()
	{
		if (isFrozen ()) {
			return StartTime;
		}
		return (Time.time-GetLagEnd())/(animation.AnimationFrames/EndTime);
	}

	public bool isLagging()
	{
		return LagTime>0 && GetLagEnd () > Time.time;
	}

	public string getAnimName()
	{
		string N = "";
		foreach (char C in animation.AnimationName) {
			if (C != '.') {
				N += C;
			} else
				break;
		}
		return N;
	}
}