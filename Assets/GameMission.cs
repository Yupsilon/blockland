﻿using System;
using UnityEngine;
using System.Collections.Generic;
	public class GameMission
	{
	float timeSum=0f;
	public List<List<List<entityBlock>>>  Tileset;
	public entityChunk[,,]  Map;
	public Vector3 MapSize=Vector3.zero;
	public List<Vector3> SpawnPoints=new List<Vector3>();

	public entityStage level;
	public List<entityBase > gameEntities;
	public List<entityActor> gamePlayers;
	public List<entityBase> collidingEntities = new List<entityBase> ();

	public entityActor Selection=null;
	GameObject WaterLevel;
	public GameResources gDat;

	//physx collision
	public int state=0;
	public List<entityMesh> activeColliders = new List<entityMesh> ();
	public List<entityChunk> needsUpdate= new List<entityChunk>();

	public int settingsLevel=5;

	public int CurrentTurn=0;

	public GameMission (GameResources res)
	{
		gDat=res;
		gameEntities = new List<entityBase> ();
		gamePlayers = new List<entityActor> ();

		DrawStage ();
		SpawnPoints=GenerateSpawnPoints();

		entitySide sideA = new entitySide (entityTeam.Generate(0),0);
		entitySide sideB = new entitySide (entityTeam.Generate(0),1);
		entitySide sideC = new entitySide (entityTeam.Generate(0),2);
		entitySide sideD = new entitySide (entityTeam.Generate(0),3);
		entitySide sideE = new entitySide (entityTeam.Generate(0),4);
		entitySide sideF = new entitySide (entityTeam.Generate(0),5);

		SpawnEntity(new entityActor (this,sideA,0));
		SpawnEntity(new entityActor (this,sideB,0));
		SpawnEntity(new entityActor (this,sideC,0));
		SpawnEntity(new entityActor (this,sideD,0));
		SpawnEntity(new entityActor (this,sideE,0));
		SpawnEntity(new entityActor (this,sideF,0));
		SpawnEntity(new entityCamera (this));
		gameEntities [0].Select();

		Timer gameTimer = Timer.Create ("game", Time.fixedDeltaTime,
			delegate {

				while (timeSum<Time.fixedDeltaTime){
					Think();

					timeSum+=Time.fixedDeltaTime;}

				timeSum-=Time.fixedDeltaTime;
				return Time.fixedDeltaTime;
			}, delegate{
				return 0;
			});
		gameTimer.running = true;
		changeGameState (0, true);
	}

	public entityCamera getCameraEntity()
	{
		foreach (entityBase e in gameEntities) {
			if (e as entityCamera != null) {
				return (entityCamera)e;
			}
		}
		return null;
	}

	public void changeGameState(int gstate, bool force)
	{
		if (state != gstate || force) {
			state = gstate;
			switch (state) {
			case 0://all collision disabled
				foreach (entityChunk data in Map) {
					data.enableCollision (false);
				}
				break;
			case 1://terrain collides, and specific few ragdolls
				foreach (entityChunk data in Map) {
					data.enableCollision (true);
				}
				break;
			case 2://all terrain collides, and meshes / used for raycasts

				foreach (entityChunk data in Map) {
					data.enableCollision (true);
				}
				break;
			}
		}
	}

	// Update is called once per frame
	void Think () {

		//TBD state 2
		if (state==1 && collidingEntities.Count > 0) {
			collidingEntities.RemoveAll (delegate(entityBase obj) {
				return obj == null || !obj.isRelevant ();
			});
		} else {
			changeGameState (0,false);
		}

		for (int Zim = 0; Zim < gameEntities.Count; Zim++) {
			gameEntities [Zim].Update ();
		}

		if (needsUpdate.Count > 0) {

			doStageUpdate ();
		}
	}

	public void DrawStage(){

		float size = 10;
		MapSize = new Vector3 (size, 30, size);
		level = new entityStage (this,(int)size);
		Tileset = new  List<List<List<entityBlock>>>();
		DoStage(new propertyImageMap(gDat,"testland"));

	}

	public float GetSpeed()
	{
		return 1f;
	}

	public float GetStageGravity()
	{
		return 10f;
	}

	public float chunk = 10;
	public void DoStage(propertyMap mapData)
	{
		MapSize = new Vector3 (mapData.GetWidth (), Mathf.Max(MapSize.y,mapData.GetMaxElevation ()), mapData.GetWidth ());
		foreach (propertyBlock block in mapData.toMapData()) {
			AddBlock (block,false);

		}
		GameObject Stage = new GameObject ();
		Stage.name = "Stage";

		Map = new entityChunk [Mathf.CeilToInt(MapSize.x/chunk),Mathf.CeilToInt(MapSize.y/chunk),Mathf.CeilToInt(MapSize.z/chunk)];
		for (float iX = 0; iX < MapSize.x; iX += chunk) {
			for (float iY = 0; iY < MapSize.y; iY += chunk) {
				for (float iZ = 0; iZ < MapSize.z; iZ += chunk) {
					Map[Mathf.FloorToInt(iX/chunk),Mathf.FloorToInt(iY/chunk),Mathf.FloorToInt(iZ/chunk)]= (new entityChunk (this, new Vector3 (iX, iY, iZ), chunk));
					Map [Mathf.FloorToInt (iX / chunk), Mathf.FloorToInt (iY / chunk), Mathf.FloorToInt (iZ / chunk)].MapDisplay.transform.SetParent (Stage.transform);
					needsUpdate.Add (Map[Mathf.FloorToInt(iX/chunk),Mathf.FloorToInt(iY/chunk),Mathf.FloorToInt(iZ/chunk)]);
				}
			}
		}


		WaterLevel=  CreatePlane.CreateSprite (1, 1, CreatePlane.Orientation.Horizontal, CreatePlane.AnchorPoint.Center, false, Sprite.Create (Texture2D.whiteTexture, new Rect (0, 0, 1, 1), Vector2.zero), "WaterLevel");

		WaterLevel.GetComponent<MeshRenderer> ().material = Resources.Load<Material> ("Materials/Water");
		WaterLevel.GetComponent<MeshRenderer> ().material.color = getWaterColor ();

		WaterLevel.isStatic = true;
		WaterLevel.transform.SetParent (Stage.transform);
		WaterLevel.transform.localScale *= MapSize.x*5;
		WaterLevel.transform.localPosition = new Vector3(MapSize.x/2,0,MapSize.z/2);
	}

	public Color getWaterColor()
	{
		return Color.blue;
	}

	public void extendStage()
	{

		entityChunk[,,] tempMap = new entityChunk [Map.GetLength(0),Map.GetLength(1),Map.GetLength(2)+1];
		for (float iX = 0; iX < MapSize.x; iX += chunk) {
			for (float iY = 0; iY < MapSize.y; iY += chunk) {
				int iZ = Map.GetLength (2);
				tempMap[Mathf.FloorToInt(iX/chunk),Mathf.FloorToInt(iY/chunk),iZ]= (new entityChunk (this, new Vector3 (iX, iY, iZ*chunk), chunk));
				needsUpdate.Add (tempMap[Mathf.FloorToInt(iX/chunk),Mathf.FloorToInt(iY/chunk),iZ]);
				}
			}

	Map = tempMap;
	}

	public void doStageUpdate()
	{
		foreach (entityChunk Zim in needsUpdate) {
			Zim.hasUpdated = false;
		}
			foreach (entityChunk Zim in needsUpdate) {
			if (!Zim.hasUpdated) {
				Zim.Draw ();
				Zim.hasUpdated = true;
			}
		}
		needsUpdate.Clear ();
	}
	public List<entityActor> GetActors()
	{
		return gamePlayers;
	}

	public entityActor GetActorAtTile(Vector3 pos, entityActor self)
	{
		foreach (entityActor Zim in GetActors()) {
			if (Zim!=self && Zim.isAtTile(pos)
			){
				return Zim;
				}
		}
		return null; 
	}

	/*public List<entityActor> GetNeighboringActors(Vector3 vPos, entityActor self)
	{
		return new List<entityActor> {

			GetActorAtTile(vPos+new Vector3(0,1,0),self),
			GetActorAtTile(vPos+new Vector3(0,0,1),self),
			GetActorAtTile(vPos+new Vector3(1,0,0),self),
			GetActorAtTile(vPos+new Vector3(0,0,-1),self),
			GetActorAtTile(vPos+new Vector3(-1,0,0),self),
			GetActorAtTile(vPos+new Vector3(0,-1,0),self),
			};

	}*/
	public List<bool> GetNeighboringActors(Vector3 vPos, entityActor self)
	{
		List<bool> Value = new List<bool> {false,false,false,false,false,false};
		List<Vector3> tiles = new List<Vector3> {
			vPos + new Vector3 (0, 1, 0),
			vPos + new Vector3 (0, 0, 1),
			vPos + new Vector3 (1, 0, 0),
			vPos + new Vector3 (0, 0, -1),
			vPos + new Vector3 (-1, 0, 0),
			vPos + new Vector3 (0, -1, 0)
		};

		foreach (entityActor Zim in GetActors()) {
			if (Zim!=self && Zim.Collides()){
				for (int Gir=0; Gir<tiles.Count;Gir++)
				{
					if (Zim.isAtTile (tiles[Gir])) {
						Value [Gir] = true;
					}
				}
				}
		}
		return Value;

	}

	public void AddBlock(propertyBlock data,bool instant)
	{
		AddBlock(new entityBlock(this,data),instant);
	}

	public void AddBlock(int iX,int iY, int iZ, Color32 color,bool instant)
	{
		AddBlock(new entityBlock(this,new Vector3(iX,iY,iZ),color),instant);
	}
			public void AddBlock(entityBlock block,bool instant)
	{
		while (Tileset.Count <= block.vPos.x) {
			Tileset.Add (new List<List<entityBlock>> ());
		}
		while (Tileset[(int)block.vPos.x].Count <= block.vPos.y) {
			Tileset[(int)block.vPos.x].Add (new List<entityBlock>());
		}
		while (Tileset[(int)block.vPos.x][(int)block.vPos.y].Count <= block.vPos.z) {
			Tileset[(int)block.vPos.x][(int)block.vPos.y].Add (null);
		}
		Tileset [(int)block.vPos.x][(int)block.vPos.y][(int)block.vPos.z] = block;
	}

	public entityBlock GetBlockAt(Vector3 Pos)
	{
		int x = Mathf.FloorToInt (Pos.x);
		int y = Mathf.FloorToInt (Pos.y);
		int z = Mathf.FloorToInt (Pos.z);

		if (x<0 || y<0 || z<0 ||
			Tileset.Count <= x  ||
			Tileset [x].Count <= y ||
			Tileset [x][y].Count <= z)
		{
			return null;
		}

		return GetBlockAt (x,y,z);
	}

	public entityBlock GetBlockAt(int iX,int iY, int iZ)
	{
		return Tileset [iX][iY][iZ];
	}

	public void UpdateChunk(entityChunk data)
	{
		needsUpdate.Add(data);
	}

	public entityChunk GetChunkAt(Vector3 point)
	{
		return GetChunkAt(point.x,point.y,point.z);
	}

	public entityChunk GetChunkAt(float iX,float iY, float iZ)
	{
		Vector3 realPos = new Vector3 (iX, iY, iZ) / chunk;


		if (realPos.x<0 || realPos.y<0 || realPos.z<0 ||
			Map.GetLength (0) <= realPos.x  ||
			Map.GetLength (1) <= realPos.y)
		{
			return null;
		} else if (Map.GetLength (2) <= realPos.z) {
			extendStage ();
		}

		return Map [Mathf.FloorToInt(realPos.x),Mathf.FloorToInt(realPos.y),Mathf.FloorToInt(realPos.z)];
	}

	public entityBlock[,,] GetMapPart(Vector3 start,Vector3 end)
	{
		entityBlock[,,] resutl = new entityBlock[(int)(end.x-start.x),(int)(end.y-start.y),(int)(end.z-start.z)];

		for (float iX=start.x; iX<end.x;iX++)
		{

			for (float iY=start.y; iY<end.y;iY++)
			{

				for (float iZ=start.z; iZ<end.z;iZ++)
				{
					resutl[(int)(iX-start.x),(int)(iY-start.y),(int)(iZ-start.z)] = GetBlockAt(new Vector3(iX,iY,iZ));
				}
			}
		}
		return resutl;
	}

	public List<Vector3> GenerateSpawnPoints()
	{
		List<Vector3> Spawns = new List<Vector3> ();
		foreach (List<List<entityBlock>> Panty in Tileset) {

			foreach (List<entityBlock> Stocking in Panty) {

				foreach (entityBlock Zim in Stocking) {
					if (Zim != null && canFitActor (Zim,3,null)) {
						Spawns.Add (Zim.vPos + new Vector3 (0, 1, 0));
					}
				}
			}
		}
		return Spawns;
	}

	public float GetGravityModifier()
	{
		if (Selection.HasState (propertyModifier.modstate.gravity)) {
			return 1f / 2f;
		}
		return 1f;
	}

	public void SpawnEntity(entityBase unit)
	{
		gameEntities.Add (unit);
		if (unit.isPlayer ()) {
			gamePlayers.Add ((entityActor)unit);
		}
	}

	public float GetWaterLevel()
	{
		return WaterLevel.transform.position.y;
	}
	public bool canFitActor(entityBlock block,int h,entityActor self)
	{
		return canFitActor (block.vPos, h,self);
	}
		public bool canFitActor(Vector3 vPos,int h,entityActor self)
		{
		for (int I = 0; I < h; I++) {
			if (GetBlockAt (vPos + new Vector3 (0, h-I, 0)) != null || consideredForCollision(GetActorAtTile(vPos + new Vector3 (0, h-I, 0),self))) {
				return false;
			}
		}
		return true;
	}

	public List<entityMesh> GetAllMeshesInRadius(Vector3 center, float radius)
	{
		List<entityMesh> tot = new List<entityMesh> ();

		foreach (entityActor Panty in GetActors()) {
			if (!tot.Contains(Panty.Skeleton)) {
				tot.Add (((entityActor)Panty).Skeleton);
			}
		}
		tot.RemoveAll (delegate(entityMesh obj) {
			return  (obj.GetCenterOfMass() - center).magnitude > radius;	
		});

		return tot;
	}

	public List<entityActor> GetAllGuysInCircle(Vector3 center, float radius)
	{
		List<entityActor> tot = new List<entityActor> ();
		foreach (entityActor Panty in GetActors()) {
			if (Panty.isRelevant() && new Vector2 (Panty.Position.x - center.x, Panty.Position.z - center.z).magnitude <= radius) {
				tot.Add ((entityActor)Panty);
			}
		}
		return tot;
	}

	public List<entityActor> GetAllGuysInRadius(Vector3 center, float radius)
	{
		List<entityActor> tot = new List<entityActor> ();
		foreach (entityActor Panty in GetActors()) {
			if (Panty.isRelevant() && (Panty.Position-center).magnitude <= radius) {
				tot.Add ((entityActor)Panty);
			}
		}
		return tot;
	}

	public List<entityBlock> GetAllBlocksInCircle(Vector3 center, float radius,bool relevant)
	{
		List<entityBlock> tot = new List<entityBlock> ();

		for (float iX = center.x - radius; iX <= center.x + radius; iX++) {

			for (float iY = center.y - radius; iY <= center.y + radius; iY++) {

				for (float iZ = center.z - radius; iZ <= center.z + radius; iZ++) {

					Vector3 pos = new Vector3 (iX, iY, iZ);
					if ((pos - center).magnitude < radius && GetBlockAt (pos) != null) {
						tot.Add (GetBlockAt (pos));
					}
				}
			}
		}

		if (relevant) {
			tot.RemoveAll (delegate(entityBlock obj) {
				return !obj.isRelevant();
			});
		}

		return tot;
	}

	public bool consideredForCollision(entityActor other)
	{
		return (other!=null && other.Collides());
	}


	public void RallyNearbyGuys(entityActor self,Vector3 center,Vector3 position,float radius)
	{
		foreach (entityActor panty in GetAllGuysInCircle(center,radius)) {
			//am I allied or enemy TBD after sides
			if (panty != self) {
				panty.lookAtPoint (position);
			}
		}
	}
}