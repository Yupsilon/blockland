﻿using System;
using UnityEngine;
using System.Collections.Generic;	
public class propertyMap
{

	public int Height=0;
	public List<float> ElevationData;

	public propertyMap ()
	{
		ElevationData= new List<float>();
	}
	public propertyMap(int GenSize)
	{
		Generate (GenSize);
	}


	public float GetMinElevation()
	{
		float Gir = GetMaxElevation();

		foreach (float Zim in ElevationData)
		{
			Gir = Mathf.Min(Zim,Gir);
		}
		return Gir;
	}

	public float GetMaxElevation()
	{
		float Gir = 0;

		foreach (float Zim in ElevationData)
		{
			Gir = Mathf.Max(Zim,Gir);
		}
		return Gir;
	}

	public virtual int GetWidth()
	{
		return Mathf.FloorToInt(Mathf.Sqrt (ElevationData.Count));
	}

	public float GetTileAt(int x, int y)
	{
		int index = y * GetWidth () + x;
		if (isOnMap(index)) {

			return ElevationData [index];
		}else{return 0;
		}
	}
	public bool isOnMap(int x,int y)
	{
		return isOnMap (y * GetWidth () + x);
	}
	public bool isOnMap(int index)
	{
		return (index >= 0 && index < ElevationData.Count);
	}
	public void SetTileAt(int x, int y, float v)
	{
		int index = y * GetWidth () + x;
		if (isOnMap(index) ){
			ElevationData [index] = v;
		}
	}

	public virtual void Generate(int Size)
	{
		ElevationData = new List<float> ();

		for (int Zim = 0; Zim < Size * Size; Zim++) {
			ElevationData.Add (0);
		}
	}

	public float[] GetTilesInRange(int X, int Y, int Rad)
	{
		List<float> rValue = new List<float>();
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);

				if (isOnMap(iX,iY) && Dist < Rad) {
					rValue.Add(GetTileAt(iX,iY));}
			}
		}
		return rValue.ToArray();
	}



	public float isInValue(float Value, float Min, float Max)
	{
		if (Value < Min || Value > Max) {
			return -1;
		}
		return (Value - Min) / (Max - Min);

	}

	public virtual Color GetColorAt(int iX,int iY, int iZ)
	{
		return Color.white;
	}

	public List<propertyBlock> toMapData()
	{
		List<propertyBlock> result = new List<propertyBlock> ();


		for (int iZ = 0; iZ < GetWidth(); iZ++) {
			for (int iX = 0; iX < GetWidth(); iX++) {
				float e = ElevationData [iZ * GetWidth () + iX];
				for (int iY = 0; iY <= e; iY++) {

					result.Add (new propertyBlock(new Vector3(iX,iY,iZ),GetColorAt(iX,iY,iZ)));

				} 
			}
		}
		return result;
	}

	public void SmoothenTile(int X,int Y,int dSize,bool diag)
	{int Gir = 0;
		float Zim = 0;

		int[][] NeighData = 
			new int[][] {
			new []{ -1, 0 },
			new []{ 1, 0 },
			new []{ 0, 1 },
			new []{ 0, -1 },
		};

		if (diag) {
			NeighData = 
				new int[][] {
				new []{ 1, 1 },
				new []{ 1, -1 },
				new []{ -1, 1 },
				new []{ -1, -1 },
			};
		}

		for (int i = 0; i < NeighData.Length; i++) {
			int[] temp = NeighData[i];
			int randomIndex = UnityEngine.Random.Range(i, NeighData.Length);
			NeighData[i] = NeighData[randomIndex];
			NeighData[randomIndex] = temp;
		}
		foreach (int[] Xerxes in NeighData)
		{

			if (GetTileAt (X + dSize*Xerxes[0], Y + dSize*Xerxes[1]) > 0) {
				Zim += GetTileAt (X + dSize*Xerxes[0], Y + dSize*Xerxes[1]);
				Gir++;
			}
		}


		SetTileAt (X  , Y , Zim/Gir);

	}
}

