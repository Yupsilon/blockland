﻿using System;
using UnityEngine;

	public class propertyWeapon
	{
	public string name;
	public string animation;
	public int type=0;//charged
	public bool enabled=true;

	//accuracy
	public Vector4 accuracyRating=Vector4.zero;
	public Vector2 accuracyData = Vector2.zero;

	public static int weapontype_singleshot_charged = 0;
	public static int weapontype_singleshit_instant = 1;
	public static int weapontype_singleshit_multishot_sepparate = 2;
	public static int weapontype_singleshit_multishot_single = 3;
	public static float accuracyrating = 50;

	public propertyWeapon(int wID)
	{
		switch (wID) {
		case 0: //bazooka
			name = "grenade";
			animation = "thrown";
			accuracyData = new Vector2 (0, 6);

			break;
		}
	}

	public void AccountAccuracy()
	{
		if (accuracyRating.x == accuracyRating.z && accuracyRating.y == accuracyRating.w) {
			accuracyRating.z = Mathf.Round ((UnityEngine.Random.value * 2 - 1) * accuracyrating);
			accuracyRating.w = Mathf.Round ((UnityEngine.Random.value * 2 - 1) * accuracyrating);
		} else {
			if (new Vector2 (accuracyRating.z - accuracyRating.x, accuracyRating.w - accuracyRating.y).magnitude < 1) {
				accuracyRating.x = accuracyRating.z;
				accuracyRating.y = accuracyRating.w;
			} else {

				accuracyRating.x += (accuracyRating.z-accuracyRating.x)/10f;
				accuracyRating.y += (accuracyRating.w-accuracyRating.y)/10f;
			}
		}
	}

	public Quaternion accountAimBonuses (Vector3 AimAngle,float nArms)
	{

		float maxAim = accuracyData.x + accuracyData.y * 2 -accuracyData.y * nArms ;

		return Quaternion.Euler (AimAngle.y + accuracyRating.x / accuracyrating * maxAim, AimAngle.x + accuracyRating.y / accuracyrating * maxAim, AimAngle.z);
	}

	public entityProjectile Fire(GameMission game,entityActor owner, Vector3 AimAngle, float chargeLevel)
	{
		if (owner == null || owner.Skeleton == null || owner.Skeleton.weapon==null) {
			return null;
		}
		entitySkeleton origin = owner.Skeleton.weapon.GetLimbByName (name + "_payload");
		if (origin != null) {

			enabled = false;
			entityGore body = new entityGore (owner, origin);
			body.Skeleton.SetRagdoll (0, 0);

			GameObject center = new GameObject();
			center.transform.position = body.Skeleton.getCenter ().gameObject.transform.position;
			center.transform.rotation = body.Skeleton.getCenter ().gameObject.transform.rotation;
			body.Skeleton.LinkToGameObject (center);
			center.name = "projectile_" + name;

			body.Dispose (false);

			float minFire = 1;
			float maxFire = 75;


			float downScale = 100f;
			Vector3 fireDir = accountAimBonuses(AimAngle,owner.Skeleton.getHands ()) * Vector3.forward;//(origin.gameObject.transform.rotation*Quaternion.Euler(90,-90,0)) * Vector3.forward ;

			 
			entityProjectile pData = new entityProjectile (game,center , fireDir* (minFire+(maxFire-minFire)*chargeLevel)/downScale, 0.1f, 20,true, "","shockwave");
			game.SpawnEntity (pData);

			pData.Position = center.transform.position;
			pData.Rotation = center.transform.rotation.eulerAngles;
			pData.baseRotation = center.transform.rotation.eulerAngles;
			pData.groundFriction = 0.08f;
			pData.Size *= 0.4f;

			pData.collide = delegate(entityProjectile self, Vector3 dir) {

				float bounceFactor = 0.5f;

				if (dir.y>0)
				{
					self.Velocity.y*=-dir.y*bounceFactor;
				} else if (dir.y<0)
				{
					self.Velocity.y*=dir.y*bounceFactor;
				} 
				if (dir.x>0)
				{
					self.Velocity.x*=-dir.x*bounceFactor;
				} else if (dir.x<0)
				{
					self.Velocity.x*=dir.x*bounceFactor;
				} 
				if (dir.z>0)
				{
					self.Velocity.z*=-dir.z*bounceFactor;
				} else if (dir.z<0)
				{
					self.Velocity.z*=dir.z*bounceFactor;
				}
			};
			pData.die = delegate(entityProjectile self, Vector3 dir) {
				new propertyExplosion (game, self.Position, 3, 4,4,5, owner.Skeleton.Data.LoadTexture(name + "_explosion"));
				
			};
			pData.createThiker (
					delegate {
						
						game.RallyNearbyGuys (null, pData.Position,  pData.Position, 4);
						return 0.1f;
					},
					delegate {
						return 0;	
					}
			);
			Timer.Create (name + "_lifetime", 5f, delegate(Timer original) {
				pData.Die(true);
				return 0;
			});
			origin.Die (false);
		}
		return null;
	}
}