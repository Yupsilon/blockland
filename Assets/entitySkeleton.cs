﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class entitySkeleton
{
	public string name;
	public float lastRagdollTime=0f;
	public GameObject gameObject;
	public int liveState=0;
	public int Hierarchy = 0;
	public List<string> flags;
	public float Damage = 0;

	public entityMesh MeshData;
	public List<Sprite> emotions=new List<Sprite>();
	public List<entitySkeleton> neighbors=new List<entitySkeleton>();
	public List<List<LimbAnimation>> AnimData = new List<List<LimbAnimation>>();
	public LimbBodyData Data;
	public entitySpecialEffect particleEffect;

	public Vector3 Rotation = Vector3.zero;
	public Vector3 Position = Vector3.zero;
	public Vector4 lastPosition = Vector4.zero;
	public Vector3 Scale = Vector3.one;

	public MeshRenderer Display;	

	public BoneAnimationOverride OrderNew= new BoneAnimationOverride();
	public BoneAnimationOverride OrderLast= new BoneAnimationOverride();
	public BoneAnimationOverride Override = null;

	public entitySkeleton(){}
	public void SetParent(entitySkeleton Parent)
	{
		Parent.neighbors.Add (this);
		neighbors[0]= (Parent);
		Hierarchy = Parent.Hierarchy + 1;
	}
	public entitySkeleton(GameObject parent,LimbBodyData LimbData,entityMesh MeshData)
	{
		Data = LimbData;
		this.MeshData = MeshData;
		name=LimbData.LimbName;

		AnimData = new List<List<LimbAnimation>> ();
		foreach (LimbAnimationData Panty in MeshData.Data.Animations) {
			List<LimbAnimation> lAnimation = new List<LimbAnimation> ();

			foreach (LimbAnimation Stocking in Panty.AnimationData)
			{
				if (Stocking.BoneName==LimbData.LimbName)
				{
					lAnimation.Add(Stocking);
				}
			}
			AnimData.Add (lAnimation);
		}

		neighbors.Add (null);
		flags = LimbData.LimbType;

		Position=new Vector3(0,0,LimbData.CenterPoint.z);

		if (MeshData!=null && MeshData.GetLimbByName(LimbData.LimbParent) != null) {
			SetParent (MeshData.GetLimbByName (LimbData.LimbParent));
		}

		gameObject =  GameObject.CreatePrimitive (PrimitiveType.Cube);



			gameObject.transform.parent = parent.transform;
		gameObject.name = name;
		gameObject.AddComponent<componentSkeleton> ().parent=this;

			GameObject.Destroy (gameObject.GetComponent<BoxCollider> ());

		gameObject.AddComponent<CapsuleCollider> (); 
		gameObject.GetComponent<CapsuleCollider> ().radius = Mathf.Min(1,Data.Dimensions.y/Data.Dimensions.z,Data.Dimensions.y/Data.Dimensions.	x)*3/5f;
		gameObject.GetComponent<CapsuleCollider> ().height = 1f;
		gameObject.tag = "player_limb";

			if (Data.Dimensions.x > Data.Dimensions.y) {
				gameObject.GetComponent<CapsuleCollider> ().direction = 0;
			}
			if (Data.Dimensions.z > Data.Dimensions.y) {
				gameObject.GetComponent<CapsuleCollider> ().direction = 2;

		}

		if (Data.LimbType.Contains( "head")) {
				//do face
				Texture2D faceTexture = MeshData.Data.LoadTexture ("face");

				if (faceTexture != null && faceTexture.name == "face") {

					emotions = new List<Sprite> ();
					for (float Zim = 0; Zim < faceTexture.width / faceTexture.height; Zim++) {
						emotions.Add (Sprite.Create (faceTexture, new Rect (faceTexture.height * Zim, 0, faceTexture.height, faceTexture.height), Vector2.zero));
					}

					GameObject emotion = CreatePlane.CreateSprite (1, 1, CreatePlane.Orientation.Horizontal, CreatePlane.AnchorPoint.Center, false, emotions [0], "face");
					CreatePlane.ApplySprite (emotion.GetComponent<MeshRenderer> (), emotions [0]);


					emotion.transform.SetParent (gameObject.transform);
					emotion.transform.localPosition = new Vector3 (0.501f, 0, 0);
					emotion.transform.localRotation = Quaternion.Euler (-90, 0, -90);
					emotion.transform.localScale = Vector3.one;
				}
			}

			gameObject.transform.localPosition = GetAbsolutePosition ();
			gameObject.transform.localRotation = Quaternion.Euler (Vector3.zero);
		gameObject.transform.localScale = GetScale (true);

			gameObject.AddComponent<Rigidbody> ();
			gameObject.GetComponent<Rigidbody> ().drag = 0.2f;

		if (GetParent () != null) {

			configureJoint (GetParent().gameObject);

		} 
		if (Data.LimbType.Contains( "torso")) {
				string pardner = "r" + Data.LimbName.Substring (1);
				if (
					pardner != Data.LimbName && MeshData.GetLimbByName (pardner) != null) {
					FixedJoint Link = gameObject.AddComponent <FixedJoint> ();
					Link.axis = Vector3.one;
					Link.autoConfigureConnectedAnchor = true;
					Link.connectedBody = MeshData.GetLimbByName (pardner).gameObject.GetComponent<Rigidbody> ();
				} 
		} 

		if (!Data.LimbType.Contains( "attach")) {
			
			Data.DrawMesh (this);
			bool teamColor = false;
			Texture2D boxTexture;
			if (MeshData.Data.LoadTexture (Data.LimbName + "_teamColor", true) != null) {

				boxTexture = MeshData.Data.LoadTexture (Data.LimbName + "_teamColor",true);
				teamColor = true;
			} else {

				 boxTexture = MeshData.Data.LoadTexture (Data.LimbName,true);
			}

			if (!Data.LimbType.Contains( "voxel")) {
				//PLAYER TEAM /COLOR
				gameObject.GetComponent<MeshRenderer> ().material.mainTexture = boxTexture;
				if (teamColor) {
					gameObject.GetComponent<MeshRenderer> ().material.color = MeshData.teamColor;
				}
			}
		
		} else {
			gameObject.GetComponent<MeshRenderer> ().enabled = false;
		}

		changeState (0,0);
	}

	public bool isStatic()
	{
		return lastRagdollTime<Time.time && ((GetWorldPosition() - new Vector3(lastPosition.x,lastPosition.y,lastPosition.z)).magnitude*100 < 1f);
	}

	public void Update(PlayerAnimData Anim)
	{	
		float AnimationLag = 0;

		if (Anim.isLagging()) {

			AnimationLag = Anim.GetLagPercentage();

			Position = OrderLast.pos + (OrderNew.pos - OrderLast.pos) * AnimationLag;
			Rotation = OrderLast.rot + new Vector3(
				Mathf.DeltaAngle(OrderNew.rot.x, OrderLast.rot.x),
				Mathf.DeltaAngle(OrderNew.rot.y, OrderLast.rot.y),
				Mathf.DeltaAngle(OrderNew.rot.z, OrderLast.rot.z)
			)* AnimationLag;
			Scale = OrderLast.scale + (OrderNew.scale - OrderLast.scale) * AnimationLag;

		}
		else 
			{
			
			DrawFrame (Anim);

			 Position = OrderNew.pos;
			 Rotation = OrderNew.rot;
			 Scale = OrderNew.scale;

			if (!locked && Override != null) {
				if (Override.rot != Vector3.zero) {
					Rotation = Override.rot;
				}
			}

		}

		if (GetParent () == null) {
			gameObject.transform.localPosition = Position;
		}
		gameObject.transform.localRotation = Quaternion.Euler (GetAbsoluteRotation(true));
		gameObject.transform.localScale = GetScale (true);
	}

	public void Reset(bool soft)
	{
		OrderNew = new BoneAnimationOverride (Vector3.zero,Vector3.zero,Vector3.one);
		OrderLast = new BoneAnimationOverride (Position, Rotation, Scale);
		ChangeFace (0);
		if (!soft) {
			Override = null;
		}
	}
	public bool locked = false;

	public void DrawFrame(PlayerAnimData animation)
	{
		DrawFrame(animation,animation.GetDonePercentage ());
	}
		public void DrawFrame(PlayerAnimData animation,float Frame)
		{
		float cFrame = 1+Frame * (animation.animation.AnimationFrames-1);
		locked = true;

		string checkName = Data.LimbName;
		OrderNew = new BoneAnimationOverride();

		foreach (LimbAnimation Panty in animation.animation.AnimationData) {
			if (Panty.BoneName == checkName) {
				if (Panty.StartFrame <= cFrame) {
					if (Panty.Type == LimbAnimation.type_target) {
						if (Frame > Panty.StartFrame && Frame < Panty.EndFrame) {
							FacePoint (MeshData.followTarget, animation.GetDuration());
						}
					} else if (Panty.Type == LimbAnimation.type_emote) {
						ChangeFace ((int)Panty.Data.x);
					} else if (Panty.Type == LimbAnimation.type_locktracking) {
						if (locked) {
							locked = Frame <= Panty.StartFrame && Frame >= Panty.EndFrame;
						}
					} else {
						float Delta = Mathf.Clamp ((cFrame - Panty.StartFrame) / (Panty.EndFrame - Panty.StartFrame), 0, 1);
						if (Panty.Type == LimbAnimation.type_move) {
							OrderNew.pos = OrderNew.pos + (Panty.Data - OrderNew.pos) * Delta;
						} else if (Panty.Type == LimbAnimation.type_rotate) {
							OrderNew.rot = OrderNew.rot + (Panty.Data - OrderNew.rot) * Delta;
						} else if (Panty.Type == LimbAnimation.type_scale) {
							OrderNew.scale = OrderNew.scale + (Panty.Data - OrderNew.scale) * Delta;
						} else if (Panty.Type == LimbAnimation.type_rotate_relative) {

							Quaternion quatA = Quaternion.Euler (Panty.Data-GetParent().OrderNew.rot);
							Quaternion quatB = Quaternion.Euler (GetParent().Rotation-GetParent().OrderNew.rot);

							Quaternion final = quatB*quatA;

							OrderNew.rot = OrderNew.rot + (final.eulerAngles - OrderNew.rot) * Delta;
						} /*else if (Panty.Type == LimbAnimation.type_scale_absolute) {
							
						}*/
					}
				}
			}
		}
	}

	public void ChangeFace (int emote)
	{
		if (Data.LimbType.Contains( "head") && emotions.Count>emote) {
			if (gameObject.transform.Find ("face") != null) {
				CreatePlane.ApplySprite (gameObject.transform.Find ("face").GetComponent<MeshRenderer> (), emotions [emote]);
			}
		}
	}

	public void PhysixUpdate(GameMission game)
	{
		Vector3 world = GetWorldPosition ();

		if (MeshData.underWater && world.y < game.GetWaterLevel ()) {

			if (lastPosition.y > game.GetWaterLevel () && UnityEngine.Random.value < 0.15f) {
					Vector3 splashPos = GetWorldPosition ();
				SpecialEffects.ExplosionShockwave (game, SpecialEffects.LoadTexture(game.gDat,"ripple"), 50f,new Vector3 (
						splashPos.x, game.GetWaterLevel () + 0.01f, splashPos.z
					), 0.4f, 3f,1/2f, new Color (1, 1, 1, 0.4f));
				}



			gameObject.GetComponent<Rigidbody> ().useGravity = false;
			gameObject.GetComponent<Rigidbody> ().drag = 5f;
			gameObject.GetComponent<Rigidbody> ().AddForce (new Vector3 (
				0,
				-Mathf.Max (lastPosition.y-game.GetWaterLevel () , -1)*10),
				0);
		} else {

			gameObject.GetComponent<Rigidbody> ().useGravity = true;
			gameObject.GetComponent<Rigidbody> ().drag = 0.2f;
		}

		if (lastPosition.w+1/2f<Time.time){
			lastPosition = new Vector4(world.x,world.y,world.z,Time.time);
		}
	}

	public Vector3 GetWorldRotation()
	{
		return gameObject.transform.rotation.eulerAngles;
	}

	public Vector3 GetWorldPosition()
	{
		return gameObject.transform.position;
	}

	public List<string> GetCategory()
	{
		return Data.LimbType;
	}	

	public Vector3 GetPosition()
	{

		if (GetParent () != null && !GetCategory().Contains( "weapon")) {

			return new Vector3 (
				(GetParent ().GetScale (true).x * Data.CenterPoint.x),
				(GetParent ().GetScale (true).y * Data.CenterPoint.y),
				(GetParent ().GetScale (true).z * Data.CenterPoint.z)
			);
		}
		return Position;
	}

	public Vector3 GetCenter()
	{
		return new Vector3 (
			(GetScale (true).x * Data.AttachPoint.x),
			(GetScale (true).y * Data.AttachPoint.y),
			(GetScale (true).z * Data.AttachPoint.z)
		);

	}

	public Vector3 GetAbsolutePosition()
	{
		Vector3 Start =	GetPosition();
	

		if (GetParent () != null) {

			Start = GetParent ().GetAbsolutePosition () + GetPosition ();
		  
		}
		Vector3 delta =	GetCenter ();


		return new Vector3(
			Start.x+delta.x,
			Start.y+delta.y,
			Start.z+delta.z
		);

	}


	public entitySkeleton GetParent()
	{
		return neighbors [0];
	}

	Vector3 tempScale=Vector3.one;
	public Vector3 GetScale(bool real)
	{
		if (real) {
			return new Vector3(
				Data.Dimensions.x*GetScale(false).x,
				Data.Dimensions.y*GetScale(false).y,
				Data.Dimensions.z*GetScale(false).z
			);
		}
		if (GetParent () != null) {
			tempScale = new Vector3(
				Scale.x*GetParent ().tempScale.x,
				Scale.y*GetParent ().tempScale.y,
				Scale.z*GetParent ().tempScale.z
			);
			return tempScale;
		}
		return Scale;
	}

	public Vector3 GetAbsoluteRotation(bool local)
	{
		if (GetParent () == null) {
			return Rotation;
		}
		Vector3 parentRotation = GetParent ().GetAbsoluteRotation (false);
		return Rotation+parentRotation;
	}

	public void changeState(int ToWhat, float minTime)
	{
		if (minTime > 0) {
			lastRagdollTime = Time.time + minTime;

		}

		if (gameObject.GetComponent<CapsuleCollider> () != null) {
			switch (ToWhat ) {
			case 0://static
			
				gameObject.GetComponent<CapsuleCollider> ().enabled = false;
				gameObject.GetComponent<Rigidbody> ().detectCollisions = false;
				gameObject.GetComponent<Rigidbody> ().useGravity = false;
				gameObject.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;

				if (GetParent () == null) {
					gameObject.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;
				}
				return;

			case 1://full ragdoll

				gameObject.GetComponent<Rigidbody> ().detectCollisions = false;
				gameObject.GetComponent<Rigidbody> ().useGravity = true;
				gameObject.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.None;

				if (liveState == 0) {

					gameObject.GetComponent<CapsuleCollider> ().enabled = true;

				}
				return;

			case 2://static, collision enabled

				gameObject.GetComponent<Rigidbody> ().detectCollisions = true;
				gameObject.GetComponent<Rigidbody> ().useGravity = false;
				gameObject.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;

				if (liveState == 0) {

					gameObject.GetComponent<CapsuleCollider> ().enabled = true;

				}
				return;
			}
		}
	}

	public void FacePoint (Vector3 Point,float interval)
	{
		Vector3 center = GetWorldPosition()-Point;

		if (Data.LimbType.Contains( "hand")) {

			List<entitySkeleton> FamilyTree = GetFamilyTree () ;
			FamilyTree.Reverse ();

			{
				entitySkeleton Bone = MeshData.GetLimbByName (name [0] + "arm");

				Quaternion quatA = new Quaternion (
					0,
					0,
					Mathf.Sin(Mathf.Atan2 (-center.y, new Vector2(center.x,center.z).magnitude)/2f),
					Mathf.Cos(Mathf.Atan2 (-center.y, new Vector2(center.x,center.z).magnitude)/2f)
				                   );
				Quaternion quatB = Quaternion.Euler (Bone.OrderNew.rot);

				Quaternion final = quatA*quatB;

				Bone.OverrideRotation (final.eulerAngles,interval);
				//if (locked)
				{
					quatB = Quaternion.Euler (OrderNew.rot);
					OverrideRotation ((quatA).eulerAngles-final.eulerAngles+quatB.eulerAngles, interval);
				}
			}
		} else {
			if (getOwner ().Animation != null) {
				DrawFrame (getOwner ().Animation);
			}
			float radAngle1 = Mathf.Atan2 (center.x, center.z);
			float radAngle2 = Mathf.Atan2 (center.y, new Vector2(center.x,center.z).magnitude);
			OverrideRotation (new Vector3(
				OrderNew.rot.x,
				OrderNew.rot.y,//radAngle1 * 180f / Mathf.PI-getOwner().Rotation.x+90,
				-radAngle2*180f/Mathf.PI
			),interval);
		}
	}

	public void OverrideRotation(Vector3 angle, float dur) {

		if (Override != null && Override.rot == angle) {
			return;
		}

		Override = new BoneAnimationOverride();
		/*float Max = Data.RotationLimits.x;
		float Min = Data.RotationLimits.y;*/

		Override.rot = angle;
		Override.pos = new Vector3 (Time.time, dur, 0);
	}

	public List<entitySkeleton> GetFamilyTree()
	{
		List<entitySkeleton> FamilyTree = new List<entitySkeleton> ();

		entitySkeleton Bone = GetParent ();
		while (Bone != null) {
			FamilyTree.Add (Bone);
			Bone = Bone.GetParent ();
		}
		FamilyTree.Reverse ();

		return FamilyTree;
	}
	public List<entitySkeleton> GetChildren(bool includeself)
	{
		List<entitySkeleton> FamilyTree = new List<entitySkeleton> ();
		if (includeself) {
			FamilyTree.Add (this);
		}
		foreach (entitySkeleton Panty in MeshData.Bones) {

			if (!Panty.flags.Contains("torso") && Panty.GetParent()==this ) {
				FamilyTree.AddRange (Panty.GetChildren (true));
			}
		}

		return FamilyTree;
	}

	public bool isMortal()
	{
		return !Data.LimbType.Contains( "immortal");
	}

	public entitySkeleton Copy(entitySkeleton newParent,entityMesh mesh)
	{
		entitySkeleton newBone = new entitySkeleton ();

		newBone.MeshData = mesh;
		newBone.name = name;
		newBone.liveState = 2;
		newBone.neighbors = new List<entitySkeleton>{ newParent };
		newBone.Data = Data;


		newBone.gameObject = GameObject.Instantiate (gameObject);

		//newBone.gameObject =  GameObject.CreatePrimitive (PrimitiveType.Cube);
		newBone.gameObject.name=name+" gore";

		/*GameObject.Destroy (newBone.gameObject.GetComponent<BoxCollider> ());
		newBone.gameObject.AddComponent<CapsuleCollider> ();
		newBone.gameObject.GetComponent<CapsuleCollider> ().radius=1f/2f;
		newBone.gameObject.GetComponent<CapsuleCollider> ().height=1;

		if (Data.Dimensions.x > Data.Dimensions.y) {
			newBone.gameObject.GetComponent<CapsuleCollider> ().direction = 0;
		}
		if (Data.Dimensions.z > Data.Dimensions.y) {
			newBone.gameObject.GetComponent<CapsuleCollider> ().direction = 2;
		}

		if (Data.LimbType.Contains( "head")) {
			//do face
			if (gameObject.transform.Find ("face") != null) {

				GameObject oldFace = gameObject.transform.Find ("face").gameObject;

				newBone.emotions = emotions;

				GameObject emotion = CreatePlane.CreateSprite (1, 1, CreatePlane.Orientation.Horizontal, CreatePlane.AnchorPoint.Center, false, emotions[4], "face");

				emotion.transform.SetParent (newBone.gameObject.transform);
				emotion.transform.localPosition = oldFace.transform.localPosition;
				emotion.transform.localRotation = oldFace.transform.localRotation;
				emotion.transform.localScale = oldFace.transform.localScale;
			}
		}

		if (particleEffect != null) {
			particleEffect.gameObject = newBone.gameObject;
		}

		newBone.gameObject.transform.position = gameObject.transform.position;
		newBone.gameObject.transform.rotation = gameObject.transform.rotation;
		newBone.gameObject.transform.localScale = gameObject.transform.localScale;

		newBone.gameObject.AddComponent<Rigidbody>();
		newBone.gameObject.GetComponent<Rigidbody> ().drag = 0.2f;*/

		if (newParent != null && hasJoint()) {
			//newBone.configureJoint (newParent.gameObject);
			newBone.gameObject.GetComponent<ConfigurableJoint>().connectedBody=newParent.gameObject.GetComponent<Rigidbody> ();
		}		

		//newBone.gameObject.GetComponent<MeshFilter> ().mesh = gameObject.GetComponent<MeshFilter> ().mesh;
		//newBone.gameObject.GetComponent<MeshRenderer> ().material = gameObject.GetComponent<MeshRenderer> ().material;

		return newBone;
	}

	public bool hasJoint()
	{
		return gameObject.GetComponent<ConfigurableJoint> () != null || gameObject.GetComponent<FixedJoint> () != null;
	}

	public void configureJoint(GameObject newParent)
	{

			ConfigurableJoint Joint = gameObject.AddComponent <ConfigurableJoint> ();

			Joint.connectedBody = newParent.GetComponent<Rigidbody> ();
			Joint.autoConfigureConnectedAnchor = true;
			Joint.enableCollision = true;

			Joint.xMotion = ConfigurableJointMotion.Locked;
			Joint.yMotion = ConfigurableJointMotion.Locked;
			Joint.zMotion = ConfigurableJointMotion.Locked;

		if (Data.JointLimits != Vector2.zero) {
			Joint.angularXMotion = ConfigurableJointMotion.Limited;
			Joint.angularYMotion = ConfigurableJointMotion.Limited;
			Joint.angularZMotion = ConfigurableJointMotion.Limited;

			/*Joint.lowAngularXLimit = limitJoint (-Data.RotationLimits.x,0,0);
		Joint.highAngularXLimit = limitJoint (Data.RotationLimits.x,0,0);

		Joint.angularYLimit = limitJoint (Data.RotationLimits.x,0,0);
		Joint.angularZLimit = limitJoint (Data.RotationLimits.x,0,0);*/

			Joint.lowAngularXLimit = limitJoint (-Data.JointLimits.x, 0, 0);
			Joint.highAngularXLimit = limitJoint (Data.JointLimits.x, 0, 0);

			Joint.angularYLimit = limitJoint (Data.JointLimits.y, 0, 0);
			Joint.angularZLimit = limitJoint (Data.JointLimits.y, 0, 0);
		} else {
			Joint.angularXMotion = ConfigurableJointMotion.Locked;
			Joint.angularYMotion = ConfigurableJointMotion.Locked;
			Joint.angularZMotion = ConfigurableJointMotion.Locked;
		}
			if (Data.LimbType.Contains ("reverseY")) {
				Joint.anchor *= -1;
			}
			if (Data.LimbType.Contains ("cosmetic")) {
				Joint.enableCollision = false;
			}
	}

	public entityGore Die(bool gore)
	{
		return Die (gore, false);
	}

	public entityGore Die(bool gore,bool perma)
	{
		if (liveState == 0 || (liveState == 1 && perma)) {
			liveState = 1;
			foreach (entitySkeleton Zim in GetChildren(false)) {
				Zim.Die (false, false);
			}
			if (perma) {
				liveState = 2;
				gameObject.SetActive (false);
			} else {
				if (gameObject.transform.childCount > 0) {
					gameObject.transform.GetChild (0).gameObject.SetActive (false);
				}
				gameObject.GetComponent<MeshRenderer> ().enabled = (false);
			}
			if (gore) {

				//if (!cosmetic())
				SpecialEffects.ExplosionGeneric (getOwner ().game, SpecialEffects.LoadTexture(getOwner ().game.gDat,"splat"),50f, GetWorldPosition (), Mathf.Min (GetScale (true).x, GetScale (true).y, GetScale (true).z) / 10f, 0.1f, 0.3f,1, MeshData.teamColor);

				entityGore alGore = new entityGore (getOwner (), this);
				getOwner ().game.SpawnEntity (alGore);
				return alGore;
			}
		}
		return null;
	}

	public entityActor getOwner()
	{
		return MeshData.Owner;
	}

	public void TakeDamage(float Damage, float transmit)
	{
		if (Data.LimbType.Contains ("immortal")) {
			return;
		}
		Damage += Damage;
		if (Damage > 1f / transmit) {
			foreach (entitySkeleton bone in neighbors) {
				if (bone.Hierarchy < Hierarchy && transmit < 0) {
					bone.TakeDamage (0 - Damage * transmit, transmit);
				} else if (bone.Hierarchy > Hierarchy && transmit > 0) {
					bone.TakeDamage (Damage * transmit, transmit);
				}
			}
		}
		//death
		if (Damage > Data.Maxhealth) {
			Die (true);
		}
	}

	public SoftJointLimit limitJoint(float limit, float bounciness, float contactDistance)
	{
		SoftJointLimit Zim = new SoftJointLimit ();

		Zim.limit = limit;
		Zim.bounciness = bounciness;
		Zim.contactDistance = contactDistance;

		return Zim;
	}

	public bool cannotGore()
	{
		return false;
	}
}

public class componentSkeleton:MonoBehaviour
{
	public entitySkeleton parent;
	public entityMesh getMesh()
	{
		return parent.MeshData;
	}
	public entityActor getOwner()
	{
		return getMesh ().Owner;
	}
}