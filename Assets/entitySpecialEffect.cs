﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class entitySpecialEffect
{
	public Timer ticker;
	public GameObject gameObject;
	public List<Sprite> sprites;
	public bool isfrozen=false;
	float duration=0;
	GameMission game;
	public Vector3 data;
	public Vector3 finalScale;

	public entitySpecialEffect(GameMission g)
	{
		game = g;
	}

	public float GetDuration()
	{
		return duration * game.GetSpeed ();
	}


	public void Destroy ()
	{
			GameObject.Destroy (gameObject);

		if (ticker != null && !ticker.isDead) {
			ticker.Kill (true);
		}
	}
}

public static class SpecialEffects
{
	public static Texture2D LoadTexture(GameResources game,string texturename)
	{
		Texture2D tex =  game.LoadTexture ("Effects/" , texturename);
		if (tex == null) {
			if (texturename != "") {
				Debug.LogError ("No texture with the name " + texturename + " has been found!");
			}
			return null;
		}
		return tex;
	}

	public static entitySpecialEffect CreateGeneric(GameMission game,CreatePlane.Orientation orientation, bool twoSided,float expectedSize,Texture2D BaseTexture,Vector3 Pos,  Color32 color)
	{
		entitySpecialEffect eData = new entitySpecialEffect (game);
		eData.gameObject = CreatePlane.CreateSprite (1, 1, orientation, CreatePlane.AnchorPoint.Center, twoSided, Sprite.Create (BaseTexture, new Rect (0, 0, BaseTexture.height, BaseTexture.height), Vector2.one / 2f), BaseTexture.name);

		eData.sprites = new List<Sprite> ();
		for (int Zim = 0; Zim < Mathf.FloorToInt (BaseTexture.width / BaseTexture.height); Zim++) {
			eData.sprites.Add (Sprite.Create (BaseTexture,
				new Rect (BaseTexture.height * Zim, 0, BaseTexture.height, BaseTexture.height),
				new Vector2(0.5f,0f)));
		}


		eData.finalScale = new Vector3 (eData.sprites[0].rect.width/expectedSize,1 , eData.sprites[0].rect.height/expectedSize);
		eData.gameObject.transform.localPosition = Pos;
		eData.gameObject.transform.localScale=Vector3.zero;

		CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),color);

		return eData;
	}

	public static entitySpecialEffect ParticleEffect(GameMission game,Texture2D BaseTexture,float expectedSize,float scale, float dur,float fade,float maxHeight, Color32 color, float fireInterval, GameObject parent)
	{
		entitySpecialEffect final = new entitySpecialEffect (game);
		final.data = Vector3.one;
		final.gameObject = parent;
		final.ticker= Timer.Create ("particleTimer",0f, delegate(Timer original) {
			if (final.gameObject==null)
			{
				return 0;
			}
			if (final.gameObject.activeSelf && final.gameObject.activeInHierarchy)
			{
				ExplosionSmoke( game, BaseTexture, expectedSize,final.gameObject.transform.position, scale,  dur, fade, maxHeight,  color);
			}
			return fireInterval*final.data.x;
		}, delegate(Timer original) {
			final.Destroy();
			return 0;
		});
		return final;
	}

	public static entitySpecialEffect ExplosionSmoke(GameMission game,Texture2D BaseTexture,float expectedSize,Vector3 Pos,float scale, float dur,float fade,float maxHeight, Color32 color)
	{
		entitySpecialEffect eData = CreateGeneric(game,CreatePlane.Orientation.Vertical,false,expectedSize,BaseTexture,Pos,color);
		CreatePlane.ApplySprite (eData.gameObject.GetComponent<MeshRenderer> (), eData.sprites [Mathf.FloorToInt (UnityEngine.Random.value * eData.sprites.Count)]);

		float starttime = Time.time;
		Vector3 posVector = eData.gameObject.transform.localPosition;
		eData.gameObject.transform.localScale = Vector3.zero;

		eData.ticker = Timer.Create ("boomFX", 0, delegate {
			CreatePlane.FaceCamera(eData.gameObject.transform,Camera.main);

			float delta = (Time.time-starttime)/dur;
			eData.gameObject.transform.localScale = eData.finalScale*scale*delta;
			posVector.y+=maxHeight*Time.fixedDeltaTime;
			eData.gameObject.transform.localPosition=posVector;

			if (Time.time>starttime+dur)
			{
				return 0;
			}

			return Time.fixedDeltaTime;
		},delegate {
			float deathtime = Time.time;
			eData.ticker = Timer.Create ("boomfade", 0, delegate {

				float delta = (Time.time-deathtime)/fade;

				Color newcol = new Color(color.r/255f,color.g/255f,color.b/255f,color.a*(1-delta)/255f);
				CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),newcol);

				posVector.y+=maxHeight*Time.fixedDeltaTime;
				eData.gameObject.transform.localPosition=posVector;

				if (Time.time>deathtime+fade)
				{
					return 0;
				}

				return Time.fixedDeltaTime;
			},delegate {
				eData.Destroy();	return 0f;
			});
			return 0;
		});

		return eData;
	}

	public static List<entitySpecialEffect> ExplosionFire(GameMission game,Texture2D BaseTexture,float expectedSize,Vector3 Pos,float scale, float dur,float fade,float traveldisance, Color32 color,int instances)
	{

		List<entitySpecialEffect> value = new List<entitySpecialEffect> ();
		for (int iDeb = 0; iDeb < instances; iDeb++) {

			entitySpecialEffect eData = CreateGeneric(game,CreatePlane.Orientation.Vertical,false,expectedSize,BaseTexture,Pos,color);
			CreatePlane.ApplySprite (eData.gameObject.GetComponent<MeshRenderer> (), eData.sprites [Mathf.FloorToInt (UnityEngine.Random.value * eData.sprites.Count)]);

			eData.data = (new Vector3 (UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) - Vector3.one / 2f).normalized * traveldisance;

			value.Add (eData);
		}
		float starttime = Time.time;

		Timer ticker = Timer.Create ("boomFX", 0, delegate {

			float delta = (Time.time-starttime)/dur;
			foreach (entitySpecialEffect eData in value){
				CreatePlane.FaceCamera(eData.gameObject.transform,Camera.main);

				eData.gameObject.transform.localScale = eData.finalScale*scale*delta;
				eData.gameObject.transform.localPosition=Pos+eData.data*scale*delta;
			}

				if (Time.time>starttime+dur)
				{
					return 0;
				}
			

			return Time.fixedDeltaTime;
		},delegate {
			float deathtime = Time.time;
			Timer tocker = Timer.Create ("boomfade", 0, delegate {

				float delta = (Time.time-deathtime)/fade;

				Color newcol = new Color(color.r/255f,color.g/255f,color.b/255f,color.a*(1-delta)/255f);
				foreach (entitySpecialEffect eData in value){
					CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),newcol);
				}

				if (Time.time>deathtime+fade)
				{
					return 0;
				}

				return Time.fixedDeltaTime;
			},delegate {
				foreach (entitySpecialEffect eData in value){
					eData.Destroy();
				}
				value.Clear();
				return 0f;
			});
			return 0;
		});

		return value;
	}

	public static entitySpecialEffect ExplosionFire(GameMission game,Texture2D BaseTexture,float expectedSize,Vector3 Pos,float scale, float dur,float fade,float traveldisance, Color32 color)
	{
		entitySpecialEffect eData = CreateGeneric(game,CreatePlane.Orientation.Vertical,false,expectedSize,BaseTexture,Pos,color);
		CreatePlane.ApplySprite (eData.gameObject.GetComponent<MeshRenderer> (), eData.sprites [Mathf.FloorToInt (UnityEngine.Random.value * eData.sprites.Count)]);

		float starttime = Time.time;
		Vector3 centerVector = eData.gameObject.transform.localPosition;
		Vector3 posVector = (new Vector3 (UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) - Vector3.one / 2f).normalized * traveldisance;


		eData.ticker = Timer.Create ("boomFX", 0, delegate {
			CreatePlane.FaceCamera(eData.gameObject.transform,Camera.main);

			float delta = (Time.time-starttime)/dur;
			eData.gameObject.transform.localScale = eData.finalScale*scale*delta;
			eData.gameObject.transform.localPosition=centerVector+posVector*scale*delta;

			if (Time.time>starttime+dur)
			{
				return 0;
			}

			return Time.fixedDeltaTime;
		},delegate {
			float deathtime = Time.time;
			eData.ticker = Timer.Create ("boomfade", 0, delegate {

				float delta = (Time.time-deathtime)/fade;

				Color newcol = new Color(color.r/255f,color.g/255f,color.b/255f,color.a*(1-delta)/255f);
				CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),newcol);

				if (Time.time>deathtime+fade)
				{
					return 0;
				}

				return Time.fixedDeltaTime;
			},delegate {
				eData.Destroy();	return 0f;
			});
			return 0;
		});

		return eData;
	}

	public static List<entitySpecialEffect> ExplosionGeneric(GameMission game,Texture2D BaseTexture,float expectedSize,Vector3 Pos,float diameter, float dur,float fade, float nExplosions, Color32 color)
	{
		float smallScale = diameter*1f/2f;
		for (int Gir = 1; Gir < nExplosions; Gir++) {
			smallScale*=0.9f;
		}

		float useRadius = (diameter - smallScale*2);

		List<entitySpecialEffect> explosions = new List<entitySpecialEffect> ();
		for (int nPoof = 0; nPoof < nExplosions; nPoof++) {

			Vector3 center = new Vector3 (
				Pos.x-useRadius+UnityEngine.Random.value*useRadius*2,
				Pos.y-useRadius+UnityEngine.Random.value*useRadius*2,
				Pos.z-useRadius+UnityEngine.Random.value*useRadius*2
			                 );
			entitySpecialEffect Gir = CreateGeneric (game, CreatePlane.Orientation.Horizontal,false, expectedSize, BaseTexture, center, color); 
			Gir.finalScale /= 2f;
			explosions.Add(Gir);
		}
		if (explosions.Count==0)
		{
			return explosions;
		}

		float frameUpdate = dur*game.GetSpeed()/(explosions[0].sprites.Count);
		float fTime=Time.time;
		float starttime = Time.time;


		explosions[0].ticker = Timer.Create ("boomFX", 0, delegate {
			foreach (entitySpecialEffect eData in explosions) {
				CreatePlane.FaceCamera(eData.gameObject.transform,Camera.main);

				float delta = (Time.time-starttime)/dur;
				eData.gameObject.transform.localScale = eData.finalScale*smallScale*delta;

				if (Time.time-fTime>frameUpdate)
				{
					if (eData.sprites.Count>0)
					{
						fTime=Time.time;

						CreatePlane.ApplySprite(eData.gameObject.GetComponent<MeshRenderer>(),eData.sprites[0]);
						eData.sprites.RemoveAt(0);
					}
				}
			}
			if (Time.time>starttime+dur)
			{
				return 0;
			}

			return Time.fixedDeltaTime;
		},delegate {
			float deathtime = Time.time;
			explosions[0].ticker = Timer.Create ("boomfade", 0, delegate {

				float delta = (Time.time-deathtime)/fade;

				Color newcol = new Color(color.r/255f,color.g/255f,color.b/255f,color.a*(1-delta)/255f);

				foreach (entitySpecialEffect eData in explosions) {
				CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),newcol);
				}

				if (Time.time>deathtime+fade)
				{
					return 0;
				}

				return Time.fixedDeltaTime;
				},delegate {
					foreach (entitySpecialEffect eData in explosions) {
						eData.Destroy();
					}
					explosions.Clear(); 
					return 0f;
			});
			return 0;
		});

		return explosions;
	}

	public static List<entitySpecialEffect> makeSplash(GameMission game,Vector3 center,float scale, float time)
	{
		List<entitySpecialEffect> explosions = new List<entitySpecialEffect> ();

		Vector3 spashCenter = new Vector3 (
			center.x, game.GetWaterLevel () + 0.01f, center.z
		);

		explosions.AddRange(SpecialEffects.CastDebree (game, 3 + 5 * game.settingsLevel, 1 / 3f, 1 / 2f, 1, 3 / 2f, spashCenter, 0.02f, time / 2f, Color.white));

		Texture2D ripple = LoadTexture (game.gDat, "ripple");
		Timer.Create ("splashDelay", time / 3f, delegate {
			
			explosions.Add(SpecialEffects.ExplosionShockwave (game,ripple, 25f,spashCenter, 2.0f, time, 1, Color.white));
			explosions.Add(SpecialEffects.ExplosionShockwave (game,ripple, 25f,spashCenter, 1.5f, time, 1, Color.white));
			explosions.AddRange(SpecialEffects.CastDebree (game, 5+5*game.settingsLevel, 2/3f,3/4f,1/3f,1/2f,  spashCenter, 0.02f, time/2f, Color.white));

			return 0;
		});

						return explosions;
	}

	public static List<entitySpecialEffect> CastDebree (GameMission game, int instances,float bouncewm, float bouncewM,float bouncehm,float bouncehM,Vector3 center,float scale, float dur, Color32 color)
	{
		return CastDebree ( game,  instances, bouncewm,  bouncewM, bouncehm, bouncehM, center, scale,  dur, new List<Color32> {color});
	}

		public static List<entitySpecialEffect> CastDebree (GameMission game, int instances,float bouncewm, float bouncewM,float bouncehm,float bouncehM,Vector3 center,float scale, float dur, List<Color32> color)
		{

		List<entitySpecialEffect> value = new List<entitySpecialEffect> ();
		for (int iDeb = 0; iDeb < instances; iDeb++) {
			float bouncew = UnityEngine.Random.Range(bouncewm,bouncewM);
			float bounceh = UnityEngine.Random.Range(bouncehm,bouncehM);
			value.Add(Shrapnel(game,bouncew,bounceh,scale,color[(int)(UnityEngine.Random.value*color.Count)]));
		}
		float starttime = Time.time;

		Timer ticker = Timer.Create ("boomFX", 0, delegate {

			foreach (entitySpecialEffect eData in value){
			CreatePlane.FaceCamera(eData.gameObject.transform,Camera.main);


			float delta = (Time.time-starttime)/dur;
			eData.gameObject.transform.localPosition = new Vector3(
					center.x+eData.data.x*delta,
					center.y+eData.data.y*Mathf.Sin(delta*Mathf.PI),
					center.z+eData.data.z*delta
			);

			if (Time.time>starttime+dur)
			{
				return 0;
			}
			}

			return Time.fixedDeltaTime;
		},delegate {
			foreach (entitySpecialEffect eData in value){
				eData.Destroy();	
			}return 0f;
		});

		return value;
	}

	public static entitySpecialEffect Spark (GameMission game,float bouncew,float bounceh,float scale, Color32 color)
	{
		entitySpecialEffect eData = new entitySpecialEffect (game);

		Texture2D BaseTexture = LoadTexture(game.gDat,"spark");
		eData.gameObject = CreatePlane.CreateSprite (1, 1, CreatePlane.Orientation.Vertical, CreatePlane.AnchorPoint.Center, false, Sprite.Create (BaseTexture, new Rect (0, 0, BaseTexture.height, BaseTexture.height), Vector2.one / 2f), BaseTexture.name);

		CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),color);

		eData.gameObject.transform.localScale = Vector3.one*scale;

		bouncew *= (1 + UnityEngine.Random.value) / 2f;
		float angle = UnityEngine.Random.value  * Mathf.PI * 2;
		Vector2 direction = new Vector2 (Mathf.Cos (angle), Mathf.Sin (angle));

		eData.data = new Vector3(
			direction.x*bouncew,
			bounceh,
			direction.y*bouncew
		);


		return eData;
	}

	public static entitySpecialEffect Shrapnel (GameMission game,float bouncew,float bounceh,float scale, Color32 color)
	{
		entitySpecialEffect eData = new entitySpecialEffect (game);

		Texture2D BaseTexture = Texture2D.whiteTexture;
		eData.gameObject = CreatePlane.CreateSprite (1, 1, CreatePlane.Orientation.Vertical, CreatePlane.AnchorPoint.Center, false, Sprite.Create (BaseTexture, new Rect (0, 0, BaseTexture.height, BaseTexture.height), Vector2.one / 2f), BaseTexture.name);

		CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),color);

		eData.gameObject.transform.localScale = Vector3.one*scale*3f;

		bouncew *= (1 + UnityEngine.Random.value) / 2f;
		float angle = UnityEngine.Random.value  * Mathf.PI * 2;
		Vector2 direction = new Vector2 (Mathf.Cos (angle), Mathf.Sin (angle));

		eData.data = new Vector3(
			direction.x*bouncew,
			bounceh,
			direction.y*bouncew
		);


		return eData;
	}

	public static entitySpecialEffect ExplosionShockwave(GameMission game,Texture2D BaseTexture,float expectedSize,Vector3 Pos,float scale, float dur, float fade, Color32 color)
	{
		return ExplosionShockwave (game, BaseTexture, false, expectedSize, Pos, Vector3.zero,scale, dur, fade, color);
	}

	public static entitySpecialEffect ExplosionShockwave(GameMission game,Texture2D BaseTexture,bool twoSided,float expectedSize,Vector3 Pos,Vector3 BaseRot,float scale, float dur, float fade, Color32 color)
	{
		entitySpecialEffect eData = CreateGeneric (game, CreatePlane.Orientation.Horizontal,twoSided,expectedSize, BaseTexture, Pos, color);
		eData.gameObject.transform.rotation = Quaternion.Euler (BaseRot);

		float frameUpdate = dur*game.GetSpeed()/(eData.sprites.Count);
		float fTime=Time.time;
		float starttime = Time.time;
		eData.ticker = Timer.Create ("boomFX", 0, delegate {


			float delta = (Time.time-starttime)/dur;
			eData.gameObject.transform.localScale = eData.finalScale*scale*delta;

			if (Time.time-fTime>frameUpdate)
			{
				if (eData.sprites.Count>0)
				{
				fTime=Time.time;
				
				CreatePlane.ApplySprite(eData.gameObject.GetComponent<MeshRenderer>(),eData.sprites[0]);
				eData.sprites.RemoveAt(0);
				}
			}
			if (Time.time>starttime+dur)
			{
				return 0;
			}
			
			return Time.fixedDeltaTime;
		},delegate {
			float deathtime = Time.time;
			eData.ticker = Timer.Create ("boomfade", 0, delegate {

				float delta = (Time.time-deathtime)/fade;

				Color newcol = new Color(color.r/255f,color.g/255f,color.b/255f,color.a*(1-delta)/255f);

				CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),newcol);

					if (Time.time>deathtime+fade)
					{
						return 0;
					}

				return Time.fixedDeltaTime;
			},delegate {
				eData.Destroy();	return 0f;
			});
			return 0;
		});

		return eData;
	}
	public static entitySpecialEffect MakeBeam (GameMission game, Vector3 Start, Vector3 End, float dur,float fade,Texture2D BaseTexture)
	{
		entitySpecialEffect eData = new entitySpecialEffect (game);
	
		eData.sprites = new List<Sprite> ();
		for (int Zim = 0; Zim < Mathf.FloorToInt (BaseTexture.width / BaseTexture.height); Zim++) {
			eData.sprites.Add (Sprite.Create (BaseTexture,
				new Rect (BaseTexture.height * Zim, 0, BaseTexture.height, BaseTexture.height),
				new Vector2(0.5f,0f)));
		}

		Vector3 origScale = new Vector3 (eData.sprites[0].rect.width/25f,1 , eData.sprites[0].rect.height/25f);

		eData.gameObject = CreatePlane.CreateSprite (BaseTexture.width/80f, (End-Start).magnitude, CreatePlane.Orientation.Horizontal, CreatePlane.AnchorPoint.Center, true, eData.sprites[0],BaseTexture.name);

		eData.gameObject.transform.position = (Start+End)/2;
		eData.gameObject.transform.localScale = new Vector3(BaseTexture.width/200f,1, (End-Start).magnitude);
		eData.gameObject.transform.rotation = Quaternion.LookRotation(
			End-Start
		);

		float frameUpdate = dur*game.GetSpeed()/(eData.sprites.Count);
		float fTime=Time.time;
		float starttime = Time.time;
		eData.ticker = Timer.Create ("boomFX", 0, delegate {

			if (Time.time-fTime>frameUpdate)
			{
				if (eData.sprites.Count>0)
				{
					fTime=Time.time;

					CreatePlane.ApplySprite(eData.gameObject.GetComponent<MeshRenderer>(),eData.sprites[0]);
					eData.sprites.RemoveAt(0);
				}
			}
			if (Time.time>starttime+dur)
			{
				return 0;
			}

			return Time.fixedDeltaTime;
		},delegate {
			float deathtime = Time.time;
			eData.ticker = Timer.Create ("boomfade", 0, delegate {


				Color newcol = new Color(1,1,1,(1-(Time.time-deathtime)/fade)/255f);

				CreatePlane.ApplyColor (eData.gameObject.GetComponent<MeshRenderer> (),newcol);

				if (Time.time>deathtime+fade)
				{
					return 0;
				}

				return Time.fixedDeltaTime;
			},delegate {
				eData.Destroy();	return 0f;
			});
			return 0;
		});


		return eData;
	}
}
