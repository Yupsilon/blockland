﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class propertyImageMap:propertyMap
	{

	public Texture2D WorldTexture;
	public Texture2D ElevationMask=null;

	public propertyImageMap (GameResources game,string texturename)
	{
		WorldTexture = game.LoadTexture ("Maps/", texturename);
		ElevationMask = game.LoadTexture ("Maps/", texturename+"_elevation");
		Debug.Log (WorldTexture+" ad\t");
		Height = 10;
		ElevationData = new List<float> ();

		Generate (1);
	}

	public override int GetWidth()
	{
		return WorldTexture.width;
	}

	public override void Generate(int Size)
	{
		ElevationData = new List<float> ();
		if (ElevationMask == null) {

			for (int iY = 0; iY < WorldTexture.height; iY++) {
				for (int iX = 0; iX < WorldTexture.width; iX++) {
					int elevation = -1;
					if (WorldTexture.GetPixel (iX, iY).a == 0) {
						elevation = 0;
					}
					ElevationData.Add (elevation);
				}
			}
			bool canStop = false;
			int raise = 1;
			while (!canStop && raise<100) {
				canStop = true;
				for (int iX = 0; iX < WorldTexture.width; iX++) {
					for (int iY = 0; iY < WorldTexture.height; iY++) {
						
						if (GetTileAt (iX, iY) == -1) {
							canStop = false;
							if (GetTileAt (iX + 1, iY) == raise - 1 || GetTileAt (iX - 1, iY) == raise - 1 ||
							   GetTileAt (iX, iY + 1) == raise - 1 || GetTileAt (iX, iY - 1) == raise - 1) {
								SetTileAt (iX, iY, raise);
							}
						}
					}
				}
				raise++;
			}
			for (int iZ = 0; iZ < ElevationData.Count; iZ++) {
				ElevationData[iZ] -= 1;
			}

		} else {
			for (int iX = 0; iX < WorldTexture.width; iX++) {
				for (int iY = 0; iY < WorldTexture.width; iY++) {
					int elevation = Mathf.FloorToInt( (ElevationMask.GetPixel (iX,iY).r + ElevationMask.GetPixel (iX,iY).b + ElevationMask.GetPixel (iX,iY).g) * Height / 3f);
						ElevationData.Add (elevation);
				}
			}
		}
	}

	public override Color GetColorAt (int iX, int iY, int iZ)
	{
		return WorldTexture.GetPixel (iX, iZ);
	}
}

