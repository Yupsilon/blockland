﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityGore: entityBase
{
	entityActor owner;
	public entityMesh Skeleton;
	public entityGore( entityMesh body)
	{
		owner = body.Owner;
		Skeleton = body;
	}
		
	public entityGore(entityActor owner, entitySkeleton limb)
	{
		game = owner.game;
		Skeleton = new entityMesh (owner.game);
		Skeleton.teamColor = limb.MeshData.teamColor;

		Skeleton.Bones = new List<entitySkeleton> ();
		foreach (entitySkeleton single in limb.GetChildren(true)) {
			if (!single.cannotGore () || single.GetCategory().Contains("weapon")) {
				entitySkeleton newParent = null;
				if (Skeleton.Bones.Count > 0) {
					newParent = Skeleton.Bones [Skeleton.Bones.Count - 1];
				}
				Skeleton.Bones.Add (single.Copy (newParent, Skeleton));
			}
		}
		Skeleton.frozen = false;
		Skeleton.SetRagdoll (1, 0);
	}

	public override void Dispose(bool destroy)
	{
		if (Skeleton != null) {
			Skeleton.Kill(destroy);
		}
		base.Dispose (destroy);
	}

	public override void Update ()
	{
		Skeleton.Think ();
	}
}
