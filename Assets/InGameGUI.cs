﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InGameGUI
{
	public GameResources game;
	public float lastClickTime=0f;
	public Canvas GUICanvas;
	public List<GameObject> GUIData;
	public NewMenuWindow Window;
	public property_GUI MenuSkin = new property_GUI ();

	public GUIStyle InfoTitle;
	public GUIStyle InfoButton;
	public GUIStyle ScrollBar;

	public GUIStyle LoadSaveBG;
	public GUIStyle LoadSaveLabel;
	public GUIStyle LoadSaveInput; 

	public InGameGUI(GameResources game)
	{
		game.gui = this;
		this.game = game;

		GUICanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
		GUICanvas.transform.position = new Vector3 (0, 0, 0);
		GUICanvas.gameObject.GetComponent<UnityEngine.UI.CanvasScaler> ().referenceResolution = NewMenus.ScreenResolution;

		GUIData = new List<GameObject> ();


		InfoTitle = new GUIStyle ();
		InfoTitle.normal.background = game.LoadTexture(GameDirectory.UIFile,"MainMenuUI") as Texture2D;
		InfoTitle.normal.background.filterMode = FilterMode.Point;
		InfoTitle.font = Resources.Load ("GUI/Fonts/MainFont") as Font;
		InfoTitle.fontSize = 10;
		InfoTitle.alignment = TextAnchor.MiddleCenter;		
		InfoTitle.normal.textColor=Color.black;

		InfoButton = new GUIStyle ();
		InfoButton.normal.background = game.LoadTexture(GameDirectory.UIFile,"MainMenuUI") as Texture2D;
		InfoButton.normal.background.filterMode = FilterMode.Point;
		InfoButton.font = Resources.Load ("GUI/Fonts/MainFont") as Font;
		InfoButton.fontSize = 10;
		InfoButton.alignment = TextAnchor.MiddleLeft;		
		InfoButton.normal.textColor=Color.black;

		LoadSaveBG=InfoButton;
		LoadSaveLabel=InfoButton;
		LoadSaveInput=InfoButton;
		ScrollBar=InfoButton;
	}

	public void OpenWindow(NewMenuWindow Object)
	{
		Clear ();
		Window=Object;
	}

	public void Clear() {			

		if (Window != null) {			
			Window.Close ();
			Window = null;
		}
	}

	public bool isMouseOverUI()
	{
		if (Window != null) {
			return true;
		}
		return false;
	}
}



public class FirstPersonHUD : NewMenuWindow
{
	entityActor Actor;
	GameObject crosshair;
	List<GameObject> chargeMeter;
	Timer idleTimer;

	public FirstPersonHUD(InGameGUI gDat, entityActor owner, Rect rPos)
	{
		Pos = rPos;
		game = gDat;
		Actor = owner;
		Draw ();
	}

	public override void Draw()
	{
		myStuff = new List<GameObject> ();

		Texture2D crosshairTexture = game.game.LoadTexture ("GUI/", "HUDCrosshair");
		Sprite crossHUDsprite = Sprite.Create (crosshairTexture, new Rect (0, 0, crosshairTexture.width, crosshairTexture.height), Vector2.zero);

		crosshair = NewMenus.GUISprite (
			game, crossHUDsprite,
			new GUIStyle (), new Rect (Pos.center.x - 25f, Pos.center.y - 25f, 50f, 50f));
		myStuff.Add (crosshair.transform.parent.gameObject);

		Texture2D centerHUDtexture = game.game.LoadTexture ("GUI/", "HUDCenter");
			Sprite centerHUDsprite = Sprite.Create (centerHUDtexture, new Rect (0, 0, centerHUDtexture.width, centerHUDtexture.height), Vector2.zero);
			float centerSize = 200f;

			myStuff.Add (NewMenus.GUISprite (
				game, centerHUDsprite,
			new GUIStyle(), new Rect (Pos.center.x - centerSize / 2f, Pos.center.y - centerSize / 2f, centerSize, centerSize)).transform.parent.gameObject);

		Texture2D chargerHUDtexture = game.game.LoadTexture ("GUI/", "HUDCharge");
		Sprite chargerHUDsprite = Sprite.Create (chargerHUDtexture, new Rect (0, 0, chargerHUDtexture.width, chargerHUDtexture.height), Vector2.zero);

		float totAngle = 80;
		float nUnits = 20;

		chargeMeter = new List<GameObject> ();
		for (int Zim = 0; Zim < nUnits; Zim++) {

			float angle = 90-totAngle/2f + Zim / nUnits * totAngle;

			GameObject Gir = NewMenus.GUISprite (
				game, chargerHUDsprite,
				new GUIStyle (), new Rect (Pos.center.x + centerSize / 2f*Mathf.Sin(angle/180f*Mathf.PI), Pos.center.y + centerSize / 2f*Mathf.Cos(angle/180f*Mathf.PI), 20, 6));

			Gir.gameObject.transform.rotation = Quaternion.Euler (0, 0, 90+angle);
			chargeMeter.Add (Gir);
			myStuff.Add (Gir.transform.parent.gameObject);
		}

		disable ();
		enable ();
	}

	public override void Update()
	{ 
		if (Actor.getWeapon () == null) {
			return;
		}
			Vector3 center = NewMenus.RealPointToCamera (Camera.main.WorldToScreenPoint (Actor.targeter.transform.position));
			NewMenus.RectToTransform (crosshair.GetComponent<RectTransform> (), new Rect (
				center.x - 12.5f, center.y - 12.5f, 25f, 25f
			), true);

		if (Actor.weaponCharge>=0){
			int enabled = Mathf.CeilToInt (Actor.weaponCharge * (chargeMeter.Count-1));

			if (enabled >= chargeMeter.Count) 
			{
				enabled = chargeMeter.Count*2-enabled;
			}

			if (!chargeMeter [enabled].activeInHierarchy) {
				
				chargeMeter [enabled].SetActive (true);
				chargeMeter [enabled].transform.localScale *= 1.5f;
				Timer.Create ("Cute Anim " + enabled, 0, delegate(Timer original) {

					if (amClosed || 
						chargeMeter [enabled].transform.localScale.magnitude < Vector3.one.magnitude) {
						return 0;
					}
					chargeMeter [enabled].transform.localScale -= Vector3.one / 20f;


					return Time.deltaTime;
				}, delegate(Timer original) {
					if (!amClosed){
					chargeMeter [enabled].transform.localScale = Vector3.one;
					}return 0;
				});
			}
			/*
			if (Actor.weaponCharge>1 && !chargeMeter [enabled+1].activeInHierarchy) {
				chargeMeter [enabled].SetActive (false);
			}*/
				}
	}

	public void enable()
	{	
		if (Actor.getWeapon ()!=null && Actor.getWeapon ().type == propertyWeapon.weapontype_singleshot_charged) {
			myStuff [1].SetActive (true);
		}
	}

	public void disable()
	{	
		NewMenus.RectToTransform (crosshair.GetComponent<RectTransform> (), new Rect (
			Pos.center.x-25f,Pos.center.y-25f,50f,50f
		), true);
		myStuff[1].SetActive (false);
		foreach (GameObject Zim in chargeMeter) {
			Zim.SetActive (false);
		}
		}

}