﻿/* --FEATURES
 * 
 * projectiles/shooting
 * 
 * bazooka fire poof efx?
 * fix bazooka anim handrot?
 * 
 * skins and proper weapon textures
 * RUNDED EDGES!
 * 
 * INDIEDB/reddit
 * 
 * hitscan projectiles
 * hitscan explosions
 * proper projectile FX
 * 
 * limb damage
 * wind
 * 
 * weapon configs (grenade timers etc)
 * scare/follow anims for weapons
 * 
 * top and bot anims
 * 
 * ragdoll times
 * ragdoll knockback
 * standup front/back
 * animations
 * 
 * players/teams/wizards
 * gamephases
 * turns
 * 3d map gen
 * team view menu
 * wep sel menus
 * little guys seen from far aways
 * animations
 * ragoll collision(reactivation)
 * falling damage
 * burning blocks
 * bubbles
 * PUFF PARTICLES JUMP
 * elemental explosions
 * 
 * camera look to actor/look to point (on death), follow projectile
 * 
 * carmageddon like endgame car preview
 * 
 * intro music
 * play music
 * sfx
 * 
 * 1st Row of weapons
 * 
 * FIRST RELEASE (WOO!)
 * 
 * ---FIXES---
 * fix left right meshes
 * soft reset mesh
 * gameresources for meshes
 * face render queue
 * fx render queue
 * character ycollision fix
 * proj spin
 * look fix
 * death fixes
 * collision fix
 * fix ragdolls
 * water texture
 * skybox
 * rqueue
 * 
 * dispose of  stuff
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 0 BOOK OF WAR
 * grenade
 * crossbow - w3d shotgun
 * 
 * 3 BOOK OF EARTH
 * boulder - bouncing bomb (arcanist)
 * mud cannon - old icecream cannon
 * 
 * 2 BOOK OF FIRE
 * fire missile - high damage, low ground damage, turns ground black and on fire
 * spitfire - flamethrower
 * 
 * 1 SCULPTOR TOOLS
 * chisel
 * block
 * swap/skip turn
 * 
 * 4 BOOK OF STORMS
 * -chain lightning * bounces of walls, hits multiple
 * -thunderball
 * 
 * 
 * EXTRAS
 * 
 * sponsor expansion
 * 
 * 5 SPONSORS
 * --isaniade white (jump)
 * --insaniade blue (speed)
 * --insaniade red (damae)
 * 
 * extra expansion
 * tools and crates
 * 6 BOOK OF TIME
 * -reset turn
 * -skip turn (go forward)
 * -teleport
 * 
 * -crates
 *  weapon - texture depends on weapon type
 *  powerup
 * 		lowg
 * 		2xdamage
 * 		bonus turn time
 * 		health
 * 		
 * 		team weapon (after megaweapon expansion)
 *  
 *  slot machine?
 * 
 * airstrike expansion
 * 0-dynamite
 * 1-signal jammer?!
 * 2-rain of fire
 * 3-boulder toss
 * 4-lightning strike
 * 5-
 * 6-
 * 
 * 7 book of water
 * -water stream
 * -
 * - whale
 * 
 * 
 * 8 venomancy
 * --gas can - beehive
 * --gas pump  - bees
 * --self destruct 0
 * 
 * --ninja rope 4
 * --insaniade green (poison)5
 * --nuke (large explosion+poison on all)6
 * --radar 7
 * 
 * cluster expansion (halloween themed?)
 * 1 cluster bomb
 * 2 cluster mortar
 * 3 fencing sword
 * 4 
 * 5 
 * 6 
 * 7 vampire fangs
 * 
 * animals
 * 8
 * ----super seal
 * ----mole (digs)
 * ----bombken (walks slowly across surface)
 * ----
 * ----
 * 
 * SWEET WAR
 * or
 * WIZARDS
 * 
 * megaweapons expansion
 * 1 holy hand nade
 * 2 
 * 3
 * 4 
 * 5
 * 6 orbital laser
 * 7
 * 8
 * 
 * 
 * -paintgun mode
 * 
 * game modes
 * 	kill all guys
 * 	-extermination (classic worms)
 *  -deathmatch (respawns)
 *  -forts?
 * tag
 *  -explosions repaint instead of destroying
 *  -cannot build, replace claygun with paintgun, shovel with brush, block with
 * */