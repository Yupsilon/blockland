﻿using System;
using UnityEngine;

public class Timer : MonoBehaviour
{
	public float expireTime { get; private set;}
	public float lifeTime { get; private set;}
	public float startTime { get; private set;}
	public bool isDead=false;
	public bool mortal=true;
	public bool forced=false;
	public bool running=false;
	public bool actdeath=false;
	public delegate float timerAction(Timer original);
	timerAction expire;		
	timerAction kill;		
	public static Timer Create( bool state,string name,float ftime, timerAction loopCheck,timerAction onKill )
	{
		GameObject Panty = new GameObject ();
		Panty.name = name;
		Panty.AddComponent<Timer> ();
		Panty.GetComponent<Timer> ().lifeTimer=ftime;
		Panty.GetComponent<Timer> ().expireTimer=Time.time + ftime;
		Panty.GetComponent<Timer> ().expire = loopCheck;
		Panty.GetComponent<Timer> ().kill = onKill;
		Panty.GetComponent<Timer> ().running = state;

		return Panty.GetComponent<Timer> ();
	}
	public static Timer Create( string name,float ftime, timerAction loopCheck,timerAction onKill )
	{
		GameObject Panty = new GameObject ();
		Panty.name = name;
		Panty.AddComponent<Timer> ();
		Panty.GetComponent<Timer> ().lifeTimer=ftime;
		Panty.GetComponent<Timer> ().expireTimer=Time.time + ftime;
		Panty.GetComponent<Timer> ().expire = loopCheck;
		Panty.GetComponent<Timer> ().kill = onKill;
		Panty.GetComponent<Timer> ().running = true;

		return Panty.GetComponent<Timer> ();
	}
	public static Timer Create( string name,float ftime, timerAction onKill )
	{
		GameObject Panty = new GameObject ();
		Panty.name = name;
		Panty.AddComponent<Timer> ();
		Panty.GetComponent<Timer> ().lifeTimer=ftime;
		Panty.GetComponent<Timer> ().expireTimer=Time.time + ftime;
		Panty.GetComponent<Timer> ().expire = delegate {
			return 0;
		};
		Panty.GetComponent<Timer> ().kill = onKill;
		Panty.GetComponent<Timer> ().running = true;

		return Panty.GetComponent<Timer> ();
	}
	public void delay(float dtime)
	{
		expireTimer= dtime;
	}
	public bool isExpired()
	{
		return GetRemainingDuration()<=0;
	}
	public void Kill(bool triggerDeath)
	{
		if (!isDead) {
			isDead = true;
			if (kill != null && triggerDeath) {
				kill (this);
			}
			expireTimer = 0;
			if (gameObject != null) {
				GameObject.Destroy (gameObject);
			}
		}
	}
	public float GetRemainingDuration()
	{
		return expireTimer - Time.time;
	}
	public void Update()
	{
		if (running && isExpired() && !isDead) {

			float fExpiration = expire (this);
			if (fExpiration == 0) {
				if (mortal) {
					Kill (true);
				} else {
					fExpiration = Time.fixedDeltaTime;
				}
			} else 
				if (expireTimer < 0) {
					expireTimer = Mathf.Infinity;
				}else {
					expireTimer = Time.time + fExpiration;
				}
		}
	}
}

