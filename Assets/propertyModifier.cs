﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class propertyModifier
{
	public string ModifierName;
	public float totDuration;
	public float Duration;
	public modstate[] ModifierStates;
	public dispeltype Alignment;

	public delegate void ModifierAction(propertyModifier Mod);
	public List<string> FuncNames = new List<string> ();
	public List<ModifierAction> functions = new List<ModifierAction>();

	public enum modstate  {
		stun = 0,
		gravity = 1,
		root = 2,
		haste = 3,
		poison = 4,
		nocollision = 5,
	};

	public enum dispeltype  {
		affliction = 0,
		buff = 1,
		extra = 2,
	};

	public static string function_expire = "d";

	public propertyModifier(string Name,dispeltype alignment,float t,modstate[] States)
	{
		ModifierName = Name;
		totDuration = t;
		ModifierStates = States;
		Alignment=alignment;	
	}

	public propertyModifier(string Name,dispeltype alignment,float t,modstate State)
	{
		ModifierName = Name;
		totDuration = t;
		ModifierStates = new modstate[]{State} ;
		Alignment=alignment;	
	}

	public propertyModifier (string Name,dispeltype alignment,float t,modstate[] States, ModifierAction aDeath ){

		ModifierName = Name;
		totDuration = t;
		ModifierStates = States;
		AddFunction(function_expire,aDeath);
		Alignment=alignment;	
	}

	public void AddFunction(string Name, ModifierAction execution)
	{
		if (execution == null) {
			return;}
		if (FuncNames.Contains (Name)) {
			functions[FuncNames.IndexOf (Name)]=execution;
		} else {
			FuncNames.Add (Name);
			functions.Add (execution);
		}
	}

	public void ExecuteFunction(string Name){

		if (FuncNames.IndexOf (Name) >= 0 && FuncNames.IndexOf (Name) < functions.Count) {
			if (functions[FuncNames.IndexOf (Name)] != null) {

				functions [FuncNames.IndexOf (Name)] (this);
			}
		}
	}

	public float GetModifierDuration(GameMission game)
	{
		if (totDuration >= 0) {
			return Duration - game.CurrentTurn;
		}
		return (game.CurrentTurn - Duration) % (totDuration);
	}
}