﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;


public class LimbAnimationData
{
	public string AnimationName="pose";
	public int AnimationFrames=0;
	public bool canBeMirrored=false;
	public bool isMirrored=false;

	public List<string> Pivots=new List<string>();
	public List<LimbAnimation> AnimationData=new List<LimbAnimation>(); 

	public static LimbAnimationData Make(string AnimationName, int AnimFrames, bool m,List<LimbAnimation> AnimationData, List<string> Pivots)
	{
		LimbAnimationData Gir = new LimbAnimationData ();
		Gir.AnimationName = AnimationName;
		Gir.AnimationFrames = AnimFrames;
		Gir.canBeMirrored = m;
		Gir.AnimationData = AnimationData;
		Gir.Pivots = Pivots;
		return Gir;
	}

	public LimbAnimationData Mirror()
	{
		LimbAnimationData mirrorCopy = new LimbAnimationData ();

		mirrorCopy.AnimationFrames = AnimationFrames;
		mirrorCopy.AnimationName = AnimationName+" mirrored";
		mirrorCopy.isMirrored = true;

		foreach (string boneName in Pivots) {
			if (boneName[0]=='l'){
				mirrorCopy.Pivots.Add ('r' + boneName.Substring (1));
			} else if (boneName[0]=='r'){
				mirrorCopy.Pivots.Add ('l' + boneName.Substring (1));
			} 
		}

		mirrorCopy.AnimationData = new List<LimbAnimation> ();
		foreach (LimbAnimation boneMove in AnimationData) {
			LimbAnimation newMove = new LimbAnimation ();
			if (boneMove.BoneName [0] == 'l') {
				newMove.BoneName = ('r' + boneMove.BoneName.Substring (1));
			} else if (boneMove.BoneName [0] == 'r') {
				newMove.BoneName = ('l' + boneMove.BoneName.Substring (1));
			} else {
				newMove.BoneName = boneMove.BoneName;
			}
			newMove.Type = boneMove.Type;
			newMove.StartFrame = boneMove.StartFrame;
			newMove.EndFrame = boneMove.EndFrame;

			switch (boneMove.Type) {
			case LimbAnimation.type_emote:
			case LimbAnimation.type_scale:
			case LimbAnimation.type_scale_absolute:
				newMove.Data =  new Vector3 (boneMove.Data.x,boneMove.Data.y,  boneMove.Data.z);
				break;
			case LimbAnimation.type_rotate:
				newMove.Data = new Vector3 (0 - boneMove.Data.x,0 -boneMove.Data.y,  boneMove.Data.z);
				break;
			case LimbAnimation.type_rotate_relative:
				newMove.Data = new Vector3 (boneMove.Data.x,boneMove.Data.y,  boneMove.Data.z);
				break;
			case LimbAnimation.type_move:
			case LimbAnimation.type_move_absolute:
				newMove.Data = new Vector3 (0 - boneMove.Data.x,boneMove.Data.y, 0 - boneMove.Data.z);
				break;
			}
			mirrorCopy.AnimationData.Add (newMove);
		}
		return mirrorCopy;
	}
}

public class DataMesh
{		
	public string MeshName="";
	public int DefaultFacing=1;
	public List<LimbBodyData> BodyData;
	public List<LimbAnimationData> AnimationData;

	public static DataMesh LoadDefaultMesh ()
	{
		DataMesh Panty = new DataMesh ();

			Panty.MeshName = "Default";
			Panty.DefaultFacing = 1;

			Panty.BodyData = new List<LimbBodyData> {
				LimbBodyData.Make (
					"neck",
				"",
				new Vector3 (0,0,0),//Attach
				new Vector3 (0, 0,0),//Center
					new Vector3 (0.2f, 0.1f,0.2f),
					new Vector3 (0,90,20),//StartRot
				new Vector2(80,90),
					100,
					"torso"
			),
			LimbBodyData.Make (
				"head",
				"neck",
				new Vector3 (0, 0.5f,0),
				new Vector3 (0, 0.5f,0),
				new Vector3 (1f, 1f,1f),//Size
				new Vector3 (0, 80,30),//StartRot
				new Vector2(80,90),
				100,
				"head","reverseY","voxel"
			),
				LimbBodyData.Make (
					"rchest",
					"neck",
					new Vector3 (0,-0.5f,0),//Attach
					new Vector3 (0, -0.5f,0.5f),//Center
					new Vector3 (0.2f, 0.2f,0.2f),//Size
					new Vector3 (-90,0,0),//StartRot
				new Vector2(20,30),
					100,
					"torso"
				),
				LimbBodyData.Make (
					"lchest",
					"neck",
					new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0, -0.5f,-0.5f),//Center
				new Vector3 (0.2f, 0.2f,0.2f),//Size
				new Vector3 (0,45,0),//StartRot
				new Vector2(20,30),
					50,
					"torso"
				),
				LimbBodyData.Make (
					"rabdomen",
					"rchest",
					new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0, -0.5f,0),//Center
				new Vector3 (0.2f, 0.2f,0.2f),//Size
				new Vector3 (0,45,0),//StartRot
				new Vector2(20,30),
					50,
					"torso"
				),
				LimbBodyData.Make (
					"labdomen",
					"lchest",
					new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0, -0.5f,0),//Center
				new Vector3 (0.2f, 0.2f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(20,30),
					50,
					"torso"
			),
			LimbBodyData.Make (
				"rwaist",
				"rabdomen",
				new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0, -0.5f,0),//Center
				new Vector3 (0.2f, 0.2f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(20,30),
				50,
				"torso"
			),
			LimbBodyData.Make (
				"lwaist",
				"labdomen",
				new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0, -0.5f,0),//Center
				new Vector3 (0.2f, 0.2f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(20,30),
				50,
				"torso"
			),
				LimbBodyData.Make (
					"rarm",
					"rchest",
					new Vector3 (0,-0.25f,0.5f),//Attach
					new Vector3 (0,0,0),//Center
					new Vector3 (0.2f,0.3f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					40,
					"limb"
				),
				LimbBodyData.Make (
					"larm",
					"lchest",
					new Vector3 (0,-0.25f,-0.5f),//Attach
					new Vector3 (0,0,	0),//Center
					new Vector3 (0.2f,0.3f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					40,
					"limb"
				),
				LimbBodyData.Make (
					"rarm2",
					"rarm",
					new Vector3 (0,-0.5f,0),//Attach
					new Vector3 (0,-0.5f,0),//Center
					new Vector3 (0.2f,0.3f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					40,
					"limb"
			),
			LimbBodyData.Make (
				"larm2",
				"larm",
				new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0,-0.5f,0),//Center
				new Vector3 (0.2f,0.3f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
				40,
				"limb"
			),
			LimbBodyData.Make (
				"rhand",
				"rarm2",
				new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0,-0.5f,0),//Center
				new Vector3 (0.2f, 0.2f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
				80,
				"hand"
			),
			LimbBodyData.Make (
				"lhand",
				"larm2",
				new Vector3 (0,-0.5f,0),//Attach
				new Vector3 (0,-0.5f,0),//Center
				new Vector3 (0.2f, 0.2f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
				80,
				"hand"
			),
			LimbBodyData.Make (
				"rhand_attach",
				"rhand",
				new Vector3 (0,0,0),//Attach
				new Vector3 (0,0,0),//Center
				new Vector3 (1,1,1),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
				80,
				"attach"
			),
			LimbBodyData.Make (
				"lhand_attach",
				"lhand",
				new Vector3 (0,0,0),//Attach
				new Vector3 (0,0,0),//Center
				new Vector3 (1,1,1),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
				80,
				"attach"
			),
				LimbBodyData.Make (
					"rleg",
					"rwaist",
					new Vector3 (0,-0.5f,-0.5f),//Attach
					new Vector3 (0, -0.5f,0.5f),//Center
					new Vector3 (0.2f, 0.4f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					40,
					"limb"
				),
				LimbBodyData.Make (
					"lleg",
					"lwaist",
					new Vector3 (0,-0.5f,0.5f),//Attach
					new Vector3 (0, -0.5f,-0.5f),//Center
					new Vector3 (0.2f, 0.4f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					100,
					"limb"
				),
				LimbBodyData.Make (
					"rleg2",
					"rleg",
					new Vector3 (0,-0.5f,0),//Attach
					new Vector3 (0, -0.5f,0),//Center
					new Vector3 (0.2f, 0.4f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					40,
					"limb"
				),
				LimbBodyData.Make (
					"lleg2",
					"lleg",
					new Vector3 (0,-0.5f,0),//Attach
					new Vector3 (0, -0.5f,0),//Center
					new Vector3 (0.2f, 0.4f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					40,
					"limb"
				),
				LimbBodyData.Make (
					"lfoot",
					"lleg2",
					new Vector3 (0.3f,-0.5f,0),//Attach
					new Vector3 (0, -0.5f,0),//Center
					new Vector3 (0.4f, 0.2f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
					100,
				"pivot"
			),
			LimbBodyData.Make (
				"rfoot",
				"rleg2",
				new Vector3 (0.3f,-0.5f,0),//Attach
				new Vector3 (0, -0.5f,0),//Center
				new Vector3 (0.4f, 0.2f,0.2f),//Size
				new Vector3 (-90,0,0),//StartRot
				new Vector2(90,100),
				100,
				"pivot"
			),

			//--COSMETICS--
			LimbBodyData.Make (
				"hat1",
				"head",
				new Vector3 (0, 0.5f,0),//attach
				new Vector3 (0, 0.5f,0),//center
				new Vector3 (0.8f,0.15f,0.8f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				100,
				"cosmetic","reverseY"
			),
			LimbBodyData.Make (
				"hat2",
				"hat1",
				new Vector3 (0, 0.5f,0),//attach
				new Vector3 (0, 0.5f,0),//center
				new Vector3 (0.5f,0.2f,0.5f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				1,
				"cosmetic","immortal","reverseY"
			),
			LimbBodyData.Make (
				"hat3",
				"hat2",
				new Vector3 (0, 0.5f,0),//attach
				new Vector3 (0, 0.5f,0),//center
				new Vector3 (0.35f,0.2f,0.35f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				1,
				"cosmetic","immortal","reverseY"
			),
			LimbBodyData.Make (
				"hat4",
				"hat3",
				new Vector3 (0, 0.5f,0),//attach
				new Vector3 (0, 0.5f,0),//center
				new Vector3 (0.2f,0.2f,0.2f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				1,
				"cosmetic","immortal","reverseY"
			),

			//--WEAPONS--
			LimbBodyData.Make (
				"bazooka",
				"",
				new Vector3 (0,0,0),//Center
				new Vector3 (0.5f,0,0),//Attach
				new Vector3 (0.5f,0.6f,0.5f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				100,
				"weapon"
			),
			LimbBodyData.Make (
				"bazooka_front",
				"bazooka",
				new Vector3 (0,-0.5f,0),//Center
				new Vector3 (0,0,0),//Attach
				new Vector3 (0.4f,0.1f, 0.4f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				100,
				"weapon"
			),
			LimbBodyData.Make (
				"bazooka_back",
				"bazooka",
				new Vector3 (0,0.5f,0),//Attach
				new Vector3 (0,0,0),//Center
				new Vector3 (0.4f,0.1f, 0.4f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				100,
				"weapon"
			),
			LimbBodyData.Make (
				"bazooka_payload",
				"bazooka_back",
				new Vector3 (0,0.5f,0),//Attach
				new Vector3 (0,0,0),//Center
				new Vector3 (0.35f,0.35f, 0.35f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				100,
				"weapon"
			),
			LimbBodyData.Make (
				"bazooka_fuse",
				"bazooka_payload",
				new Vector3 (0,0.5f,0),//Attach
				new Vector3 (0,0.5f,0),//Center
				new Vector3 (0.1f,0.1f, 0.1f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				100,
				"weapon"
			),
			LimbBodyData.Make (
				"bazooka_particle",
				"bazooka_fuse",
				new Vector3 (0,0,0),//Attach
				new Vector3 (0,0,0),//Center
				new Vector3 (1f,1f, 0f),//Size
				new Vector3 (0.5f,0.25f,1f),//StartRot
				Vector2.zero,
				100,
				"particle"
			),

			//--GRENADE--

			LimbBodyData.Make (
				"grenade",
				"",
				new Vector3 (0,0,0),//Center
				new Vector3 (0.25f,0,0),//Attach
				new Vector3 (1f,1f,1f),//Size
				new Vector3 (0,0,0),//StartRot
				Vector2.zero,
				100,
				"weapon","voxel"
			),

			LimbBodyData.Make (
				"grenade_bubbles",
				"grenade",
				new Vector3 (0,0,0),//Center
				new Vector3 (0,0,0),//Attach
				new Vector3 (10f,10f,1f),//X,Y,Height
				new Vector3 (0.2f,0.2f,0.2f),//duration,fade,interval
				Vector2.zero,
				100,
				"particle"
			),
			};

			float totScale =  3/2.5f;
			foreach (LimbBodyData data in Panty.BodyData) {
				data.Dimensions *= totScale;
			}

		Panty.AnimationData = new List<LimbAnimationData> () {

			LimbAnimationData.Make ("select.0", 30, true,
					new List<LimbAnimation> () {
						//hand wave
						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 0, 8, new Vector3 (-120, 0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 3, 8, new Vector3 ( -60, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rhand", 3, 8, new Vector3 ( -20, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "larm", 0, 8, new Vector3 (20, 0, 0)),

						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 8, 13, new Vector3 (-90, 0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 8, 13, new Vector3 ( -35, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rhand", 8, 13, new Vector3 ( 20, 0,0)),

						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 13, 20, new Vector3 (-110, 0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 13, 20, new Vector3 ( -65, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rhand", 13, 20, new Vector3 ( -25, 0,0)),

						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 20, 24, new Vector3 (-90, 0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 20, 24, new Vector3 ( -35, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rhand", 20, 24, new Vector3 ( 20, 0,0)),


						//twist head
						new LimbAnimation (LimbAnimation.type_rotate, "lchest", 0, 6, new Vector3 ( 8, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rchest", 0, 6, new Vector3 ( 8, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 0, 6, new Vector3 ( 8, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 0, 6, new Vector3 ( 8, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 0, 6, new Vector3 ( 4, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 0, 6, new Vector3 ( 4, 0,0)),

					//head nod
					new LimbAnimation (LimbAnimation.type_rotate, "neck", 0, 6, new Vector3 ( -20, 0,0)),
						//new LimbAnimation (LimbAnimation.type_scale, "head", 0, 3, new Vector3 ( 1.4f, 1.4f,1.4f)),
						//new LimbAnimation (LimbAnimation.type_scale, "head", 3, 6, new Vector3 ( 1, 1,1)),
					new LimbAnimation (LimbAnimation.type_move, "neck",  0, 6, new Vector3 ( 0, 0, -0.11f)),
					new LimbAnimation (LimbAnimation.type_move, "neck",  5, 8, new Vector3 ( 0, -0.5f*0.4f, -0.11f)),
					new LimbAnimation (LimbAnimation.type_move, "neck", 24, 28, new Vector3 ( 0, 0, 0)),

						//Tiptoe
						new LimbAnimation (LimbAnimation.type_rotate, "lfoot", 5, 8, new Vector3 ( 0, 0,-30)),
						new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 5, 8, new Vector3 ( 0, 0,-30)),
						new LimbAnimation (LimbAnimation.type_rotate, "lfoot", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 24, 28, new Vector3 ( 0, 0,0)),

						//end
						new LimbAnimation (LimbAnimation.type_rotate, "larm", 24, 28, new Vector3 (0, 0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 24, 28, new Vector3 (0, 0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 24, 30, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rhand", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "head", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "neck", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "lchest", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rchest", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 24, 28, new Vector3 ( 0, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 24, 28, new Vector3 ( 0, 0,0)),

					new LimbAnimation (LimbAnimation.type_emote, "head",4,4, new Vector3 (1,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",24,30, new Vector3 (0,0,0)),
					},

					new List<string>{ "rhand" }),

			LimbAnimationData.Make ("select.1", 30,false, //salute
					new List<LimbAnimation> () {
						//hand wave
						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 0, 4, new Vector3 (20, 0, 90)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 4, 8, new Vector3 (60, 0, 160)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 4, 8, new Vector3 ( -90, 0,0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 20, 24, new Vector3 ( 20, 0, 90)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm", 24, 28, new Vector3 ( 0, 0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 20, 24, new Vector3 ( 0, 0,0)),

						//foot
						new LimbAnimation (LimbAnimation.type_rotate, "rleg", 2, 6, new Vector3 ( 0,0, 20)),
						new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 2, 6, new Vector3 ( 0,0, -40)),
						new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 2, 6, new Vector3 ( 0,0, 20)),
						new LimbAnimation (LimbAnimation.type_rotate, "rleg", 6, 10, new Vector3 ( 0,0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 6, 10, new Vector3 ( 0,0, 0)),
						new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 6, 10, new Vector3 ( 0,0, 0)),

					new LimbAnimation (LimbAnimation.type_emote, "head",4,4, new Vector3 (2,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",24,30, new Vector3 (0,0,0)),
					},

				new List<string>{ "rhand" }),

			LimbAnimationData.Make ("select.2", 12,false,//head boop 
				new List<LimbAnimation> () {

					//head nod
					new LimbAnimation (LimbAnimation.type_scale, "head",3,6, Vector3.one*1.33f),
					new LimbAnimation (LimbAnimation.type_scale, "head",6,9, Vector3.one),

					new LimbAnimation (LimbAnimation.type_emote, "head",3,9, new Vector3 (1,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",9,11, new Vector3 (0,0,0)),
				},

				new List<string>{ "head" ,"rfoot","lfoot"}),

			LimbAnimationData.Make ("losehand.0", 30,true,//head boop 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_rotate, "head", 0, 1, new Vector3 ( 0, 50,-10)),
					new LimbAnimation (LimbAnimation.type_rotate, "neck", 0, 1, new Vector3 ( 15, 0,0)),


					new LimbAnimation (LimbAnimation.type_rotate, "lchest", 0, 1, new Vector3 ( -5, 0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rchest", 0, 1, new Vector3 ( -5, 0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 0, 1, new Vector3 ( -5, 0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 0, 1, new Vector3 ( -5, 0,0)),
					new LimbAnimation (LimbAnimation.type_scale, "labdomen", 0, 1, new Vector3 ( 1, 1.1f,1)),
					new LimbAnimation (LimbAnimation.type_scale, "lwaist", 0, 1, new Vector3 ( 1, 1.1f,1)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm", 0, 1, new Vector3 ( -30, 0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm", 0, 1, new Vector3 ( 85, -20,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2", 0, 1, new Vector3 ( 80, -20,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 0, 1, new Vector3 ( -5, 0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 0, 1, new Vector3 ( -5, 0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "head",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "neck",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "head",7,30, new Vector3 (0,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "lchest",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "rchest",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_scale, "labdomen",  0, 7,Vector3.one),
					new LimbAnimation (LimbAnimation.type_scale, "lwaist",  0, 7,Vector3.one),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "larm",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist",  0, 7,Vector3.zero),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist",  0, 7, Vector3.zero),

					new LimbAnimation (LimbAnimation.type_target, "head", 7, 40, Vector3.zero),

					new LimbAnimation (LimbAnimation.type_emote, "head",0,1, new Vector3 (3,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",29,30, new Vector3 (0,0,0)),
				},

				new List<string>{ "rhand" }),

			LimbAnimationData.Make ("death.0", 15,false,//head boop 
				new List<LimbAnimation> () {

					//watch in amazement
					new LimbAnimation (LimbAnimation.type_rotate, "head", 0, 6, new Vector3 ( 0, 0,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "neck", 0, 6, new Vector3 ( 0, 0,10)),
					new LimbAnimation (LimbAnimation.type_move, "neck", 3, 8, new Vector3 (0.3f,0,  0)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm", 2, 8, new Vector3 ( 0, 0,-10)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm", 2, 8, new Vector3 ( 0, 0,-10)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 2, 8, new Vector3 ( 5, 0,10)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 2, 8, new Vector3 ( -10, 0,-110)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg", 2, 8, new Vector3 ( -5, 0,10)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 2, 8, new Vector3 ( 10, 0,-110)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 2, 8, new Vector3 ( 0, 0,-45)),
					new LimbAnimation (LimbAnimation.type_rotate, "lfoot", 2, 8, new Vector3 ( 0, 0,-45)),


				},

				new List<string>{ "rleg","lleg" }),
			LimbAnimationData.Make ("stand.0", 60,true,//stand 
				new List<LimbAnimation> () {
					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,100, new Vector3 (0,0,0))
				}, new List<string>{"rfoot"}),
			LimbAnimationData.Make ("stand.1", 60,true,//blink 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_emote, "head",1,2, new Vector3 (1,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",2,3, new Vector3 (0,0,0)),
					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,100, new Vector3 (0,0,0))
				}, new List<string>{"rfoot"}),
			LimbAnimationData.Make ("stand.2", 60,true,//blink x2 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_emote, "head",1,2, new Vector3 (1,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",2,3, new Vector3 (0,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",6,7, new Vector3 (1,0,0)),
					new LimbAnimation (LimbAnimation.type_emote, "head",7,8, new Vector3 (0,0,0)),
					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,100, new Vector3 (0,0,0))
				}, new List<string>{"rfoot"}),

			LimbAnimationData.Make ("turn.0", 2,false,
				new List<LimbAnimation> () {
					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,100, new Vector3 (0,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "head",1,2, new Vector3 (0,80,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "neck", 1, 2, new Vector3 ( 0,40, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 1, 2, new Vector3 ( 0,-20f, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 1, 2, new Vector3 ( 0,-20f, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 1, 2, new Vector3 ( 0,-20f, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 1, 2, new Vector3 ( 0,-20f, 0)),
				},

				new List<string>{ "rfoot","lfoot"}),

			LimbAnimationData.Make ("rotate.0", 20,false,
				new List<LimbAnimation> () {
					//new LimbAnimation (LimbAnimation.type_locktracking, "head",0,100, new Vector3 (0,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "head",0,1, new Vector3 (0,80,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "head",0,10, new Vector3 (0,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "neck", 0, 1, new Vector3 ( 0,40, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 0, 1, new Vector3 ( 0,-20f, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 0, 1, new Vector3 ( 0,-20f, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 0, 1, new Vector3 ( 0,-20f, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 0, 1, new Vector3 ( 0,-20f, 0)),

					new LimbAnimation (LimbAnimation.type_rotate, "neck", 5, 15, new Vector3 ( 0,0, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 5, 15, new Vector3 ( 0,0, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen",5 , 15, new Vector3 ( 0,0, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 5, 15, new Vector3 ( 0,0, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 5, 15, new Vector3 ( 0,0, 0)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 0, 10, new Vector3 ( 0,0, 40)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 0, 10, new Vector3 ( 0,0, -80)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 5, 10, new Vector3 ( 0,0, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 5, 10, new Vector3 ( 0,0, 0)),

					new LimbAnimation (LimbAnimation.type_rotate, "rleg", 10, 15, new Vector3 ( 0,0, 40)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 10, 15, new Vector3 ( 0,0, -80)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg", 15, 20, new Vector3 ( 0,0, 0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 15, 20, new Vector3 ( 0,0, 0)),
				},

				new List<string>{ "rfoot","lfoot"}),
			
			LimbAnimationData.Make ("walk.0", 15,true,//stand 
				new List<LimbAnimation> () {


					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 0, 1, new Vector3 ( 0,0,-12)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 0, 1, new Vector3 ( 0,0,-12)),

					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 0, 3, new Vector3 ( 0,0,-25)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg", 0, 3, new Vector3 ( 0,0,-15)),

					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 3, 6, new Vector3 ( 0,0,-25)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg", 3, 6, new Vector3 ( 0,0,25)),

					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 6, 9, new Vector3 ( 0,0,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg", 6, 9, new Vector3 ( 0,0,30)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 6, 9, new Vector3 ( 0,0,15)),

					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 9, 12, new Vector3 ( 0,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg", 9, 12, new Vector3 ( 0,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 9, 12, new Vector3 ( 0,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "rleg2", 12, 15, new Vector3 ( 0,0,-12)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot", 12, 15, new Vector3 ( 0,0,-12)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 0, 1, new Vector3 ( 0,0,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 0, 1, new Vector3 ( 0,0,30)),
					new LimbAnimation (LimbAnimation.type_rotate, "lfoot", 0, 1, new Vector3 ( 0,0,15)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 0, 3, new Vector3 ( 0,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 0, 3, new Vector3 ( 0,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lfoot", 0, 3, new Vector3 ( 0,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 3, 6, new Vector3 ( 0,0,-12)),
					new LimbAnimation (LimbAnimation.type_rotate, "lfoot", 3, 6, new Vector3 ( 0,0,-12)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 6, 9, new Vector3 ( 0,0,-25)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 6, 9, new Vector3 ( 0,0,-15)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 9, 12, new Vector3 ( 0,0,-25)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 9, 12, new Vector3 ( 0,0,25)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2", 12, 15, new Vector3 ( 0,0,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg", 12, 15, new Vector3 ( 0,0,30)),
					new LimbAnimation (LimbAnimation.type_rotate, "lfoot", 12, 15, new Vector3 ( 0,0,15)),

					new LimbAnimation (LimbAnimation.type_rotate, "rarm", 0, 1, new Vector3 ( 0, 0,10)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm", 0, 9, new Vector3 ( 0, 0,-10)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm", 9, 15, new Vector3 ( 0, 0,10)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 0, 1, new Vector3 ( 0, 0,10)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 0, 9, new Vector3 ( 0, 0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm2", 9, 15, new Vector3 ( 0, 0,10)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm", 0, 1, new Vector3 ( 0, 0,-10)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm", 0, 9, new Vector3 ( 0, 0,10)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm", 9, 15, new Vector3 ( 0,0,-10)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2", 0, 1, new Vector3 ( 0, 0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2", 0, 9, new Vector3 ( 0, 0,10)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2", 9, 15, new Vector3 ( 0,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "head", 0, 1, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "neck", 0, 1, new Vector3 ( 0, 10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 0, 1, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 0, 1, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 0, 1, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 0, 1, new Vector3 ( 0, -10,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "head", 0, 9, new Vector3 ( 0, 10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "neck", 0, 9, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 0, 9, new Vector3 ( 0, 10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 0, 9, new Vector3 ( 0, 10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 0, 9, new Vector3 ( 0, 10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 0, 9, new Vector3 ( 0, 10,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "head", 9, 15, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "neck", 9, 15, new Vector3 ( 0, 10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist", 9, 15, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rabdomen", 9, 15, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist", 9, 15, new Vector3 ( 0, -10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "labdomen", 9, 15, new Vector3 ( 0, -10,0)),

				}, new List<string>{"rfoot","lfoot"}),
			LimbAnimationData.Make ("prejump.0", 10,true, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_rotate, "rarm",0,1, new Vector3 (0,30,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (0,-30,-20)),

					new LimbAnimation (LimbAnimation.type_rotate, "head",0,1, new Vector3 (0,0, 40)),
					new LimbAnimation (LimbAnimation.type_rotate, "neck",0,1, new Vector3 (0,0,-40)),

					new LimbAnimation (LimbAnimation.type_rotate, "rchest",0,1, new Vector3 (0,0,20)),
					new LimbAnimation (LimbAnimation.type_rotate, "lchest",0,1, new Vector3 (0,0,20)),

					new LimbAnimation (LimbAnimation.type_rotate, "lwaist",0,1, new Vector3 (0,0,20)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist",0,1, new Vector3 (0,0,20)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg",0,1, new Vector3 (0,10,50)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg",0,1, new Vector3 (0,-10,50)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2",0,1, new Vector3 (0,-10,-100)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg2",0,1, new Vector3 (0,10,-100)),

					new LimbAnimation (LimbAnimation.type_rotate, "lfoot",0,1, new Vector3 (0,0,50)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot",0,1, new Vector3 (0,0,50)),

				}, new List<string>{"rfoot"}),
			LimbAnimationData.Make ("land.0", 10,true, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_rotate, "rarm",0,1, new Vector3 (0,30,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (0,-30,-20)),

					new LimbAnimation (LimbAnimation.type_rotate, "head",0,1, new Vector3 (0,0, 40)),
					new LimbAnimation (LimbAnimation.type_rotate, "neck",0,1, new Vector3 (0,0,-40)),

					new LimbAnimation (LimbAnimation.type_rotate, "rchest",0,1, new Vector3 (0,0,20)),
					new LimbAnimation (LimbAnimation.type_rotate, "lchest",0,1, new Vector3 (0,0,20)),

					new LimbAnimation (LimbAnimation.type_rotate, "lwaist",0,1, new Vector3 (0,0,20)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist",0,1, new Vector3 (0,0,20)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg",0,1, new Vector3 (0,10,50)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg",0,1, new Vector3 (0,-10,50)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg2",0,1, new Vector3 (0,-10,-100)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg2",0,1, new Vector3 (0,10,-100)),

					new LimbAnimation (LimbAnimation.type_rotate, "lfoot",0,1, new Vector3 (0,0,50)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot",0,1, new Vector3 (0,0,50)),

				}, new List<string>{"rfoot"}),
			LimbAnimationData.Make ("jump.0", 8,true, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_rotate, "rarm",0,1, new Vector3 (0,10,40)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm2",0,1, new Vector3 (0,0,80)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (0,-30,-20)),


					new LimbAnimation (LimbAnimation.type_rotate, "rleg",0,1, new Vector3 (0,0,30)),
					new LimbAnimation (LimbAnimation.type_rotate, "rleg2",0,1, new Vector3 (0,0,-80)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg",0,1, new Vector3 (0,0,-15)),

					new LimbAnimation (LimbAnimation.type_rotate, "rfoot",0,1, new Vector3 (0,0,-15)),
					new LimbAnimation (LimbAnimation.type_rotate, "lfoot",0,1, new Vector3 (0,0,-45)),

				}, new List<string>{"rfoot"}),
			LimbAnimationData.Make ("strongjump.0", 4,true, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_rotate, "head",0,1, new Vector3 (0,0,10)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (10,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm",0,1, new Vector3 (-10,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "lfoot",0,1, new Vector3 (0,0,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot",0,1, new Vector3 (0,0,-20)),

				}, new List<string>{"rfoot"}),

			LimbAnimationData.Make ("fall.0", 10,true, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_rotate, "head",0,1, new Vector3 (0,0,-20)),

					new LimbAnimation (LimbAnimation.type_rotate, "lchest",0,1, new Vector3 (0,0,3)),
					new LimbAnimation (LimbAnimation.type_rotate, "rchest",0,1, new Vector3 (0,0,3)),
					new LimbAnimation (LimbAnimation.type_rotate, "lwaist",0,1, new Vector3 (0,0,3)),
					new LimbAnimation (LimbAnimation.type_rotate, "rwaist",0,1, new Vector3 (0,0,3)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (60,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm",0,1, new Vector3 (-60,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",0,1, new Vector3 (5,-10,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "rarm2",0,1, new Vector3 (-5,-10,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "lleg",0,1, new Vector3 (0,0,60)),
					new LimbAnimation (LimbAnimation.type_rotate, "lleg2",0,1, new Vector3 (0,0,-110)),
					new LimbAnimation (LimbAnimation.type_rotate, "lfoot",0,1, new Vector3 (0,0,-20)),
					new LimbAnimation (LimbAnimation.type_rotate, "rfoot",0,1, new Vector3 (0,0,-20)),

				}, new List<string>{"lfoot"}),

			LimbAnimationData.Make ("hold.launcher.twohanded.0", 2,false, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "lhand",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "larm",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "rhand",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "rarm",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_emote, "head",0,2, new Vector3(2,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (0,20,80)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",0,1, new Vector3 (0,-20,20)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "lhand",0,1, new Vector3 (0,0,90)),

					new LimbAnimation (LimbAnimation.type_rotate_relative, "rarm",0,1, new Vector3 (0,10,60)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "rarm2",0,1, new Vector3 (0,50,90)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "rhand",0,1, new Vector3 (0,80,180)),

					new LimbAnimation (LimbAnimation.type_scale, "lhand_attach",0,1, Vector3.one),
					new LimbAnimation (LimbAnimation.type_scale, "lhand_attach",1,2, new Vector3 (1.4f,0.7f,1.4f)),

				}, new List<string>{"lhand", "rhand","lfoot","rfoot"}),

			LimbAnimationData.Make ("fire.launcher.twohanded.0", 6,false, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,20, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "lhand",0,20, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "larm",0,20, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "rhand",0,20, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "rarm",0,20, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_emote, "head",0,20, new Vector3(1,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (0,20,80)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",0,1, new Vector3 (0,-20,20)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "lhand",0,1, new Vector3 (0,0,90)),

					new LimbAnimation (LimbAnimation.type_rotate, "rarm",0,1, new Vector3 (0,10,60)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "rarm2",0,1, new Vector3 (0,50,90)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "rhand",0,1, new Vector3 (0,80,180)),

					new LimbAnimation (LimbAnimation.type_scale, "lhand_attach",0,4, new Vector3 (1f,1.3f,1)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,4, new Vector3 (0,20,80+40)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",0,4, new Vector3 (0,-20,20+40)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "lhand",0,4, new Vector3 (0,0,90+40)),

				}, new List<string>{"lhand", "rhand","lfoot","rfoot"}),

			LimbAnimationData.Make ("hold.thrown.twohanded.0", 2,false, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "lhand",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_locktracking, "larm",0,2, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_emote, "head",0,2, new Vector3(2,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (0,0,0)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",0,1, new Vector3 (0,0,90)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "lhand",0,1, new Vector3 (0,0,90)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",1,2, new Vector3 (0,90,90)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",1,2, new Vector3 (0,0,90)),
					new LimbAnimation (LimbAnimation.type_rotate_relative, "lhand",1,2, new Vector3 (0,0,90)),


				}, new List<string>{"lhand", "rhand","lfoot","rfoot"}),

			LimbAnimationData.Make ("fire.thrown.twohanded.0", 5,false, 
				new List<LimbAnimation> () {

					new LimbAnimation (LimbAnimation.type_locktracking, "head",0,6, Vector3.zero),
					new LimbAnimation (LimbAnimation.type_emote, "head",0,6, new Vector3(2,0,0)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",0,1, new Vector3 (0,45,90)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",0,1, new Vector3 (0,0,90)),

					new LimbAnimation (LimbAnimation.type_rotate, "larm",1,3, new Vector3 (30,0,90)),
					new LimbAnimation (LimbAnimation.type_rotate, "larm2",1,3, new Vector3 (0,0,0)),


				}, new List<string>{"lhand", "rhand","lfoot","rfoot"}),

		};

		/*
		 * ANIM NAMES


scared
threaten
greet

		stand nolegs
		idle

		Aim bazooka twohanded
		onehand
		aim rifle twohanded
		onehand
		aim thrown twohanded
		onehand
		aim no legs

		walk

		stand wounded stomach
		walk wounded torso

		jump missing one leg
		
		lose leg
		
		lose head

*/

		return Panty;
	}
}

public class LimbAnimation
{
	public const int type_rotate = 0;
	public const int type_scale = 1;
	public const int type_move = 2;
	public const int type_emote = 3;
	public const int type_target = 4;
	public const int type_rotate_relative = 5;
	public const int type_move_absolute = 6;
	public const int type_scale_absolute = 7;
	public const int type_locktracking = 8;

	public int Type=0;
	public string BoneName="";

	public float StartFrame=0;
	public float EndFrame=1;
	public Vector3 Data=Vector3.zero;

	public LimbAnimation()
	{

	}

	public LimbAnimation(int t, string Bone, float FrameA, float FrameB, Vector3 vectorData)
	{
		Type = t;
		BoneName = Bone;

		StartFrame=FrameA;
		EndFrame=FrameB;
		Data=vectorData;
	}

}