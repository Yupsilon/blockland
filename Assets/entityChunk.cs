﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FaceData
{
public	entityBlock block; 
	public Vector3 center;public Vector3 height;public Vector3 width;public bool reversed;public int colorID;

	public FaceData (entityBlock b, Vector3 vC, Vector3 vU, Vector3 vR, bool rev, int color)
	{
		block = b;
		center = vC;
		height = vU;
		width = vR;
		reversed = rev;
		colorID = color;
	}

	public List<Vector3> getCoordinates()
	{
		return  new List<Vector3> () {
			center,				center+ width/2f,				center+ width,

			center+ height/2f,		center+ width/2f + height/2f,		center+ width,

			center+ height,	center+ width/2f + height,	center+ width + height,
		};
	}
	public char getName()
	{
		if (height==Vector3.up && width == Vector3.forward)
		{
			if (!reversed) {
				return 'l';
			} else {
				return'r';
			}
		}
		else if (height==Vector3.forward && width == Vector3.right)
		{
			if (!reversed) {
				return 'd';
			} else {
				return'u';
			}
		}
		else if (height==Vector3.up && width == Vector3.right)
		{
			if (!reversed) {
				return 'b';
			} else {
				return'f';
			}
		}
		return '-';
	}
}

public class entityChunk
{
	public GameMission game;
	public Mesh visualMesh;
	public GameObject MapDisplay;
	public static float ChunkSize=10 ;

	public bool hasUpdated=false;
	Vector3 Pos=Vector3.zero;
	entityBlock[,,] MapData;

	bool isEmpty()
	{
		foreach (entityBlock bData in MapData) {
			if (bData != null) {
				return false;
			}
		}
		return true;
	}

	public entityChunk (GameMission g,Vector3 v, float d)
	{
		game = g;
		Pos = v;
		MapDisplay = GameObject.CreatePrimitive (PrimitiveType.Cube);
		MapDisplay.tag = "ground";
		GameObject.Destroy(MapDisplay.GetComponent<BoxCollider> ());
		MapDisplay.AddComponent<MeshCollider> ();
		enableCollision (false);
		MapDisplay.name = "Chunk " + v.ToString ();
		MapData = game.GetMapPart (v, v + Vector3.one * d);
	}

	public void Draw() {

		if (isEmpty ()) {
			MapDisplay.layer=(LayerMask.NameToLayer("LayerNoDraw"));
			MapDisplay.SetActive (false);
			return;
		}

		MapDisplay.layer=(LayerMask.NameToLayer("Default"));
		MapDisplay.transform.position = Pos;
		MapDisplay.GetComponent<MeshFilter>().mesh = DrawMesh();
		MapDisplay.GetComponent<MeshCollider> ().sharedMesh=MapDisplay.GetComponent<MeshFilter>().mesh;

		Texture2D meshTexture = new Texture2D (colorFaceData.Count, 1);

		for (int iC = 0; iC < colorFaceData.Count; iC++) {
			meshTexture.SetPixel (iC, 0, colorFaceData [iC]);
		}
		meshTexture.Apply ();
		meshTexture.filterMode = FilterMode.Point;

		MapDisplay.GetComponent<MeshRenderer> ().material = Material.Instantiate(Resources.Load<Material>("Materials/Ground"));
		MapDisplay.GetComponent<MeshRenderer> ().material.mainTexture = meshTexture;
	}

	List<Vector3> verts = new List<Vector3>();
	List<Vector2> uvs = new List<Vector2>();
	List<int> tris = new List<int>();

	List<Color32> colorFaceData = new List<Color32> ();
	public Mesh DrawMesh() {
		visualMesh = new Mesh();
		verts.Clear ();
		uvs.Clear ();
		tris.Clear ();

		colorFaceData.Clear ();
		List<FaceData> fData = new List<FaceData> ();

		for (int x = 0; x < Mathf.Min(ChunkSize,MapData.GetLength(0)); x++)
		{
			for (int y = 0; y < Mathf.Min(ChunkSize,MapData.GetLength(1)); y++)
			{
				for (int z = 0; z < Mathf.Min(ChunkSize,MapData.GetLength(2)); z++)
				{
					entityBlock brick = MapData[x,y,z];
					if (brick != null) {

						int colIndex = colorFaceData.Count;
						Color32 myCol = brick.getColor ();
						if (colorFaceData.Contains(myCol)) {
							colIndex = colorFaceData.IndexOf (myCol);
						} else {
							colorFaceData.Add (myCol);
						}

						List<entityBlock> neighbors = brick.GetNeighbours ();
						// Left wall
						if (neighbors [4] == null) {
							fData.Add(new FaceData (brick, new Vector3 (x, y, z), Vector3.up, Vector3.forward, false,colIndex));
						}
						// Right wall
						if (neighbors [2] == null)
							fData.Add(new FaceData (brick, new Vector3 (x + 1, y, z), Vector3.up, Vector3.forward, true,colIndex));

						// Bottom wall
						if (neighbors [5] == null)
							fData.Add(new FaceData (brick, new Vector3 (x, y, z), Vector3.forward, Vector3.right, false,colIndex));
						// Top wall
						if (neighbors [0] == null)
							fData.Add(new FaceData (brick, new Vector3 (x, y + 1, z), Vector3.forward, Vector3.right, true,colIndex));

						// Back
						if (neighbors [3] == null)
							fData.Add(new FaceData (brick, new Vector3 (x, y, z), Vector3.up, Vector3.right, true,colIndex));
						// Front
						if (neighbors [1] == null)
							fData.Add(new FaceData (brick, new Vector3 (x, y, z + 1), Vector3.up, Vector3.right, false,colIndex));
					}

				}
			}
		}

		/*foreach (FaceData face in fData) {
			foreach (Vector3 Zim in face.getCoordinates()) {
				game.level.verts.Add (Zim * entityStage.a);
			}
		}
		foreach (FaceData face in fData) {
			foreach (Vector3 Zim in face.getCoordinates()) {
				game.level.smoothVectorAt (Zim);
			}
		}*/
		foreach (FaceData face in fData) {
			BuildFace (face, colorFaceData.Count);
		}

		visualMesh.vertices = verts.ToArray();
		visualMesh.uv = uvs.ToArray();
		visualMesh.triangles = tris.ToArray();
		visualMesh.RecalculateBounds();
		visualMesh.RecalculateNormals();

		return visualMesh;
	}
	public void deleteBlockAt(Vector3 bPos)
	{
		foreach (entityBlock Zim in MapData [(int)(bPos.x - Pos.x), (int)(bPos.y - Pos.y), (int)(bPos.z - Pos.z)].GetNeighbours()) {
			if (Zim != null) {
				game.needsUpdate.Add (Zim.getParent ());
			}
		}
		MapData [(int)(bPos.x - Pos.x), (int)(bPos.y - Pos.y), (int)(bPos.z - Pos.z)]=null;

		game.Tileset [(int)(bPos.x )][ (int)(bPos.y)][(int)(bPos.z)] = null;

		game.needsUpdate.Add (this);
	}

	public void BuildFace(FaceData data,int colorsMax)
	{
		BuildFace (data.block, data.center, data.height, data.width, data.reversed, data.colorID, colorsMax);
		
	}
	public void BuildFace(entityBlock block, Vector3 corner, Vector3 up, Vector3 right, bool reversed,float colorID, float colorsMax)
	{
		BuildFace(block,corner,corner + up,corner + up + right,corner + right,reversed,colorID,colorsMax);
	}
	public void BuildFace(entityBlock block, Vector3 a, Vector3 b, Vector3 c, Vector3 d, bool reversed,float colorID, float colorsMax)
	{
		int index = verts.Count;

		verts.Add (a);
		verts.Add (b);
		verts.Add (c);
		verts.Add (d); 


		Vector2 uvWidth = new Vector2(1f/colorsMax,1f)/3f;
		Vector2 uvCorner = new Vector2(colorID/colorsMax+uvWidth.x, uvWidth.y);

		uvs.Add(uvCorner);
		uvs.Add(new Vector2(uvCorner.x, uvCorner.y + uvWidth.y));
		uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y + uvWidth.y));
		uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y));


		if (reversed)
		{
			tris.Add(index + 0);
			tris.Add(index + 1);
			tris.Add(index + 2);
			tris.Add(index + 2);
			tris.Add(index + 3);
			tris.Add(index + 0);
		}
		else
		{
			tris.Add(index + 1);
			tris.Add(index + 0);
			tris.Add(index + 2);
			tris.Add(index + 3);
			tris.Add(index + 2);
			tris.Add(index + 0);
		}

	}


	public void enableCollision(bool towhat)
	{
		MapDisplay.GetComponent<MeshCollider> ().enabled = towhat;
	}
}

