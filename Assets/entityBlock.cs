﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class propertyBlock
{
	public Vector3 vPos;
	public Color32 color;

	public propertyBlock(Vector3 v, Color32 c)
	{
		vPos = v;
		color = c;
	}
}

public class propertyBlockDamage
{
	public Vector3 center;
	public entityBlock block;
	public float damage=0f;

	public propertyBlockDamage(entityBlock b,Vector3 c,float d)
	{
		block = b;
		center = c;
		damage = d;
	}
}

public class entityBlock
	{
	public GameMission game;
	public Vector3 vPos;
	public Color32 oCol;
	public float life=1f;
	public float lifetotal=1f;
	public entityBlock (GameMission g,propertyBlock data)
	{

		game = g;
		vPos = data.vPos;
		oCol = data.color;
	}
	public entityBlock (GameMission g,Vector3 pos, Color32 color)
	{
		game = g;
		vPos = pos;
		oCol = color;
	}
	public entityChunk getParent()
	{
		return game.GetChunkAt(vPos);
	}

	public void Refresh()
	{
		game.UpdateChunk (getParent ());
	}

	public entityBlock GetNeighbour(Vector3 delta)
	{
		return game.GetBlockAt (vPos + delta);
	}

	public List<entityBlock> GetNeighbours()
	{
		return GetNeighbours(game,vPos,false);
	}

	public static List<entityBlock> GetNeighbours(GameMission game, Vector3 vPos,bool real)
	{
		if (!real) {
			return new List<entityBlock> {

				game.GetBlockAt(vPos+new Vector3(0,1,0)),//top
				game.GetBlockAt(vPos+new Vector3(0,0,1)),//front
				game.GetBlockAt(vPos+new Vector3(1,0,0)),//r
				game.GetBlockAt(vPos+new Vector3(0,0,-1)),//back
				game.GetBlockAt(vPos+new Vector3(-1,0,0)),//l
				game.GetBlockAt(vPos+new Vector3(0,-1,0)),//bot
			};
		}
		return new List<entityBlock> {

			game.GetBlockAt(vPos+new Vector3(0,1,0)),//top
			game.GetBlockAt(vPos+new Vector3(0,0,1)),//front
			game.GetBlockAt(vPos+new Vector3(1,0,0)),//left
			game.GetBlockAt(vPos+new Vector3(0,0,-1)),//back
			game.GetBlockAt(vPos+new Vector3(-1,0,0)),//right
			game.GetBlockAt(vPos+new Vector3(0,-1,0)),//bot

			game.GetBlockAt(vPos+new Vector3(0,-1,1)),//bot-front
			game.GetBlockAt(vPos+new Vector3(0,-1,-1)),//bot-back
			game.GetBlockAt(vPos+new Vector3(-1,-1,0)),//bot-right
			game.GetBlockAt(vPos+new Vector3(1,-1,0)),//bot-left

			game.GetBlockAt(vPos+new Vector3(0,1,1)),//top-front
			game.GetBlockAt(vPos+new Vector3(0,1,-1)),//top-back
			game.GetBlockAt(vPos+new Vector3(-1,1,0)),//top-right
			game.GetBlockAt(vPos+new Vector3(1,1,0)),//top-left

			game.GetBlockAt(vPos+new Vector3(1,-1,1)),//right-bot-front
			game.GetBlockAt(vPos+new Vector3(1,-1,-1)),//right-bot-back
			game.GetBlockAt(vPos+new Vector3(-1,-1,1)),//left-bot-front
			game.GetBlockAt(vPos+new Vector3(-1,-1,-1)),//left-bot-back

			game.GetBlockAt(vPos+new Vector3(1,1,1)),//right-top-front
			game.GetBlockAt(vPos+new Vector3(1,1,-1)),//right-top-back
			game.GetBlockAt(vPos+new Vector3(-1,1,1)),//left-top-front
			game.GetBlockAt(vPos+new Vector3(-1,1,-1)),//left-top-back
		};
	}

	public void Die()
	{	
		life=0;
		if (getParent () != null) {
			getParent ().deleteBlockAt (vPos);
		}
	}
	public bool isRelevant()
	{
		return true;
	}
	public float takeDamage(float nDamage)
	{
		if (nDamage >= life) {
			nDamage -= life;
			life = 0;
			return nDamage;
		} else {
			life -= nDamage;
			return 0;
		}
	}
	public Color getColor()
	{
		return oCol;
	}
}
