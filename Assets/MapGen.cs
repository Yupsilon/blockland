﻿using System;
using UnityEngine;
using System.Collections.Generic;	public class MapGen
{

	public enum WorldType
	{
		Continental,
		Archipelago,
	}

	public List<float> ElevationData;
	public MapGen ()
	{ElevationData = new List<float> ();
	}
	public MapGen(int GenSize)
	{
		Generate (GenSize);
	}
	public int Height=0;
	public MapGen(int GenSize, WorldType GenType)
	{
		Height = 100;
		Generate (GenSize-1);

		switch (GenType)
		{
		case WorldType.Continental:
			ElevateHill (GenSize / 2, GenSize / 2, GenSize * 3 / 5, 25);

			float mountains = 2+UnityEngine.Random.Range (0, GenSize / 10);
			for (int il = 0; il < mountains; il++) {
				float radius = 5+15*UnityEngine.Random.value*GenSize/100;
				ElevateHill (radius+(GenSize-radius*2) *UnityEngine.Random.value, radius+(GenSize-radius*2)*UnityEngine.Random.value,radius , 10+10*UnityEngine.Random.value);

			}

			Naturalize (1, 0.5f, 4);	
			break;
		case WorldType.Archipelago:

			float islands = 20+UnityEngine.Random.Range (0, GenSize / 10);
			for (int il = 0; il < islands; il++) {
				float radius = 5+15*UnityEngine.Random.value;
		//		ElevateHill (radius+(GenSize-radius*2) *UnityEngine.Random.value, radius+(GenSize-radius*2)*UnityEngine.Random.value,radius , //GenHeight/islands);

			}

			Naturalize (0.5f, 0.1f, 2);	
			break;
	}
	}

	public float GetMinElevation()
	{
		float Gir = GetMaxElevation();

		foreach (float Zim in ElevationData)
		{
			Gir = Mathf.Min(Zim,Gir);
		}
		return Gir;
	}

	public float GetMaxElevation()
	{
		float Gir = 0;

		foreach (float Zim in ElevationData)
		{
			Gir = Mathf.Max(Zim,Gir);
		}
		return Gir;
	}

	public int GetWidth()
	{
		return Mathf.FloorToInt(Mathf.Sqrt (ElevationData.Count));
	}

	public float GetTileAt(int x, int y)
	{
		int index = y * GetWidth () + x;
		if (isOnMap(index)) {

			return ElevationData [index];
		}else{return 0;
		}
	}
	public bool isOnMap(int x,int y)
	{
		return isOnMap (y * GetWidth () + x);
	}
	public bool isOnMap(int index)
	{
		return (index >= 0 && index < ElevationData.Count);
	}
	public void SetTileAt(int x, int y, float v)
	{
		int index = y * GetWidth () + x;
		if (isOnMap(index) ){
			ElevationData [index] = v;
		}
	}

	public void Generate(int Size)
	{
		ElevationData = new List<float> ();

		for (int Zim = 0; Zim < Size * Size; Zim++) {
			ElevationData.Add (0);
		}
	}

	public void SmoothenTile(int X,int Y,int dSize,bool diag)
	{int Gir = 0;
		float Zim = 0;

		int[][] NeighData = 
			new int[][] {
			new []{ -1, 0 },
			new []{ 1, 0 },
			new []{ 0, 1 },
			new []{ 0, -1 },
		};

		if (diag) {
			NeighData = 
				new int[][] {
				new []{ 1, 1 },
				new []{ 1, -1 },
				new []{ -1, 1 },
				new []{ -1, -1 },
			};
		}

		for (int i = 0; i < NeighData.Length; i++) {
			int[] temp = NeighData[i];
			int randomIndex = UnityEngine.Random.Range(i, NeighData.Length);
			NeighData[i] = NeighData[randomIndex];
			NeighData[randomIndex] = temp;
		}
		foreach (int[] Xerxes in NeighData)
		{

			if (GetTileAt (X + dSize*Xerxes[0], Y + dSize*Xerxes[1]) > 0) {
				Zim += GetTileAt (X + dSize*Xerxes[0], Y + dSize*Xerxes[1]);
				Gir++;
			}
		}


		SetTileAt (X  , Y , Zim/Gir);

	}


	public void ElevateMountain(int X, int Y, int Rad, int Elevation)
	{
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);

				if (Dist <= Rad) {
					SetTileAt (iX, iY, Elevation- (Elevation - GetTileAt (iX, iY)) * (Dist / Rad));
				}
			}
		}
	}
	public void ElevateHill(float X, float Y, float Rad, float Elevation)
	{
		ElevateHill ((int)X, (int)Y, (int)Rad, (int)Elevation);
	}

	public void ElevateHill(int X, int Y, int Rad, int Elevation)
	{
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);
				
				if (Dist < Rad) {
					SetTileAt (iX, iY,GetTileAt (iX, iY)+  (Elevation - GetTileAt (iX, iY)) * Mathf.Cos(Dist / Rad*Mathf.PI/2f));
				}
			}
		}
	}

	public void ElevateChasm(int X, int Y, int Rad, int Elevation)
	{
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);

				if (Dist < Rad) {
					SetTileAt (iX, iY,GetTileAt (iX, iY)+  (Elevation - GetTileAt (iX, iY)) * Mathf.Cos(Dist / Rad*Mathf.PI/2f));
				}
			}
		}
	}

	public float[] GetTilesInRange(int X, int Y, int Rad)
	{
		List<float> rValue = new List<float>();
		for (int iY = Y-Rad-1; iY <= Y + Rad+1; iY++) {

			for (int iX = X-Rad-1; iX <= X + Rad+1; iX++) {
				float Dist =

					Mathf.Sqrt (
						(iX-X)*(iX-X)+
						(iY-Y)*(iY-Y)
					);

				if (isOnMap(iX,iY) && Dist < Rad) {
					rValue.Add(GetTileAt(iX,iY));}
			}
		}
		return rValue.ToArray();
	}


	public void Naturalize(float Var, float wLevel, float Max)
	{
		int dSize = 4;
		int rSize = Mathf.CeilToInt ((float)GetWidth() / (float)dSize) * dSize + 1;

		MapGen NaturalMap = new MapGen (rSize);

		NaturalMap.SetTileAt (0, 0, UnityEngine.Random.Range (1, Max));
		NaturalMap.SetTileAt (0, NaturalMap.GetWidth(), UnityEngine.Random.Range (1, Max));
		NaturalMap.SetTileAt (NaturalMap.GetWidth(), 0, UnityEngine.Random.Range (1, Max));
		NaturalMap.SetTileAt (NaturalMap.GetWidth(), NaturalMap.GetWidth(), UnityEngine.Random.Range (1, Max));


		while (dSize >1 ) {
			for (int Y = 0; Y <= NaturalMap.GetWidth() / dSize; Y++) {
				for (int X = 0; X <= NaturalMap.GetWidth() / dSize; X++) {
					if (NaturalMap.GetTileAt (X * (dSize), Y * (dSize)) == 0) {
						if (NaturalMap.GetTileAt ((X - 1) * (dSize), (Y - 1) * (dSize)) == 0 &&
							NaturalMap.GetTileAt ((X - 1) * (dSize), (Y + 1) * (dSize)) == 0 &&
							NaturalMap.GetTileAt ((X + 1) * (dSize), (Y - 1) * (dSize)) == 0 &&
							NaturalMap.GetTileAt ((X + 1) * (dSize), (Y + 1) * (dSize)) == 0) { 
							NaturalMap.SetTileAt (X * dSize, Y * dSize, UnityEngine.Random.Range (1, Max));

						}else {
							NaturalMap.SmoothenTile (X * dSize, Y * dSize, dSize,false);
							float rnd = UnityEngine.Random.Range (-Var, Var);
							NaturalMap.SetTileAt (X * dSize, Y * dSize, 

								Mathf.Clamp(
									NaturalMap.GetTileAt (X * dSize, Y * dSize) + rnd,
									0,Max)


							);
							if (float.IsNaN(NaturalMap.GetTileAt (X * dSize, Y * dSize))) {
								NaturalMap.SetTileAt (X * dSize, Y * dSize, UnityEngine.Random.Range (1, Max));

							}
						}
					}
				}
			}


			for (int Y = 0; Y <= NaturalMap.GetWidth () / dSize; Y++) {
				for (int X = 0; X <= NaturalMap.GetWidth () / dSize; X++) {


					NaturalMap.SmoothenTile (X * dSize + Mathf.FloorToInt (dSize / 2f), 
						Y * dSize + Mathf.FloorToInt (dSize / 2f), 
						Mathf.FloorToInt(dSize/2f),true);
				}
			}

			dSize /= 2;
		}

		for (int Y = 0; Y <= NaturalMap.GetWidth () ; Y++) {
			for (int X = 0; X <= NaturalMap.GetWidth () ; X++) {
				if (NaturalMap.GetTileAt (X , Y ) == 0) {

					NaturalMap.SmoothenTile (X ,Y, 1,false);

				}
			}
		}

		for (int Y = 0; Y < GetWidth () ; Y++) {
			for (int X = 0; X < GetWidth () ; X++) {
				SetTileAt (X, Y, GetTileAt(X,Y)+NaturalMap.GetTileAt (X, Y));
			}
		}

		Max = GetMaxElevation();
		for (int Y = 0; Y < GetWidth () ; Y++) {
			for (int X = 0; X < GetWidth () ; X++) {
				SetTileAt (X, Y, GetTileAt(X,Y)-Max*wLevel);

			}
		}
	}

	public void GenRiver()
	{

	}
}

