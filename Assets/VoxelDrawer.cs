﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class VoxelDrawer
{
	public Texture2D assignedTexture;
	public Mesh mData;
	public Texture2D tData;

	public bool pixelIsEmpty(int x, int y,int z)
	{
		if (x >= 0 && x < assignedTexture.height && 
			y >= 0 && y < GetMeshHeight() && 
			z >= 0 && z < assignedTexture.height) {

			if (assignedTexture.GetPixel (x+y*assignedTexture.height, z).a > 0) {
				return false;
			}
		} 
		return true;
	}

	public int GetMeshHeight()
	{
		return Mathf.FloorToInt(assignedTexture.width / assignedTexture.height);
	}

	public VoxelDrawer(Texture2D Refference,float num) {

		assignedTexture = Refference;

		List<Color32> colorFaceData = new List<Color32> ();
		List<FaceData> fData = new List<FaceData> ();

		Vector3 center = new Vector3 (Refference.height / 2f, (float)GetMeshHeight()/2f, Refference.height / 2f);
		float scale = 1f / num;

		for (int y = 0; y < GetMeshHeight(); y++) {
			

			for (int x = 0; x < Refference.height; x++) {
				for (int z = 0; z < Refference.height; z++) {
					if (!pixelIsEmpty (x, y,z)) {
						Color Panty = Refference.GetPixel (x+y*assignedTexture.height, z);

						int colIndex = colorFaceData.Count;
						if (colorFaceData.Contains (Panty)) {
							colIndex = colorFaceData.IndexOf (Panty);
						} else {
							colorFaceData.Add (Panty);
						}
						// Bottom wall
						if (pixelIsEmpty (x,y-1, z)) {
							fData.Add (new FaceData (null, new Vector3 (x, y, z), Vector3.forward, Vector3.right, false, colIndex));
						}
						// Top wall
						if (pixelIsEmpty (x,y + 1, z)) {
							fData.Add (new FaceData (null, new Vector3 (x, y+1, z), Vector3.forward, Vector3.right, true, colIndex));
						}

						// Left wall
						if (pixelIsEmpty (x - 1,y, z)) {
							fData.Add (new FaceData (null, new Vector3 (x, y, z), Vector3.up, Vector3.forward, false, colIndex));
						}
						// Right wall
						if (pixelIsEmpty (x + 1,y, z))
							fData.Add (new FaceData (null, new Vector3 (x + 1, y, z), Vector3.up, Vector3.forward, true, colIndex));

						// Back
						if (pixelIsEmpty (x,y, z - 1))
							fData.Add (new FaceData (null, new Vector3 (x, y, z), Vector3.up, Vector3.right, true, colIndex));
						// Front
						if (pixelIsEmpty (x,y, z + 1))
							fData.Add (new FaceData (null, new Vector3 (x, y, z + 1), Vector3.up, Vector3.right, false, colIndex));

					}
				}
			}
		}
		/*
		for (int x = 0; x < Refference.width; x++) {
			for (int z = 0; z < Refference.height; z++) {
				if (!pixelIsEmpty (x, z)) {
					Color Panty = Refference.GetPixel (x, z);

					int colIndex = colorFaceData.Count;
					if (colorFaceData.Contains (Panty)) {
						colIndex = colorFaceData.IndexOf (Panty);
					} else {
						colorFaceData.Add (Panty);
					}

					fData.Add (new FaceData (null, new Vector3 (x, 0, z), Vector3.forward, Vector3.right, false, colIndex));
					fData.Add (new FaceData (null, new Vector3 (x, 1, z), Vector3.forward, Vector3.right, true, colIndex));

					// Left wall
					if (pixelIsEmpty(x-1,z)) {
						fData.Add (new FaceData (null, new Vector3 (x, 0, z), Vector3.up, Vector3.forward, false, colIndex));
					}
					// Right wall
					if (pixelIsEmpty(x+1,z))
						fData.Add (new FaceData (null, new Vector3 (x + 1, 0, z), Vector3.up, Vector3.forward, true, colIndex));

					// Back
					if (pixelIsEmpty(x,z-1))
						fData.Add (new FaceData (null, new Vector3 (x, 0, z), Vector3.up, Vector3.right, true, colIndex));
					// Front
					if (pixelIsEmpty(x,z+1))
						fData.Add (new FaceData (null, new Vector3 (x, 0, z + 1), Vector3.up, Vector3.right, false, colIndex));

				}
			}
		}*/

		List<Vector3> verts = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<int> tris = new List<int>();
		foreach (FaceData face in fData) {
			int index = verts.Count;

			verts.Add ((face.center-center)*scale);
			verts.Add ((face.center-center + face.height)*scale);
			verts.Add ((face.center-center + face.height + face.width)*scale);
			verts.Add ((face.center-center + face.width)*scale); 

			Vector2 uvWidth = new Vector2(1f/colorFaceData.Count,1f)/3f;
			Vector2 uvCorner = new Vector2((float)face.colorID/(float)colorFaceData.Count+uvWidth.x, uvWidth.y);

			uvs.Add(uvCorner);
			uvs.Add(new Vector2(uvCorner.x, uvCorner.y + uvWidth.y));
			uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y + uvWidth.y));
			uvs.Add(new Vector2(uvCorner.x + uvWidth.x, uvCorner.y));

			if (face.reversed)
			{
				tris.Add(index + 0);
				tris.Add(index + 1);
				tris.Add(index + 2);
				tris.Add(index + 2);
				tris.Add(index + 3);
				tris.Add(index + 0);
			}
			else
			{
				tris.Add(index + 1);
				tris.Add(index + 0);
				tris.Add(index + 2);
				tris.Add(index + 3);
				tris.Add(index + 2);
				tris.Add(index + 0);
			}
		}

		mData = new Mesh ();
		mData.vertices = verts.ToArray();
		mData.triangles = tris.ToArray();
		mData.uv = uvs.ToArray();
		mData.RecalculateBounds();
		mData.RecalculateNormals();

		tData = new Texture2D (colorFaceData.Count, 1);

		for (int iC = 0; iC < colorFaceData.Count; iC++) {
			tData.SetPixel (iC, 0, colorFaceData [iC]);
		}
		tData.filterMode = FilterMode.Point;
		tData.Apply ();
	}

}