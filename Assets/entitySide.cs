﻿using System;
using UnityEngine;
	
public class entityTeam {
	public string TeamName;
	public string TeamMesh;
	public int TeamColor;
	public string[] MemberNames;

	public static entityTeam Generate(int ID)
	{
		entityTeam value = new entityTeam ();
		value.TeamName = "Team Name";
		value.TeamMesh = "Wizard";
		value.TeamColor = 0;
		value.MemberNames = new string[] {
			"Guy 1",
			"Guy 2",
			"Guy 3",
			"Guy 4",
			"Guy 5",
			"Guy 6",
			"Guy 7",
			"Guy 8"

		};
		switch (ID)
		{
		case 1:
			value.TeamName = "Vanilla Guerilla";
			value.TeamMesh = "Wizard";
			value.TeamColor = 0;
			value.MemberNames = new string[]{
				"General Imami",
				"Commander",
				"Artillery",
				"Support",
				"Grunt",
				"Company",
				"Infantry",
				"Recruit"
			};
			break;
		case 2:
			value.TeamName = "Mintsanity";
			value.TeamMesh = "Wizard";
			value.TeamColor = 2;
			value.MemberNames = new string[]{
				"Scoop of Cthulhu",
				"Cup of the Elders",
				"Eldrich Flavor",
				"Sanity Spoon",
				"Monster Snack",
				"Sweet Revenge",
				"Candy Cultist",
				"Flavor Acolyte"
			};
			break;
		case 3:
			value.TeamName = "Just Ice";
			value.TeamMesh = "Wizard";
			value.TeamColor = 5;
			value.MemberNames = new string[]{
				"Blind Justice",
				"The Right Hand Of Justice",
				"",
				"Guy 4",
				"Guy 5",
				"Guy 6",
				"Cultist",
				"Acolyte"
			};
			break;
		}

		return value;
	}
}

public class entitySide
	{
	public entityTeam team;
	int side=0;
	public static Vector3[] teamColors = new Vector3[]
	{
		new Vector3(255f,0*239f,0*183f),//vanila		yellow
		new Vector3(255f,143f,143f),//strawberry	magenta
		new Vector3(154f,251f,218f),//mint			cyan
		new Vector3(144f, 59f, 13f),//choco			red
		new Vector3(170f,208f,121f),//pistacho		green
		new Vector3( 49f, 37f,129f),//blueberry		blue
	};

	public entitySide (entityTeam T,int i)
	{
		team = T;
		side = i;
	}

	public int GetAlliance(entitySide Other)
	{
		if (Other == null || Other.side != side) {
			return -1;
		}
		if (Other == this) {
			return 0;
		} else  {
			return 1;
		}
	}

	public Color GetColor() {
		//Team Colors
		return GetColor(side) ;
	}

	public static Color GetColor(int side) {
		//Team Colors
		return new Color(teamColors[side].x/255f,teamColors[side].y/255f,teamColors[side].z/255f,1);
	}
}

