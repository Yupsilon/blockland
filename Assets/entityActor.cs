﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityActor:entityBase{
	public PlayerAnimData Animation;
	string Name;
	public entityMesh Skeleton;
	public entitySide Owner;
	public List<propertyModifier> Modifiers = new List<propertyModifier>();
	public List<int> ModifierStates= new List<int>();

	public entityActor(GameMission data,entitySide team, int wormID)
	{
		game = data;
		Owner = team;
		Name = team.team.MemberNames[wormID];
		onCreated ();
		Draw ();
	}
		
	public override bool isRelevant()
	{
		return Skeleton.isSteady();
	}
	public bool isSelected()
	{
		return game.Selection == this;
	}
	public PlayerAnimData getActiveAnimation()
	{
		if (Animation != null) {
			return Animation;
		}
		return new PlayerAnimData ();
	}

	public override bool[] GetPassage()
	{
		bool[] Passage = base.GetPassage ();
		if (Passage [0]) {
			Passage [0] = game.canFitActor (Position,GetTileHeight(),this);
		}
		if (!isGrounded ()) {
			return Passage;
		}
		List<Vector3> neighbors = new List<Vector3> {
			Position+new Vector3(0,1,0),
			Position+new Vector3(0,0,1),
			Position+new Vector3(1,0,0),
			Position+new Vector3(0,0,-1),
			Position+new Vector3(-1,0,0),
			Position+new Vector3(0,-1,0)
		};
		for (int pass = 1; pass < Passage.Length-1; pass++) {
			if (Passage [pass]) {
				Vector3 pos = Position + new Vector3 (0, GetTileHeight (), 0);
				Passage [pass] = game.GetBlockAt (pos)==null && !game.consideredForCollision( game.GetActorAtTile (pos, this));
			} else// if (isGrounded ()) 
			{
				Passage [pass] = game.canFitActor (neighbors [pass],GetTileHeight(),this);
			}
		}

		return Passage;
	}

	public override bool isPlayer ()
	{
		return true;
	}

	public override bool CanAct ()
	{
		return !isDead;
	}

	public void orientToCamera()
	{	
		Quaternion facing = new Quaternion ();

		entityCamera camera = game.getCameraEntity ();
		if (weapon == null) {
			facing=Quaternion.Euler (new Vector3 (camera.CameraRot.y,camera.CameraRot.x,0));
		} else {
			facing=Quaternion.Euler (new Vector3 (camera.CameraAim.y,camera.CameraAim.x,0));
		}
				lookAtPoint (Skeleton.getCenter ().GetWorldPosition () + facing * Vector3.forward*10);

	}

	public void lookAtPoint(Vector3 point)
	{
		if (!CanAct ()) {
			return;
		}
		Skeleton.followTarget = point;
		Skeleton.lastWatchTime = Time.time + 4f;

		Vector3 center = Position - Skeleton.followTarget;
		float newAngle = 180f - Mathf.Atan2 (center.z, center.x) * 180f / Mathf.PI;
		float angleDiff = Mathf.DeltaAngle (Rotation.y, newAngle);

		if (weapon == null) {

			float rotDelta = Skeleton.getCenter ().Data.RotationLimits.y * 3f / 2f;

			FindAnimation ("turn", 0f, 0, 1, 0);

			if (angleDiff < 0 && !Animation.animation.isMirrored) {
				PlayAnimation (Animation.animation.Mirror (), 0f, 0, 1, 0,false);
			}
			Animation.StartTime = (Mathf.Abs ((float)angleDiff / (float)rotDelta));

			if (Mathf.Abs (angleDiff) > rotDelta) {
				FindAnimation (new string[]{ "rotate", "stand" }, 60f, 1 / 5f, 2, 1);

				if (angleDiff < 0 && !Animation.animation.isMirrored) {
					PlayAnimation (Animation.animation.Mirror (), 60f, 0, 2, 1,false);
				}
				FacePoint (point, Animation.GetDuration());
			}
		} else {
			FacePoint (point,1/20f);
		}
	}

	Vector4 inputDir =Vector4.zero;
	bool jumping=false;
	public float weaponCharge=-1f;
	public override void Update ()
	{
		inputDir = new Vector4 (
			Input.GetAxisRaw ("Horizontal Movement"),
			Input.GetAxisRaw ("Vertical Movement"),
			inputDir.x, inputDir.y
		);

		if (isDead) {
			return;
		}

		if (Skeleton != null && !Skeleton.Ragdoll) {
			base.Update ();

			if (game.GetBlockAt (Position) != null) {
				Position.y++;
			}
		}

		if (CanAct () && isSelected ()) {
			float facingTime = 1f / 5f;


			Vector3 cameraRot = Camera.main.transform.rotation.eulerAngles;
			if (inputDir.x != 0 || inputDir.y != 0) {
				float facingDir = Mathf.Atan2 (inputDir.y, inputDir.x) * 180f / Mathf.PI + cameraRot.y;
				 
				FacePoint (facingDir, facingTime);
				Walk (facingDir);
			} else if (inputDir.z != 0 || inputDir.w != 0) {
				FindAnimation ("stop", 20, 0.1f, 9, 0);
			}
			if (Input.GetAxisRaw ("Jump") != 0) {
				if (!jumping && isGrounded ()) {
					Jump (inputDir);
					jumping = true;
				}
			} else {
				jumping = false;
			}


			if (Input.GetAxis ("Fire2") != 0) {

				equiptWeapon (0);
			}

			if (Input.GetAxis ("Fire1") != 0) {

				if (weapon != null && Skeleton.hasWeapon () && (Animation==null || !Animation.isLagging ())) {

					if (weaponCharge >= 0) {
						weaponCharge += Time.fixedDeltaTime*4f/6f;

						if (Animation != null) {
							Animation.StartTime = Mathf.Max (0, weaponCharge);
						}
						if (weaponCharge > 1) {
							weaponCharge = 1f;
							fireWeapon ();
						}
					} else if (weapon.type == propertyWeapon.weapontype_singleshot_charged) {

						if (Animation != null) {
							Animation.StartTime = 0f	;
						}
						weaponCharge = 0f;
					} else {
						fireWeapon ();
					}
				}

				/*entityActor other=null;
				foreach (entityBase entity in game.gameEntities) {
					/*if (entity.isPlayer () && entity != this) {
						other = (entityActor)entity;
					}* /
					entity.FacePoint (Camera.main.transform.position, 1f);
					((entityActor)entity).FindAnimation ("select", 20f, 0.25f,2,2);
				}
				/*other.FacePoint (Position, 1f);
				Vector3 sniperStart = other.Position;

				FindAnimation ("select", 20f, 0.25f,true);
				Timer.Create (true,"checktimer", 1.5f, delegate {
					
						return 0;
					},
					delegate {
						entityGore newBone = Skeleton.GetLimbByName ("rchest").Die (true);
						if (newBone != null) {
							newBone.Skeleton.getCenter ().gameObject.GetComponent<Rigidbody> ().AddExplosionForce (-2000, Camera.main.transform.position, 100);
							SpecialEffects.MakeBeam(game,sniperStart,newBone.Skeleton.getCenter ().GetWorldPosition(),0.08f,0.16f,"bullet");

						}
						FindAnimation ("losehand", 20f, 1.5f,true);
					
						Timer.Create (true,"checktimer", 3f, delegate {

							return 0;
						},
							delegate {
				 newBone = Skeleton.GetLimbByName ("head").Die (true);
				if (newBone != null) {
					newBone.Skeleton.getCenter ().gameObject.GetComponent<Rigidbody> ().AddExplosionForce (-2000, Camera.main.transform.position, 100);
									SpecialEffects.MakeBeam(game,sniperStart,newBone.Skeleton.getCenter ().GetWorldPosition(),0.05f,0.2f,"bullet");
				}
				FindAnimation ("death", 20f, 1f,true);
				Timer.Create (true,"checktimer", 0, delegate {
					if (Animation == null) {
						return 0;
					}
					return Time.deltaTime;
				},
					delegate {
						Skeleton.Death (10);
						return 0;
									});
								return 0;
							});
						return 0;
					});*/
			} else if (weaponCharge >= 0) {
				fireWeapon ();
		}

		if (game.gDat.gui.Window != null) {
			game.gDat.gui.Window.Update ();
		}
	}

	if (!stateNoDraw) {
		if (Animation != null && Animation.GetEndTime () < Time.time) {
			if (Animation.getAnimName () == "walk" && (inputDir.x != 0 || inputDir.y != 0)) {
				Skeleton.Reset (true);
				Animation.Reset ();

			} else if (weapon != null && Animation.getAnimName () == "hold") {
				Animation.Reset ();
			} else {
				Animation = null;
			}
		} else if (Animation == null) {
			if (isGrounded ()) {
				if (Animation == null) {
					if (UnityEngine.Random.value > 0.33f) {
						FindAnimation ("stand", 20f, 0.2f, 0, 0);
					} else {
						FindRańdomAnimation (new string[]{ "idle", "stand" }, 20f, 0.2f, 1, 0);
					}
				}
			} else {
				FindAnimation ("fall", 20f, 0.2f, 1, 1);
			}
		} else if (isGrounded () && isSelected ()) {
				if (weapon != null) {
					if (Animation.priority < 1) {
						if (!Skeleton.hasWeapon ()) { 
							FindAnimation (new string[] {
								"hold." + weapon.animation + ".twohanded",
								"hold." + weapon.animation + ".onehanded"
							}, 0f, 0.2f, 1, 0);
						Animation.StartTime = 0;
							Skeleton.equiptWeapon (weapon, false);
						}
					}
					if (weaponCharge < 0) {
						weapon.AccountAccuracy ();
					}
					updateTargeter ();
				}
			if (!Skeleton.getCenter ().locked) {
				orientToCamera ();
			}
		}
			if (game.settingsLevel == 0) {
				if (rallyTime < Time.time ) {
				rallyTime = Time.time+0.08f;
					Skeleton.Think ();
				}
			} else {
				Skeleton.Think ();
			}
		}
	}

	propertyWeapon weapon=null;
	public void equiptWeapon(int weaponID)
	{
		removeWeapon ();
		weapon = new propertyWeapon (weaponID);
		game.getCameraEntity ().CameraAim = new Vector3(game.getCameraEntity ().CameraRot.x,0,0);
		Animation=null;
		drawTargeted ();
	}
public propertyWeapon getWeapon()
	{
		return weapon;
	}
	public void fireWeapon()
	{
	if (weapon == null || !weapon.enabled) {
			return;
		}
	weapon.Fire (game, this,game.getCameraEntity().CameraAim,weaponCharge);

		FindAnimation (new string[] {
			"fire." + weapon.animation + ".twohanded",
			"fire." + weapon.animation + ".onehanded"
		}, 20f, 0, 3, 2,true);
	Skeleton.enableWeapon (true);
	Animation.weaponEnabled=true;

		weaponCharge = -1;

		if (animTimer != null) {
			animTimer.Kill (true);
		}
		
		weapon = null;
	animTimer = Timer.Create ("endtime", Animation.GetDuration (), delegate(Timer original) {
		
		removeWeapon();
		return 0;
		});		
	}
public void 
removeWeapon()
{Skeleton.Reset (false);
	Skeleton.disableWeapon (true);
	weapon = null;
	clearTargeter ();
}

public GameObject targeter;
public void drawTargeted()
	{
		clearTargeter ();

	targeter= new GameObject();
	if ((FirstPersonHUD)game.gDat.gui.Window  != null) {
		((FirstPersonHUD)game.gDat.gui.Window).enable ();
	}
}
public void clearTargeter()
{
		if (targeter != null) {
			GameObject.Destroy (targeter);
	}

	if ((FirstPersonHUD)game.gDat.gui.Window  != null) {
		((FirstPersonHUD)game.gDat.gui.Window).disable ();
	}
}
public void updateTargeter()
	{
	if (targeter == null || weapon==null) {
			return;
		}


		entityCamera gameCam = game.getCameraEntity ();

		targeter.SetActive (gameCam.firstPerson);
		if (targeter.activeSelf) {
			targeter.transform.position = Skeleton.weapon.parent.transform.position + weapon.accountAimBonuses (gameCam.CameraAim, Skeleton.getHands ()) * Vector3.forward * 100;

		CreatePlane.FaceCamera(targeter.transform,Camera.main);
	}
	}

	public override string GetName ()
	{
		return Name;
	}

	public override float GetMass ()
	{
		
		return 0.5f*game.GetGravityModifier();
	}

	public override float GetAirDrag ()
	{

		return 10f;
	}

	public float GetJumpStrength ()
	{

		return 0.8f;
	}

	public override Vector3 GetHeight()
	{
	return new Vector3(0,Skeleton.GetHeight ()-0.5f,0);
	}

	public float GetWalkSpeed ()
	{
		float Default= 0.06f*GetFriction();
		if (HasState (propertyModifier.modstate.haste)) {
			return Default * 2f;
		}
		return Default;
	}

	public void Jump(Vector2 inputDir)
	{

		bool strong = inputDir == Vector2.zero;
		if (!HasState (propertyModifier.modstate.root)) {

			float prejump = 0.2f;
			if (strong) {
				prejump=0.4f;
			}

			FindAnimation ("prejump", 20f, 0.4f,3, 2);
			AddModifier(new propertyModifier("jump",propertyModifier.dispeltype.extra,0,propertyModifier.modstate.stun),1);
			Timer.Create("jump "+Name,prejump,delegate {
				return 0f;
			}, delegate {
				RemoveModifierByName("jump");
				if (CanAct() && isGrounded()){
					if (!strong) {
						FindAnimation ("jump", 20f, 0.1f, 3, 2);
				ForcesActing.Add (new Vector4 (GetWalkSpeed () * Mathf.Cos (Rotation.y / 180f * Mathf.PI), GetJumpStrength (), GetWalkSpeed () * Mathf.Cos ((Rotation.y + 90) / 180f * Mathf.PI) , Time.time + 0.01f));

					} else {
						FindAnimation ("strongjump", 20f, 0.1f, 3, 2);
				ForcesActing.Add (new Vector4 (0, GetJumpStrength ()*5f/3f, 0f, Time.time + 0.01f));
						//Make the puff
//						SpecialEffects.ExplosionShockwave (game, "ripple", Position-new Vector3(0,0.5f,0), 0.5f, 0.2f,0.4f, Color.white);
					}}

				return 1f;
			});

		}
	}

	float rallyTime=0;
	public void Walk(float angle)
	{
		if (!HasState (propertyModifier.modstate.root)) 
		{
			

				if (isGrounded ()) {
				Skeleton.Reset (true);
				FindAnimation ("walk", 18f, 0.1f,2, 2);
			}
			
		game.RallyNearbyGuys (this,Position,Position+GetHeight(),10);
			ForcesActing.Add (new Vector4 (GetWalkSpeed () * Mathf.Cos (angle / 180f * Mathf.PI), 0, GetWalkSpeed () * Mathf.Cos ((angle + 90) / 180f * Mathf.PI), Time.time + 0.01f));
		}
	}

	public override void onCreated ()
	{
		int Spawn = Mathf.FloorToInt (UnityEngine.Random.value * game.SpawnPoints.Count);

		Position = game.SpawnPoints [Spawn] + Vector3.one / 2f;
		game.SpawnPoints.RemoveAt (Spawn);

		gameObject = new GameObject();
		gameObject.name = GetName ();
		Skeleton= new entityMesh (game, Owner.team.TeamMesh, this);
		Skeleton.frozen = false;
	}

	public override float GetFriction ()
	{
		if (isGrounded ()) {
			return 1f;
		}
		return 1f / 10f;
	}

	public int GetTileHeight()
	{
		return 3;
	}

	public void AddModifier(propertyModifier oData, int Type){

		oData.Duration = game.CurrentTurn+oData.totDuration;

		if (oData.totDuration < 0) {
			oData.Duration = game.CurrentTurn;}
		switch (Type) {
		case 0: //Multiple 
			Modifiers.Add (oData);
			break;
		case 1: //Replace 
			RemoveModifierByName(oData.ModifierName);
			Modifiers.Add (oData);
			break;
		case 2: //Unique 
			if (!HasModifier (oData.ModifierName)) {
				Modifiers.Add (oData);
			}
			break;
		}
		UpdateStates ();}
	public void RemoveModifierByName(string Name){

		List<propertyModifier> Temp = new List<propertyModifier>();
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierName == Name) {
				Temp.Add (Mod);
			}
		}
		foreach (propertyModifier Mod in Temp) {
			Mod.ExecuteFunction (propertyModifier.function_expire);
			Modifiers.Remove (Mod);
		}
		UpdateStates ();
					}
					public bool HasModifier(string Name){
						return GetModifierByName (Name) != null;
					}
					public bool HasState(propertyModifier.modstate state){
						return ModifierStates.Contains ((int)state);
					}

	public propertyModifier GetModifierByName(string Name){
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierName == Name) {return Mod;
			}
		}
		return null;
	}

	public void UpdateStates()
	{
		ModifierStates.Clear();

		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierStates!=null){foreach (int i in Mod.ModifierStates) {
					if (!ModifierStates.Contains (i)) {
						ModifierStates.Add (i);
					}
				}
			}
		}
	}

	public bool FindAnimation(string animName,float aTime,float lag,int force,int priority)
	{
		return FindAnimation (new string[] { animName }, aTime, lag,force,priority);
}

	public bool FindAnimation(string[] animName,float aTime,float lag,int force,int priority)
{
		return FindAnimation (animName, aTime, lag, force, priority, false);
	}

public bool FindAnimation(string[] animName,float aTime,float lag,int force,int priority,bool wep)
	{
		foreach (string anim in animName) {
			List<LimbAnimationData> aTotal = new List<LimbAnimationData> ();
			int I = 0;

			string findName = anim;

			if (anim.EndsWith (".0")) {
				findName=anim.Substring (anim.Length - 2, anim.Length) + "." + I ;
			} 

			while (Skeleton.Data.AnimationNames.Contains (findName + "." + I)) {
				LimbAnimationData bData = Skeleton.Data.FindAnimation (findName + "." + I);
				aTotal.Add (bData);
				if (bData.canBeMirrored) {
					aTotal.Add (bData.Mirror ());
				}
				I++;
			}

			if (Skeleton.Data.AnimationNames.Contains (findName)) {
				aTotal.Add (Skeleton.Data.FindAnimation (findName));
			}
			aTotal.RemoveAll (delegate(LimbAnimationData obj) {
				return !Skeleton.canPerformAnimation (obj);
			});

			if (aTotal.Count > 0) {
				PlayAnimation (aTotal [Mathf.FloorToInt (UnityEngine.Random.value * aTotal.Count)], aTime, lag,force,priority,wep);
				return true;
			} 
		}
	PlayAnimation (new LimbAnimationData (), aTime,lag,force, priority,wep);
		return false;
	}


public void FindRańdomAnimation(string[] animName,float aTime,float lag,int force,int priority)
	{
		List<LimbAnimationData> aTotal = new List<LimbAnimationData> ();
		foreach (string anim in animName) {
			int I = 0;

			string findName = anim;

			if (anim.EndsWith (".0")) {
				findName = anim.Substring (anim.Length - 2, anim.Length) + "." + I;
			} 

			while (Skeleton.Data.AnimationNames.Contains (findName + "." + I)) {
				LimbAnimationData bData = Skeleton.Data.FindAnimation (findName + "." + I);
				aTotal.Add (bData);
				if (bData.canBeMirrored) {
					aTotal.Add (bData.Mirror ());
				}
				I++;
			}

			if (Skeleton.Data.AnimationNames.Contains (findName)) {
				aTotal.Add (Skeleton.Data.FindAnimation (findName));
			}
		}
		aTotal.RemoveAll (delegate(LimbAnimationData obj) {
			return !Skeleton.canPerformAnimation (obj);
		});

		if (aTotal.Count > 0) {
		PlayAnimation (aTotal [Mathf.FloorToInt (UnityEngine.Random.value * aTotal.Count)], aTime, lag,force,priority,false);

		} else {
		PlayAnimation (new LimbAnimationData (), aTime,lag,force, priority,false);
		}
	}

		public void PlayAnimation(LimbAnimationData animName,float aTime,float lag,int force,int priority,bool wep)
	{
		if (Animation == null || (force>Animation.priority)) {
				Animation = new PlayerAnimData (animName, lag, aTime,priority,wep);
			Skeleton.PlayAnimation ();
					}
				}


	public override void Select()
	{
		game.Selection = this;
		FindAnimation ("select", 20f,0.25f,3, 1);
		FacePoint (Camera.main.transform.position, 1f/3f);
		inputDir =Vector4.zero;
	}

public override void Die (bool destroy)
	{
		Skeleton.Death (5+5*game.settingsLevel);

	base.Die (destroy);
	}

	public bool isAlive()
	{
	return Skeleton!=null && !Skeleton.isBeheaded();
	}

	public override bool Collides ()
	{
	return !isSelected () && !HasState (propertyModifier.modstate.nocollision) && !Skeleton.Ragdoll;
	}	
public override void onCollide (float X, float Y, float Z)
{
	if (X != 0) {
		Velocity.x = 0;
	}
	if (Y != 0) {
		Velocity.y = 0;
		if (!isGrounded() && Y < 0) {

			float posDelta = lastY-Position.y;

			if ( posDelta > 1.3f) 
			{
				FindAnimation ("land", 40f, 0.1f, 5, 1);
			}
		}
	}
	if (Z != 0) {
		Velocity.z = 0;
	}
	}
public override Vector3 getTilePosition ()
	{
		if (Skeleton.Ragdoll) {

			Vector3 center = Skeleton.GetCenterOfMass ();
		return new Vector3 (Mathf.Floor (center.x), Mathf.Floor (center.y), Mathf.Floor (center.z));
		}
	return base.getTilePosition ();

}

public bool isAtTile(Vector3 tile)
{
	return isAtTile (Mathf.FloorToInt (tile.x), Mathf.FloorToInt (tile.y), Mathf.FloorToInt (tile.z));
}

	public bool isAtTile(int x,int y, int z)
	{

	Vector3 actorPosition = getTilePosition ();
	if ( actorPosition.x == x && actorPosition.z == z && actorPosition.y <= y && actorPosition.y+GetTileHeight() > y) {
			return true;
		}

return false;
}

public void Knockback()
{
	Skeleton.SetRagdoll (1, 5f);
	foreach (entitySkeleton Zim in Skeleton.Bones) {
		if (Zim.Data.LimbType.Contains ("cosmetic") && !Zim.Data.LimbType.Contains ("immortal")) {
				if (UnityEngine.Random.Range (0, 100) < Zim.Data.Maxhealth) {
				Zim.Die (true,false);
				}
			}
		}
}

}