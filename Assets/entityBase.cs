﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityBase
{
	public static float projMult = 3;
	public static int MaxFacings= 8; 
	public int MaxFrames=0;
	public static int MaxSides = 4;
	public bool underWater = false;

	public GameMission game=null;


	//Movement
	public bool frozen {get; private set;}
	public bool stateNoDraw = false;
	public Vector3 Position = Vector3.zero;
	public Vector3 Velocity = Vector3.zero;
	public Vector3 potentialVelocity = Vector3.zero;
	public List<Vector4> ForcesActing = new List<Vector4>();
	public Vector3 Rotation = Vector3.zero;

	public int ID = 0;	
	public bool isDead = false;
	public GameObject gameObject;

	public virtual void onCreated()
	{
		
	}

	public virtual bool TakeProjectiles()
	{return false;}

	public virtual void changeState(bool freeze)
	{
		frozen = freeze;
	}

	public virtual void Update()
	{
		if (!frozen) {
			Move ();
		}
	}

	public virtual bool CanAct()
	{
		return !isDead;
	}

	float thinkSpeed=1f;
	public virtual float GetSpeed()
	{
		return game.GetSpeed()*thinkSpeed;
	}

	public Vector3 getAcceleration()
	{
		Vector3 total = new Vector3 (0, 0 - GetMass ()*game.GetStageGravity()*Time.fixedDeltaTime, 0);
		foreach (Vector4 acc in ForcesActing) {
			
				total += new Vector3(acc.x,acc.y,acc.z);
		}

		ForcesActing.RemoveAll (delegate(Vector4 acc) {
			return (acc.w < Time.time);
		});


		return total;
	}

	public virtual void onCollide(Vector3 dir)
	{
		if (dir.x != 0) {
			Velocity.x = 0;
		}
		if (dir.y != 0) {
			Velocity.y = 0;
		}
		if (dir.z != 0) {
			Velocity.z = 0;
		}
	}

	public virtual void Die(bool destroy)
	{
		Velocity = Vector3.zero;
		ForcesActing.Clear() ;
		isDead = true;
		if (destroy) {
			Dispose (true);
		}
	}

	public virtual void Dispose(bool destroy)
	{
		game.gameEntities.Remove (this);
		if (destroy) {
			GameObject.Destroy (gameObject);
		}
	}

	public virtual float GetMass()
	{
		return 1f;
	}

	public virtual float GetAirDrag()
	{
		return 5f;
	}

	public virtual float GetFriction(){

		return 1f;}

	public virtual bool[] GetPassage()
	{
		bool[] Passage = new bool[]{ true, true,  true, true, true, true };

		List<entityBlock> neighbors = entityBlock.GetNeighbours (game, Position,false);
		entityActor center = null;
		if (isPlayer ()) {
			center = (entityActor)this;
		}
		List<bool> nearbies = game.GetNeighboringActors (Position,center);
		for (int pass = 0; pass < neighbors.Count; pass++) {
			Passage [pass] = neighbors [pass] == null && nearbies[pass]==false;
		}

		return Passage;
	}

	public virtual void Move()
	{
		if (isGrounded ()) 
		{
			Velocity += getAcceleration () - new Vector3 (Velocity.x * GetFriction (), 0, Velocity.z * GetFriction ());

		} else {
			lastY = Mathf.Max(Position.y,lastY);
			if (GetAirDrag () == 0) {
				Velocity = (Velocity + getAcceleration ());
			} else {
				Velocity = (Velocity + getAcceleration ()) * (1 - Velocity.magnitude / (Velocity.magnitude + GetAirDrag ()));
			}
		}
		potentialVelocity = Velocity;

		Vector3 size = getEntitySize();

		bool stop = false;
		thinkSpeed = Mathf.Min (1, size.magnitude / Velocity.magnitude);
		for (float Zim = 0; Zim < Velocity.magnitude && !stop; Zim+=size.magnitude) {

			bool[] neighbors = GetPassage ();

			if (!neighbors [0]) { // Up
				if (Position.y + Velocity.y * GetSpeed () > Mathf.Ceil (Position.y) - size.y/2f) {
					Position.y = Mathf.Ceil (Position.y) - size.y/2f;
					onCollide (0, +1, 0);
					stop = true;
				}
			} else if (!neighbors [5]) { // Down
				if (Position.y + Velocity.y * GetSpeed () < Mathf.Floor (Position.y) + size.y/2f) {
					Position.y = Mathf.Floor (Position.y) + size.y/2f;
					onCollide (0, -1, 0);
					grounded = true;
					lastY = Position.y;
					stop = true;
				}
			} else {
				grounded = false;
			}

			if (!neighbors [2]) { // Left
				if (Position.x + Velocity.x * GetSpeed () > Mathf.Ceil (Position.x) - size.x/2f) {
					Position.x = Mathf.Ceil (Position.x) - size.x/2f;
				//if (Position.x + Velocity.x * GetSpeed () > Mathf.Floor (Position.x) + size.x/2f) {
				//	Position.x = Mathf.Floor (Position.x) + size.x/2f;
					onCollide (+1, 0, 0);
					stop = true;
				}
			}
				if (!neighbors [4]) { // Right
					if (Position.x + Velocity.x * GetSpeed () < Mathf.Floor (Position.x) + size.x/2f) {
						Position.x = Mathf.Floor (Position.x) + size.x/2f;
				//if (Position.x + Velocity.x * GetSpeed () - 0.1f < Mathf.Ceil (Position.x) - size.x/2f) {
				//	Position.x = Mathf.Floor (Position.x) + size.x/2f;
					onCollide (-1, 0, 0);
					stop = true;
				}
			}

			if (!neighbors [1]) { // Front
				if (Position.z + Velocity.z * GetSpeed () > Mathf.Ceil (Position.z) - size.z/2f) {
					Position.z = Mathf.Ceil (Position.z) - size.z/2f;
			//	if (Position.z + Velocity.z * GetSpeed () > Mathf.Floor (Position.z) + size.z/2f) {
			//		Position.z = Mathf.Floor (Position.z) + size.z/2f;
					onCollide (0, 0, +1);
					stop = true;
				}
			}
				if (!neighbors [3]) { // Back
					if (Position.z + Velocity.z * GetSpeed () < Mathf.Floor (Position.z) + size.z/2f) {
						Position.z = Mathf.Floor (Position.z) + size.z/2f;
			//	if (Position.z + Velocity.z * GetSpeed () - 0.1f < Mathf.Ceil (Position.z) - size.z/2f) {
			//		Position.z = Mathf.Floor (Position.z) + size.z/2f;
					onCollide (0, 0, -1);
					stop = true;
				}
			}
			Tick ();
		}
		thinkSpeed = 1f;
	}

	bool grounded=false;
	public float lastY=-1;
	public bool isGrounded()
	{
		return grounded;
	}

	public virtual Vector3 getEntitySize()
	{
		return Vector3.one;
	}

	public virtual void Tick()
	{
		Position = new Vector3 (
			Position.x+Velocity.x*GetSpeed(),
			Position.y+Velocity.y*GetSpeed()
			,Position.z+Velocity.z*GetSpeed()
		);
			
		Draw ();
	}

	public virtual void Draw()
	{
		gameObject.transform.position = new Vector3(Position.x,Position.y,Position.z)+GetHeight();
		gameObject.transform.rotation = Quaternion.Euler(Rotation);
	}

	public virtual float GetElasticity()
	{
		return 0;
	}

	public virtual Vector3 GetHeight()
	{
		return Vector3.zero;
	}

	public virtual void onCollide(float X, float Y, float Z)
	{
		onCollide (new Vector3 (X, Y, Z));
	}

	public virtual bool isPlayer()
	{
		return false;
	}

	public virtual string GetName()
	{
		return "Entity";
	}

	public bool AmIInRangeOfUnit(entityBase Attacker,float Range)
	{
		Vector2 PosA = Attacker.Position;
		Vector2 PosB = Position;

		return (Mathf.Sqrt((PosA.x-PosB.x)*(PosA.x-PosB.x)+(PosA.y-PosB.y)*(PosA.y-PosB.y))<=Range);
	}

	public int[] GetMapCoords(){
		return new int[]{Mathf.FloorToInt(Position.x),Mathf.FloorToInt(Position.y)};
	}

	public entityBlock GetMyTile()
	{
		return game.GetBlockAt (Position);
	}

	public virtual void Select()
	{

	}

	public void FacePoint(Vector3 point, float time)
	{
		float ang = Mathf.Atan2(
			Position.z-point.z,
			point.x-Position.x
		)*180f/Mathf.PI;

		FacePoint(ang,time); 
		}

	public Timer animTimer = null;
	public void FacePoint(float ang, float time)
	{
		float startTime = Time.time;
		float startRot = Rotation.y;

		float endRot = Mathf.DeltaAngle(Rotation.y,ang);
		float endtime = time;

		animTimer = Timer.Create (GetName () + " facing", 0, delegate {

			if (startTime+endtime<=Time.time)
			{
				return 0;
			}
			Rotation.y =startRot+endRot*((Time.time-startTime)/endtime);
			return Time.fixedDeltaTime;
		},
			delegate {
				Rotation.y = startRot+endRot;
				return 0;
			});
	}

	public virtual bool Collides()
	{
		return false;
	}

	public virtual bool isRelevant()
	{
		return false;
	}

	public virtual Vector3 getTilePosition()
	{
		return new Vector3 (Mathf.Floor (Position.x), Mathf.Floor (Position.y), Mathf.Floor (Position.z));
	}
}
