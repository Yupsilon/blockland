﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

public class LimbBodyData
{
	[XmlAttribute("Name")]
	public string LimbName="";
	public string LimbParent="";
	public Vector3 AttachPoint=Vector3.zero;
	public Vector3 CenterPoint=Vector3.zero;
	public Vector3 Dimensions=Vector3.zero;
	public Vector3 RotationLimits=Vector3.zero;
	public Vector2 JointLimits=Vector2.zero;
	public float Maxhealth=100;
	public List<string> LimbType;

	public static LimbBodyData Make (string LimbName, string LimbParent, Vector3 Center, Vector3 Attach, Vector3 Dimensions,Vector3 RotationLimits,Vector2 JointLimits,float limbHealth, string LimbType)
	{
		LimbBodyData Gir = new LimbBodyData ();
		Gir.LimbName = LimbName;
		Gir.LimbParent = LimbParent;
		Gir.AttachPoint = Attach;
		Gir.CenterPoint = Center;
		Gir.Dimensions = Dimensions;
		Gir.RotationLimits = RotationLimits;
		Gir.JointLimits = JointLimits;
		Gir.Maxhealth = limbHealth;
		Gir.LimbType = new List<string>{LimbType};
		return Gir;
	}

	public static LimbBodyData Make (string LimbName, string LimbParent, Vector3 Center, Vector3 Attach, Vector3 Dimensions,Vector3 RotationLimits,Vector2 JointLimits,float limbHealth, string LimbTypeA, string LimbTypeB)
	{
		LimbBodyData Gir = new LimbBodyData ();
		Gir.LimbName = LimbName;
		Gir.LimbParent = LimbParent;
		Gir.AttachPoint = Attach;
		Gir.CenterPoint = Center;
		Gir.Dimensions = Dimensions;
		Gir.RotationLimits = RotationLimits;
		Gir.JointLimits = JointLimits;
		Gir.Maxhealth = limbHealth;
		Gir.LimbType = new List<string>{LimbTypeA,LimbTypeB};
		return Gir;
	}

	public static LimbBodyData Make (string LimbName, string LimbParent, Vector3 Center, Vector3 Attach, Vector3 Dimensions,Vector3 RotationLimits,Vector2 JointLimits,float limbHealth, string LimbTypeA, string LimbTypeB, string LimbTypeC)
	{
		LimbBodyData Gir = new LimbBodyData ();
		Gir.LimbName = LimbName;
		Gir.LimbParent = LimbParent;
		Gir.AttachPoint = Attach;
		Gir.CenterPoint = Center;
		Gir.Dimensions = Dimensions;
		Gir.RotationLimits = RotationLimits;
		Gir.JointLimits = JointLimits;
		Gir.Maxhealth = limbHealth;
		Gir.LimbType = new List<string>{LimbTypeA,LimbTypeB,LimbTypeC};
		return Gir;
	}

	public static LimbBodyData Make (string LimbName, string LimbParent, Vector3 Center, Vector3 Attach, Vector3 Dimensions,Vector3 RotationLimits,Vector2 JointLimits,float limbHealth,  List<string> LimbType)
	{
		LimbBodyData Gir = new LimbBodyData ();
		Gir.LimbName = LimbName;
		Gir.LimbParent = LimbParent;
		Gir.AttachPoint = Attach;
		Gir.CenterPoint = Center;
		Gir.Dimensions = Dimensions;
		Gir.RotationLimits = RotationLimits;
		Gir.JointLimits = JointLimits;
		Gir.Maxhealth = limbHealth;
		Gir.LimbType = LimbType;
		return Gir;
	}

	public void DrawMesh(entitySkeleton parent)
	{

		if (parent.Data.LimbType.Contains ("voxel")) {

			if (parent.MeshData.Data.LoadTexture (parent.Data.LimbName, true) == null) {
				Debug.Log (parent.Data.LimbName + " does not exist!");
			}
			VoxelDrawer mesh = new VoxelDrawer(parent.MeshData.Data.LoadTexture (parent.Data.LimbName,true),10);

			parent.gameObject.GetComponent<MeshFilter> ().mesh = mesh.mData;
			parent.gameObject.GetComponent<MeshRenderer> ().material = Material.Instantiate(Resources.Load<Material> ("Materials/Actor"));
			parent.gameObject.GetComponent<MeshRenderer> ().material.mainTexture = mesh.tData;
		} else if (!parent.Data.LimbType.Contains ("norender")) {

			Mesh mData = new Mesh ();

			List<Vector3> verts = new List<Vector3> ();
			List<Vector2> uvs = new List<Vector2> ();
			List<int> tris = new List<int> ();

			FaceData[] mesh = new FaceData[] {
				new FaceData (null, new Vector3 (0.5f, 0.5f, 0.5f), Vector3.up, Vector3.forward, false, 0),
				new FaceData (null, new Vector3 (1.5f, 0.5f, 0.5f), Vector3.up, Vector3.forward, true, 1),
				new FaceData (null, new Vector3 (0.5f, 0.5f, 0.5f), Vector3.forward, Vector3.right, false, 2),
				new FaceData (null, new Vector3 (0.5f, 1.5f, 0.5f), Vector3.forward, Vector3.right, true, 3),
				new FaceData (null, new Vector3 (0.5f, 0.5f, 0.5f), Vector3.up, Vector3.right, true, 4),
				new FaceData (null, new Vector3 (0.5f, 0.5f, 1.5f), Vector3.up, Vector3.right, false, 5)
			};

			foreach (FaceData fais in mesh) {
				int index = verts.Count;

				verts.Add (fais.center - Vector3.one);
				verts.Add (fais.center + fais.height - Vector3.one);
				verts.Add (fais.center + fais.height + fais.width - Vector3.one);
				verts.Add (fais.center + fais.width - Vector3.one); 


				Vector2 uvWidth = new Vector2 (0.333f, 0.5f);
				Vector2 uvCorner = new Vector2 (fais.colorID / 3f - Mathf.Floor (fais.colorID / 3f), Mathf.Floor (fais.colorID / 3f) / 2f);

				uvs.Add (uvCorner);
				uvs.Add (new Vector2 (uvCorner.x, uvCorner.y + uvWidth.y));
				uvs.Add (new Vector2 (uvCorner.x + uvWidth.x, uvCorner.y + uvWidth.y));
				uvs.Add (new Vector2 (uvCorner.x + uvWidth.x, uvCorner.y));


				if (fais.reversed) {
					tris.Add (index + 0);
					tris.Add (index + 1);
					tris.Add (index + 2);
					tris.Add (index + 2);
					tris.Add (index + 3);
					tris.Add (index + 0);
				} else {
					tris.Add (index + 1);
					tris.Add (index + 0);
					tris.Add (index + 2);
					tris.Add (index + 3);
					tris.Add (index + 2);
					tris.Add (index + 0);
				}

			}

			mData.vertices = verts.ToArray ();
			mData.triangles = tris.ToArray ();
			mData.uv = uvs.ToArray ();
			mData.RecalculateBounds ();
			mData.RecalculateNormals ();

			parent.gameObject.GetComponent<MeshRenderer> ().material = Material.Instantiate(Resources.Load<Material> ("Materials/Actor"));
			parent.gameObject.GetComponent<MeshFilter> ().mesh = mData;
		}
	}
}