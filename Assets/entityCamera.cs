﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityCamera:entityBase{

	public GameObject target;
	public bool firstPerson = false;
	public Vector3 CameraRot=Vector3.one;
	public Vector3 CameraAim=Vector3.zero;

	public entityCamera(GameMission data)
	{
		game = data;
		onCreated ();
	}


	public override void Update()
	{
		if (game.Selection != null) {
			target = game.Selection.Skeleton.getCenter().gameObject;
			gameObject.transform.position = target.transform.position-getCamPos (CameraRot);
			gameObject.transform.rotation = Quaternion.Euler (new Vector3 (CameraRot.y,CameraRot.x,0));

			if (game.GetBlockAt (gameObject.transform.position ) != null) {
				//Unstick
				RotateAxis(new Vector3 (0,0,-0.2f),true);

			}
			if (Input.GetAxis ("Horizontal") != 0) { 
				RotateAxis (new Vector3 (Input.GetAxisRaw ("Horizontal") * 4f, 0, 0),	false);
			}
			if (Input.GetAxis ("Vertical") != 0) {
				RotateAxis (new Vector3 (0, -Input.GetAxisRaw ("Vertical") * 3f, 0),	false);
			}

			if (firstPerson) {
				if (Input.GetAxis ("Fire3") == 0) {
					CameraRot = new Vector3 (CameraRot.x, 45, 10);
					firstPerson = false;
					target.GetComponent<MeshRenderer>().enabled= (true);
					game.gDat.gui.Clear ();
				}
			} else {
				if (Input.GetAxis ("Fire3") != 0) {
					firstPerson = true;
					target.GetComponent<MeshRenderer>().enabled= (false);
					game.gDat.gui.OpenWindow( new FirstPersonHUD (game.gDat.gui,game.Selection,new Rect(0,0,1600,900)));
					CameraRot = new Vector3 (CameraRot.x, CameraRot.y, 0);
				}

				if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
					RotateAxis (new Vector3 (0, 0, Input.GetAxisRaw ("Mouse ScrollWheel") * 2),	false);
				}
			}

			if (CameraRot.z < target.transform.localScale.y/2f) {
				CameraAim = CameraRot;
			}
		}
		/*if (target != null) {
			gameObject.transform.rotation = (Quaternion.FromToRotation (target.transform.position, gameObject.transform.position));
		}*/
	}

	public void RotateAxis(Vector3 delta,bool forced)
	{
		if (delta.z < 0) {
			forced = true;
		}
		Vector3 newRot = CameraRot + delta;
		Vector3 testRot = CameraRot + delta*2;
		newRot = new Vector3 (newRot.x % 360, Mathf.Clamp(newRot.y,-50,50),  Mathf.Clamp(newRot.z,0,25));

		if (forced || game.GetBlockAt (target.transform.position - getCamPos (testRot)) == null) 
		{
			CameraRot = newRot;
		}
	}

	public Vector3 getCamPos(Vector3 rotation)
	{
		return new Vector3 (
			Mathf.Cos((90-rotation.x)/180f*Mathf.PI)*Mathf.Cos(rotation.y/180f*Mathf.PI),
			-Mathf.Sin(rotation.y/180f*Mathf.PI),
			Mathf.Cos(rotation.x/180f*Mathf.PI)*Mathf.Cos(rotation.y/180f*Mathf.PI)
		)*rotation.z;
	}

	public override void onCreated ()
	{
		CameraRot = new Vector3 (90, 45, 10);
		gameObject = Camera.main.gameObject;
		gameObject.GetComponent<Camera>().cullingMask^=(LayerMask.NameToLayer("LayerNoDraw"));
	}
}