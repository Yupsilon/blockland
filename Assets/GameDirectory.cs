﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class GameDirectory { 

	public static string UIFile="GUI/";
	public static string[] ImageFileExtension= new string[]{".tga",".png",".jpg"};
	public static string DirectoryFolder = Application.dataPath +"/";

	public static string TerrainFile="Terrain/";	
	public static string LanguageFile="Language/";	

	public static string DataFolder="Custom/";

	public static string AssetFolder="Assets/Resources/";

	public static bool DataDebug = false;

	public static string CleanseString(string name)
	{
		return System.Text.RegularExpressions.Regex.Replace (name, @"[^a-zA-Z0-9 -_.']", "");

	}

	public static void CreateRequiredFiles()
	{
		if ( !Directory.Exists(Application.dataPath+GameDirectory.DataFolder) ) {
			Directory.CreateDirectory( Application.dataPath+GameDirectory.DataFolder);
		}	
		if ( !Directory.Exists(Application.dataPath+GameDirectory.DataFolder+GameDirectory.LanguageFile) ) {
			Directory.CreateDirectory( Application.dataPath+GameDirectory.DataFolder+ GameDirectory.LanguageFile);
		}	
	}
}