﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class entityMesh
{
	public entityActor Owner;
	public propertyMesh Data;
	public Color teamColor=Color.white;
	public List<entitySkeleton> Bones;
	public bool Ragdoll=true;
	public bool ofTrack=false;
	public List<entitySkeleton> Pivots;
	public List<entityChunk> Collision;
	public int Frame=0;
	GameMission game;
	public GameObject parent;
	public bool frozen = true;
	public bool underWater = false;
	public float lastWatchTime=-1;

	public float GetHeight()
	{
		if (Pivots.Count == 0) {
			return 0;
		} else {
			float Height = 0;

			foreach (entitySkeleton Zyzyx in Pivots) {
				Height = Mathf.Max (0-Zyzyx.gameObject.transform.localPosition.y+Zyzyx.Data.Dimensions.y/2f
					, Height);
			}

			return Height;
		}
	}

	public entityMesh(GameMission g,string LoadFrom,entityActor Parent)
	{	
		game = g;
		Data = new propertyMesh (g.gDat,LoadFrom);
		Pivots=new List<entitySkeleton>();
		Owner = Parent;
		parent = Owner.gameObject;

/*		string fore = "icecream";
		if (Data.TextureName.Length>=fore.Length && Data.TextureName.Substring (0, fore.Length) == fore) {
*/			//PLAYER TEAM /COLOR
		
			teamColor = Parent.Owner.GetColor();


		Bones = new List<entitySkeleton> ();
		foreach (LimbBodyData Bone in Data.MeshData.BodyData) {

			if (Bone.LimbType.Contains("particle")) {
				entitySkeleton particlePoint = GetLimbByName (Bone.LimbParent);
				if (particlePoint != null) {
					particlePoint.particleEffect = SpecialEffects.ParticleEffect (game, Data.LoadTexture(Bone.LimbName), 10f, Mathf.Max (Bone.Dimensions.x, Bone.Dimensions.y),
						Bone.RotationLimits.x, Bone.RotationLimits.y, Bone.Dimensions.z, Color.white, Bone.RotationLimits.z, particlePoint.gameObject);
				}
			} else if (!Bone.LimbType.Contains( "weapon")) {
				entitySkeleton newSkel = new entitySkeleton (Parent.gameObject, Bone, this);

				Bones.Add (newSkel);
				newSkel.gameObject.transform.SetParent (Parent.gameObject.transform);
				if (newSkel.flags.Contains ("pivot")) {
					Pivots.Add (newSkel);
				}
			}
		}

		SetRagdoll (0,0);
	}

	public entityMesh(GameMission g)
	{	
		game = g;
		Bones = new List<entitySkeleton> ();
	}

	public entitySkeleton GetLimbByName(string Name)
	{
		foreach (entitySkeleton sData in Bones) {
			if (sData.name == Name) {
				return sData;
			}
		}
		return null;
	}

	public List<entitySkeleton> GetLimbsByType(string Type)
	{
		List<entitySkeleton> aData = new List<entitySkeleton>();
		foreach (entitySkeleton sData in Bones) {
			if (sData.Data.LimbType.Contains( Type)) {
				aData.Add( sData);
			}
		}
		return aData;
	}

	public void SetRagdoll(int state,float delta) {
		Ragdoll=state==1;

		if (state==-1)
		{frozen=true;
			state = 0;
		}
		foreach (entitySkeleton Stocking in Bones) {
			Stocking.changeState(state,delta);
		}
		if (state==1) {
			game.collidingEntities.Add (Owner);
			game.changeGameState (1,false);
		}
	}

	public void Death (float delta){
		GetLimbByName ("head").ChangeFace (4);
		//SetRagdoll (true, delta);

		foreach (entitySkeleton Stocking in Bones) {
			if (Stocking.liveState>0) {
				Stocking.Die (false, true);
			}
		}
		game.SpawnEntity (new entityGore (this));
	}

	public virtual void Kill(bool destroy)
	{
		if (Owner != null) {
			Owner.Skeleton = null;
		}
		if (destroy) {
		foreach (entitySkeleton Panty in Bones) {
			GameObject.Destroy (Panty.gameObject);
			}}
		if (parent != null) {
			GameObject.Destroy (parent);
		}
		Bones.Clear ();
	}


	public Vector3 GetCenterOfMass()
	{
		Vector3 center = Bones [0].GetWorldPosition ();
		foreach (entitySkeleton Bone in Bones) {
			center = (Bone.GetWorldPosition () + center) / 2f;
		}
		return center;
	}

	public bool isSteady() 
	{
		if (frozen || Bones.Count==0) {
			return true;
		}
		bool soft = game.settingsLevel>1;
		if (!soft) {
			foreach (entitySkeleton Bone in Bones) {
				if (!Bone.isStatic ()) {
					return false;
				}
			}
			return true;
		}
		return getCenter().isStatic();
	}

	public void Think()
	{	
		if (!frozen) {
			Vector3 worldPos = GetCenterOfMass ();
			if (!Ragdoll) {

				Vector2 Pos = new Vector2 (0, Mathf.Infinity);
				if (Pivots.Count == 0) {
					Pos = new Vector2 (0, 0);
				} else if (Pivots.Count == 1) {

					Pos = Pivots [0].gameObject.transform.localPosition;
				} else {
					foreach (entitySkeleton Panty in Pivots) {
						if (Panty.liveState==0) {
							Pos.y = Mathf.Min (Pos.y, Panty.gameObject.transform.localPosition.y + Panty.gameObject.transform.localScale.y / 2);
						}
					}
					if (followTarget != Vector3.zero) {
						if (!GetLimbByName ("head").locked) {
							GetLimbByName ("head").FacePoint (followTarget, 2);
						}
						if (!GetLimbByName ("lhand").locked) {
							GetLimbByName ("lhand").FacePoint (followTarget, 2);
						}
						if (!GetLimbByName ("rhand").locked) {
							GetLimbByName ("rhand").FacePoint (followTarget, 2);
						}
					}
				}

				foreach (entitySkeleton Panty in Bones) {
								
					Panty.Update (Owner.getActiveAnimation());

				}
				if (lastWatchTime > 0 && lastWatchTime < Time.time) {
					if (Owner.Animation.isFrozen ()) {
						Owner.Animation=null;
					}
					Reset (false);
					lastWatchTime = -1;
				}
			} else {

				if (isSteady ()) {
					if (!isAttachedToActor () && game.settingsLevel == 0) {
						Kill (true);
					} else {
						SetRagdoll(-1,0);
					}
				} else {




				List <Vector3> CollisData = new List<Vector3> {
					worldPos,
					(worldPos - new Vector3 (1, 0, 0) * game.chunk),
						(worldPos + new Vector3 (1, 0, 0) * game.chunk),
						(worldPos - new Vector3 (0, 1, 0) * game.chunk),
						(worldPos + new Vector3 (0, 1, 0) * game.chunk),
						(worldPos - new Vector3 (0, 0, 1) * game.chunk),
						(worldPos + new Vector3 (0, 0, 1) * game.chunk)
				};

				foreach (Vector3 relpos in CollisData) {
					if (game.GetChunkAt (relpos) != null) {
						game.GetChunkAt (relpos).enableCollision (true);
					}
						}
				


					foreach (entitySkeleton Panty in Bones) {

						Panty.PhysixUpdate (game);
					}
				}
			}
			if (worldPos.y < game.GetWaterLevel () && !underWater) {

				underWater = true;

				SpecialEffects.makeSplash (game, worldPos, 1, 1.5f);

				if (Owner != null) {
					Owner.Die (false);
				}
			} 
		}
	}


	public void Clear() {//obsolete

		foreach (entitySkeleton Panty in Bones)
		{
			GameObject.Destroy(Panty.gameObject);
		}
	}

	public bool canPerformAnimation(LimbAnimationData Required)
	{
		foreach (string Stocking in Required.Pivots) {
			entitySkeleton Panty = GetLimbByName (Stocking);
			if (Panty == null || Panty.liveState>0) {
				return false;
			}
		}
		return true;
	}
	public Vector3 followTarget = Vector3.zero;

	public void Reset(bool clearWeapon)
	{
		ofTrack = false;
		followTarget = Vector3.zero;
		foreach (entitySkeleton Bone in Bones) {
			Bone.Reset (clearWeapon);
		}
		if (clearWeapon && hasWeapon ()) {
			disableWeapon (false);
		}
	}
	public void PlayAnimation()
	{
		if (!Owner.Animation.weaponEnabled) {
			disableWeapon (false);
		}
		foreach (entitySkeleton bone in Bones) {
			bone.Reset (true);
			bone.DrawFrame (Owner.Animation, 0);

		}
	}

	public bool isAttachedToActor()
	{
		return Owner!=null;
	}

	public entitySkeleton getCenter()
	{
		if (isAttachedToActor () && GetLimbByName ("head") != null) {
			return GetLimbByName ("head");
		} else {
			foreach (entitySkeleton skData in Bones) {
				if (skData.GetParent () == null) {
					return skData;
				}
			}
		}
		return null;
	}

	public bool isBeheaded()
	{
		return GetLimbByName ("head").liveState==0;
	}

	public int getHands()
	{
		int Hands = 0;
		if (GetLimbByName ("lhand").liveState==0) {
			Hands++;
		} 
		if (GetLimbByName ("rhand").liveState==0) {
			Hands++;
		} 
		return Hands;
	}

	public int getFeet()
	{
		int Feet = 0;
		if (GetLimbByName ("lfoot").liveState==0) {
			Feet++;
		} 
		if (GetLimbByName ("rfoot").liveState==0) {
			Feet++;
		} 
		return Feet;
	}
	public entityMesh weapon=null;
	float fadeTime=0.25f;

	public void disableWeapon(bool perma)
	{
		if (!perma && !hasWeapon()) {
			return;
		} else if (hasWeapon ()) {
		weapon.ofTrack = false;
		float startTime = Time.time;

		entityMesh weaponDummy = weapon;
		string chkName = "weaponPopOut";
		if (animTimer != null && animTimer.name!=chkName) {
			animTimer.Kill (false);
		}
			animTimer = Timer.Create (chkName, 0, delegate {

				if (weaponDummy.parent == null || startTime + fadeTime < Time.time) {
					return 0;
				}

				weaponDummy.parent.transform.localScale = Vector3.one * (1 - (Time.time - startTime) / fadeTime);

				return Time.fixedDeltaTime;
			}, delegate {
				if (perma) {
					weaponDummy.Kill (true);
					weapon = null;
				} else if (weaponDummy.parent!=null){
					weaponDummy.parent.SetActive (false);
				}
				return 0;
			});
		} else if (perma && weapon!=null) {
			weapon.Kill (true);
			weapon = null;
		}
	}

	public bool hasWeapon()
	{
		return weapon != null && weapon.parent != null && weapon.parent.activeSelf;
	}

	public Timer animTimer;
	public void equiptWeapon(propertyWeapon weaponData,bool instant)
	{
		if (animTimer != null) {
			animTimer.Kill (true);
		}

		if (weapon != null) {
			disableWeapon (true);
		}

		if ((weapon == null || weapon.parent == null || weapon.parent.name != weaponData.name)) {
			

			entitySkeleton equiptArm = GetLimbByName ("lhand_attach");
			if (equiptArm == null) {
				equiptArm = GetLimbByName ("rhand_attach");
			}
			weapon = new entityMesh (game);
			weapon.parent = new GameObject ();
			weapon.parent.name = weaponData.name;

			weapon.Data = new propertyMesh (game.gDat);
			weapon.Data.TextureName = Data.TextureName;
			weapon.Bones = new List<entitySkeleton> ();
			weapon.Data.LimbNames = new List<string> ();

			List<LimbBodyData> BodyData = new List<LimbBodyData> ();

			foreach (LimbBodyData Panty in Data.MeshData.BodyData) {

				if ((Panty.LimbName == weaponData.name || weapon.Data.LimbNames.Contains (Panty.LimbParent))) {
		
					BodyData.Add (Panty);
					weapon.Data.LimbNames.Add (Panty.LimbName);
				}
			}

			Vector3 origin = Vector3.zero;
			equiptArm.gameObject.transform.localScale = Vector3.one;

			List<entitySkeleton> tempBones = new List<entitySkeleton> ();
			foreach (LimbBodyData Panty in BodyData) {

				if (Panty.LimbType.Contains( "particle")) {

					entitySkeleton particlePoint = tempBones[0];
					foreach (entitySkeleton Zim in tempBones) {
						if (Zim.name == Panty.LimbParent) {
							particlePoint = Zim;
						}
					}


					if (particlePoint != null) {
						particlePoint.particleEffect = SpecialEffects.ParticleEffect (game, Data.LoadTexture(Panty.LimbName), 500f, Mathf.Max (Panty.Dimensions.x, Panty.Dimensions.y),
							Panty.RotationLimits.x, Panty.RotationLimits.y, Panty.Dimensions.z, Color.white, Panty.RotationLimits.z, particlePoint.gameObject);
					}
				} else {
					entitySkeleton boney = new entitySkeleton (weapon.parent, Panty, weapon);
					tempBones.Add (boney);
					if (Panty.LimbParent == "") {
						origin = Panty.AttachPoint;
						boney.Position = new Vector3 (
							boney.Data.Dimensions.x * boney.Data.AttachPoint.x,
							boney.Data.Dimensions.y * boney.Data.AttachPoint.y,
							boney.Data.Dimensions.z * boney.Data.AttachPoint.z
						);
					} else {
						entitySkeleton parent = null;
						foreach (entitySkeleton Zim in tempBones) {
							if (Zim.name == Panty.LimbParent) {
								parent = Zim;
							}
						}
						boney.SetParent (parent);
						boney.Position = parent.Position +
						new Vector3 (
							parent.Data.Dimensions.x * boney.Data.CenterPoint.x,
							parent.Data.Dimensions.y * boney.Data.CenterPoint.y,
							parent.Data.Dimensions.z * boney.Data.CenterPoint.z
						) - new Vector3 (
							boney.Data.Dimensions.x * boney.Data.AttachPoint.x,
							boney.Data.Dimensions.y * boney.Data.AttachPoint.y,
							boney.Data.Dimensions.z * boney.Data.AttachPoint.z
							);

					}
					boney.gameObject.transform.position = boney.Position;
				}
			}
			weapon.Bones = tempBones;

			weapon.parent.transform.SetParent (equiptArm.gameObject.transform);
			weapon.parent.transform.localPosition = origin;
			weapon.parent.transform.localRotation = Quaternion.Euler (BodyData [0].RotationLimits);
			weapon.ofTrack = false;

			weapon.SetRagdoll (-1,0);
		}
		enableWeapon (instant);
	}

	public void enableWeapon(bool instant){

			if (!weapon.parent.activeSelf) {
				weapon.parent.SetActive (true);
			}

		weapon.parent.transform.localScale = Vector3.zero;
			weapon.ofTrack = true;

		float startTime = Time.time;
		animTimer = Timer.Create ("weaponPopIn", 0, delegate {

			if (weapon==null){
				return 0;
			}
			weapon.parent.transform.localScale = Vector3.one * ((Time.time - startTime) / fadeTime);
			if (startTime + fadeTime < Time.time || instant) {
					return 0;
				}
				return Time.fixedDeltaTime;
			}, delegate {
			if (weapon!=null){
				weapon.parent.transform.localScale = Vector3.one;
			}

				return 0;
			});
	}

	public void LinkToGameObject(GameObject parent)
	{

		foreach (entitySkeleton Zim in Bones) {
			Zim.gameObject.transform.SetParent (parent.transform);
		}
	}

	public void ApplyKnockback(float strength, Vector3 center, float radius,float radiusDelta,float minDur)
	{
		SetRagdoll (1, minDur);
		foreach (entitySkeleton Zim in Bones) {
			Zim.gameObject.GetComponent<Rigidbody> ().AddExplosionForce (strength*200f,center ,radius*radiusDelta);
		}
	}

	public void TakeDamage(float damage,Vector3 center)
	{
		foreach (entitySkeleton Zim in Bones) {
			Zim.TakeDamage (damage, 0);
		}
	}
}
