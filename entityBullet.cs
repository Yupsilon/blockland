﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityBullet: entityProjectile
{

	public entityBullet (GameMission g, GameObject parent,Vector3 speed, float weight, bool wind, string trail, string ground)
	{
		game = g;
		gameObject = parent;
		behavior = 0;

		ForcesActing.Add (new Vector4 (speed.x, speed.y, speed.z, Time.time + 0.01f));

		Weight = weight;
		affectedByWind = wind;

		trailTexture = SpecialEffects.LoadTexture(game.gDat,trail);
		bumpTexture=SpecialEffects.LoadTexture(game.gDat,ground);
	}

	public override void Move()
	{
		if (GetAirDrag () == 0) {
			Velocity = (Velocity + getAcceleration ());
		} else {
			Velocity = (Velocity + getAcceleration ()) * (1 - Velocity.magnitude / (Velocity.magnitude + GetAirDrag ()));
		}


		List<RaycastHit> targets = new List<RaycastHit> ();
		targets.AddRange( Physics.RaycastAll(Position,Velocity.normalized,Velocity.magnitude));
		targets.Sort (delegate(RaycastHit x, RaycastHit y) {
			return (x.point - Position).magnitude.CompareTo ((y.point - Position).magnitude);
		});

		if (targets.Count > 0) {
			//attack
			foreach (RaycastHit Zim in targets) {
				if (Zim.rigidbody.tag == "player_limb") {

					Position = Zim.point;
					if (Zim.rigidbody.GetComponent<componentSkeleton>()!=null)
					{
					gameObject.transform.SetParent (Zim.rigidbody.transform);
						componentSkeleton target = Zim.rigidbody.GetComponent<componentSkeleton> ();
						target.parent.TakeDamage (1, 0);
						float knockback = 0;
						if (knockback > 0) {
							target.getOwner ().Knockback ();
							target.GetComponent<Rigidbody> ().AddForce (knockback*Velocity.normalized);
						}
					}
					break;
				} else if (Zim.rigidbody.tag == "ground") {
					Position = Zim.point;
					break;
				}
			}
					} else {
						Position = new Vector3 (
							Position.x+Velocity.x*GetSpeed(),
							Position.y+Velocity.y*GetSpeed()
							,Position.z+Velocity.z*GetSpeed()
						);

		}

					Draw ();

	}
	public override void onCreated ()
	{
		game.changeGameState (2, false);
		base.onCreated ();
	}
	public override void Die(bool dispose)
	{
		base.Die (dispose);
		if (!dispose) {
			Timer.Create ("destroy EFX", 5, delegate {
				Die (true);
				return 0;
			});
				}
	}
}

